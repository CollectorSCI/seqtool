SEQTool
Version 1.0.0.1
By Andrew Branscom
http://sierrahelp.com/
http://sciwiki.sierrahelp.com/index.php?title=SEQTool


SEQTool 1.0.0.0 (Plugin for SCI Companion http://scicompanion.com/) is based on Kawa's command line SeqMaker tool and uses his KawaTools class. SEQTool is a plugin for SCI Companion for creating, editing and viewing of SCI SEQ (cut scene) resources. It can be opened directly from SCI Companion, using your Companion project as for your SEQ project.

SEQ resources were used for cut scenes in the Sierra SCI1.1 games. It is also a viewer that can be used to play existing SEQ files, including those from the official Sierra games like King's Quest 6 and Gabriel Knight 1.


SEQTool can create SEQ resources from uncompressed individual 320 by 200 pixel 8-bit (256) color or from 320 by 200 pixel animated GIF images for SCI 1.1 games made with SCI Companion. Frame count automatically determined, uses palette from frame 1 (subsequent palette changes are ignored)

SEQTool is free and open source. The source code is licensed under the GNU General Public License version 2 or later. 
