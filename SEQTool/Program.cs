﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Runtime.InteropServices;

namespace SEQTool
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// https://www.google.com/search?num=100&q=c%23+pass+argument+to+another+instance&oq=c%23+pass+argument+to+another+instance&gs_l=serp.3...17838.28359.0.29702.22.19.3.0.0.0.145.2074.0j18.18.0....0...1c.1.64.serp..1.9.1078.id3xFJlQdlE
        /// 
        /// http://stackoverflow.com/questions/7777727/pass-parameters-to-previously-started-program-instance
        /// http://sanity-free.org/143/csharp_dotnet_single_instance_application.html
        /// 
        /// http://www.uhlme.ch/c_sharp_pass_arguments_to_other_instances
        /// </summary>
        static Mutex mutex = new Mutex(true, "{0510f50c-e279-49ea-a40f-39bd76d009b9}");

        public static string Arg { get; set; }

        public static string seqPath;
        [STAThread]
        static void Main()
        {
            string[] args = Environment.GetCommandLineArgs();
            int i = 0;
            foreach (string arg in args)
            {
                i = i + 1;
                if (i == 2)
                {
                    // Open SEQ if argument is SEQ file path
                    if (arg.ToLower().Contains(".seq"))
                    {
                        MainFrm.seqFile = Path.GetFullPath(arg);
                    }
                    // Open SQP if argument is save file path
                    else if (arg.ToLower().Contains(".sqp"))
                    {
                        MainFrm.projFile = Path.GetFullPath(arg);
                    }
                    // If argument is game path
                    else
                    {
                        if (Directory.Exists(arg))
                        {
                            MainFrm.projPath = arg;
                        }
                    }
                }
            }

            if (mutex.WaitOne(TimeSpan.Zero, true))
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainFrm());
            }
            else
            {
                foreach (string arg in args)
                {
                    if (arg.ToLower().Contains(".seq"))
                    {
                        seqPath = Path.GetFullPath(arg);
                        string tempPath = Path.GetTempPath();

                        using (StreamWriter writer = new StreamWriter(tempPath + "argtmp"))
                        {
                            writer.Write(seqPath);
                            writer.Close();
                        }
                    }
                }

                // send our Win32 message to make the currently running instance
                // jump on top of all the other windows
                NativeMethods.PostMessage(
                    (IntPtr)NativeMethods.HWND_BROADCAST,
                    NativeMethods.WM_SHOWME,
                    IntPtr.Zero,
                    IntPtr.Zero);
            }
        }


        // this class just wraps some Win32 stuff that we're going to use
        internal class NativeMethods
        {
            public const int HWND_BROADCAST = 0xffff;
            public static readonly int WM_SHOWME = RegisterWindowMessage("WM_SHOWME");
            [DllImport("user32")]
            public static extern bool PostMessage(IntPtr hwnd, int msg, IntPtr wparam, IntPtr lparam);
            [DllImport("user32")]
            public static extern int RegisterWindowMessage(string message);
        }
    }
}




//bool result;
//static Mutex mutex = new Mutex(true, "{0510f50c-e279-49ea-a40f-39bd76d009b9}", out result); //"UniqueAppId"

//if (!result)
//{
//    //http://www.geekdroppings.com/2014/07/09/super-simple-single-instance-application-in-windows-using-c/
//    MessageBox.Show("Another instance is already running.");
//    return;
//}

//GC.KeepAlive(mutex); 


//string[] args = Environment.GetCommandLineArgs();

//int i = 0;
//foreach (string arg in args)
//{
//    //i = i + 1;
//    //if (i == 2)
//    //{
//        // Open SEQ if argument is SEQ file path
//        if (arg.ToLower().Contains(".seq"))
//        {
//            MainFrm.seqFile = Path.GetFullPath(arg);
//        }
//        // Open SQP if argument is save file path
//        else if (arg.ToLower().Contains(".sqp"))
//        {
//            MainFrm.projFile = Path.GetFullPath(arg);
//        }
//        // If argument is game path
//        else
//        {
//            if (Directory.Exists(arg))
//            {
//                MainFrm.projPath = arg;
//            }
//        }
//    //}
//}

//Application.EnableVisualStyles();
//Application.SetCompatibleTextRenderingDefault(false);
//Application.Run(new MainFrm());