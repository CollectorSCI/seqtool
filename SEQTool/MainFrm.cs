﻿/// To do:
/// Assemble export images into ani GIF. Export to AVI?
/// Reload thumbs after image edit?
/// 

using Kawa.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using System.IO.MemoryMappedFiles;
using System.Globalization;
using Microsoft.Win32;
using System.Reflection;

namespace SEQTool
{
    /// <summary>
    /// SCI SEQ Tool - Plugin for SCI Companion
    /// Creates SCI SEQ videos from 320 by 200 pixel 256-color uncompressed images.
    /// 
    /// Uses Kawa's Kawa.Tools
    /// </summary>

    public partial class MainFrm : Form
    {
        #region Declares, Imports. etc.

        #region Application variables

        private string appTitle = "SEQTool";

        public static bool seqSubDir;
        private static bool drag = false;
        public static bool seqLoaded = false;
        private bool thumbDrag = false;
        protected bool validData;

        private int swapDragIndex;
        int newSelectedIdx;
        int draggedThumbIdx;

        //private static List<string> cleanupList = new List<string>();

        #region Drag&Drop Variables

        protected Image image;
        protected Image nextImage;

        protected int lastX = 0;
        protected int lastY = 0;
        private Point listLastMouseCoords;
        private Point thumbsLastMouseCoords;

        [DllImport("User32.dll")]
        public static extern Int32 SetForegroundWindow(int hWnd);

        internal enum MoveFileFlags
        {
            MOVEFILE_REPLACE_EXISTING = 1,
            MOVEFILE_COPY_ALLOWED = 2,
            MOVEFILE_DELAY_UNTIL_REBOOT = 4,
            MOVEFILE_WRITE_THROUGH = 8
        }

        // Mark file for deletion on reboot
        [System.Runtime.InteropServices.DllImportAttribute("kernel32.dll", EntryPoint = "MoveFileEx")]
        internal static extern bool MoveFileEx(string lpExistingFileName, string lpNewFileName,
        MoveFileFlags dwFlags);

        protected DragDropEffects effect;
        protected Thread getImageThread;

        public delegate void AssignImageDlgt();

        PictureBox clickedThumb;

        #endregion Drag&Drop Variables

        ErrorLogging errorLogging = new ErrorLogging();
        ShellClass shellClass = new ShellClass();

        #endregion Application variables

        #region Project variables

        public static string projName = null;
        public static string projPath;
        //public static string projPath = "E:\\Games\\SCIQuest"; // <========================== populate for testing
        public static string projFile; // = projPath + "\\" + Directory.GetParent(projPath).Name + ".sqp";
        public static string seqName;
        public static string seqFile;
        private string previousSelectedItem;
        private string clickedFrameImg;
        string dragDestination;
        protected string lastFilename = String.Empty;
        public string exportFormat;
        public string exportPath;
        private string playMode = null;
        private string currentName;

        public static bool? aspect = null;

        #endregion Project variables

        #region tracking variables

        private string trackingName;
        private string changedName;
        private bool open = false;
        List<string> changedImgList = new List<string>();
        List<string> trackingImgList = new List<string>();
        
        private string trackingMD5;
        private string changedMD5;
        private string trackedIMG;

        #endregion tracking variables

        #region play variables

        private BinaryReader fileStream;
        //private int[] palette;
        private byte[] screen;
        private bool exam;

        private int frameCount, curFrame;
        private string outputFrame = null;

        private Bitmap bitmap;

        public static List<string> frames = new List<string>();

        #endregion play variables

        #region Import variables

        private static string aniGIFPath;
        private static Image[] gifFrames;
        private bool extracted = false;

        #endregion Import variables

        #endregion Declares, Imports. etc.


        public MainFrm()
        {
            InitializeComponent();
        }


        #region Form Events

        /// <summary>
        /// Get initial settings
        /// </summary>
        private void Form_Load(object sender, EventArgs e)
        {
            #region Get last screen metrics

            if (!Properties.Settings.Default.frmLocation.IsEmpty)
                this.Location = Properties.Settings.Default.frmLocation;
            else
                this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;

            if (!Properties.Settings.Default.frmSize.IsEmpty)
                this.Size = Properties.Settings.Default.frmSize;

            #endregion Get last screen metrics

            // Get aspect preference
            GetAspect();

            SetAspect();

            // Get last viewMenuExamMode CheckBox state
            viewMenuExamMode.Checked = Properties.Settings.Default.exMode;

            // Set tracking image list to compare against changes
            trackingImgList.Clear();
            foreach (string listImg in frameListBox.Items)
                trackingImgList.Add(listImg);
            changedImgList = trackingImgList;

            // Set tracking SEQ name
            trackingName = seqNameTxtBox.Text;
            changedName = trackingName;

            // Load SEQ if seqFile exists
            SEQNameLbl.Text = "";
            if (File.Exists(seqFile))
            {
                playBtn.Enabled = true;
                playCntxtMenu.Enabled = true;
                editSEQCntxtMenu.Enabled = true;
                SEQNameLbl.Text = Path.GetFileName(seqFile);
                Play(seqFile);
            }

            // Load project if projFile exists
            if (File.Exists(projFile))
                ReadSaveFile();

            ManageButtons();
        }


        /// <summary>
        /// Check editied images
        /// </summary>
        private void Form_Activated(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(trackedIMG))
            {
                //// Check if file is loked
                //FileInfo fi = new FileInfo(trackedIMG);
                //bool inuse = FileMethods.FileInUse(fi);

                changedMD5 = FileMethods.GetMD5Hash(trackedIMG);

                // Check if editied image has changed
                if (changedMD5 != trackingMD5)
                {
                    // Enable saveSEQ controls if at least 2 images in list and SEQ file loaded
                    if (frameListBox.Items.Count > 1 && File.Exists(seqFile))
                    {
                        fileMenuSaveSEQ.Enabled = true;
                        //saveSEQBtn.Enabled = true;
                    }
                    else
                    {
                        fileMenuSaveSEQ.Enabled = false;
                        saveSEQBtn.Enabled = false;
                    }
                }
            }
        }


        /// <summary>
        /// Save setting before closing
        /// </summary>
        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            timer.Stop();
            
            bool changed = false;
            if (trackingImgList != changedImgList)
                changed = true;
            if (trackingName != changedName)
                changed = true;

            if (changed == true)
            {
                DialogResult dialogResult = MessageBox.Show("Would you like to save your changes?", "Project Changed", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                    Save();
                else if (dialogResult == DialogResult.Cancel)
                    e.Cancel = true;
            }

            //// Clean up any temp import files
            //foreach (string file in cleanupList)
            //    if (File.Exists(file))
            //        try
            //        {
            //            File.Delete(file);
            //        }
            //        catch
            //        {
            //            // mark for deletion after reboot
            //            MoveFileEx(file, null, MoveFileFlags.MOVEFILE_DELAY_UNTIL_REBOOT);
            //        }

            // Save screen metrics
            Properties.Settings.Default.frmLocation = this.Location;
            Properties.Settings.Default.frmSize = this.Size;

            // Save screen metrics
            Properties.Settings.Default.Save();
        }

        #endregion Form Events


        #region Menubar

        #region File Menu

        /// <summary>
        /// Clears image list & thumbs and nulls all values for new project
        /// </summary>
        private void fileMenuNew_Click(object sender, EventArgs e)
        {
            New();
        }


        /// <summary>
        /// Opens a browse to file dialog to open existing project file
        /// </summary>
        private void fileMenuOpen_Click(object sender, EventArgs e)
        {
            Open();
        }


        /// <summary>
        /// Saves current project
        /// </summary>
        private void fileMenuSave_Click(object sender, EventArgs e)
        {
            Save();
        }


        /// <summary>
        /// Opens a save file dialog to save current project to a new name
        /// </summary>
        private void fileMenuSaveAs_Click(object sender, EventArgs e)
        {
            SaveAs();
        }


        /// <summary>
        /// Saves current SEQ
        /// </summary>
        private void fileMenuSaveSEQ_Click(object sender, EventArgs e)
        {
            SaveSEQ();
        }


        /// <summary>
        /// Opens a save file dialog to save current SEQ to a new name
        /// </summary>
        private void fileMenuSaveSEQAs_Click(object sender, EventArgs e)
        {
            SaveSEQAs();
        }


        /// <summary>
        /// Opens a file browser dialog to select animated GIF to import
        /// </summary>
        private void fileMenuImport_Click(object sender, EventArgs e)
        {
            Import();
        }


        /// <summary>
        /// Opens a save folder browser dialog to Export current SEQ file
        /// </summary>
        private void fileMenuExport_Click(object sender, EventArgs e)
        {
            if (!File.Exists(seqFile)) return;

            //ExportFrm exportFrm = new ExportFrm();
            //exportFrm.Show();
            Export();
        }


        /// <summary>
        /// Closes Form
        /// </summary>
        private void fileMenuExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion File Menu


        #region View Menu

        /// <summary>
        /// Sets aspect corrected playback
        /// </summary>
        private void viewMenuAspectCorrection_CheckedChanged(object sender, EventArgs e)
        {
            if (viewMenuAspectCorrection.Checked == false)
                aspect = false;

            if (viewMenuAspectCorrection.Checked == true)
                aspect = true;

            SetAspect();
        }


        /// <summary>
        /// Sets examination mode playback
        /// </summary>
        private void viewMenuExamMode_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.exMode = exam = viewMenuExamMode.Checked;
        }

        #endregion View Menu


        #region Tools Menu

        /// <summary>
        /// Selects if movieDir location will be in SEQ subfolder
        /// </summary>
        private void toolsMenuUseSubDir_CheckStateChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.useSubDir = toolsMenuUseSubDir.Checked;
        }

        /// <summary>
        /// Add movieDir line to RESOURCE.CFG
        /// </summary>
        private void toolsMenuAppendCFG_Click(object sender, EventArgs e)
        {
            AppendResCFG();
        }


        /// <summary>
        /// Opens options dialog
        ///     remove?
        /// </summary>
        private void toolsMenuOptions_Click(object sender, EventArgs e)
        {
            //OptionsFrm optionsFrm = new OptionsFrm();
            //optionsFrm.ShowDialog();
        }


        /// <summary>
        /// Registers SEQ files to open in SEQTool
        /// </summary>
        private void toolsMenuRegisterSEQ_Click(object sender, EventArgs e)
        {
            AddSEQReg();
        }


        /// <summary>
        /// Registers SQP project files to open in SEQTool
        /// </summary>
        private void toolsMenuRegisterSQP_Click(object sender, EventArgs e)
        {
            AddSQPReg();
        }

        #endregion Tools Menu


        #region Help Menu

        /// <summary>
        /// Show Help File's contents list
        /// </summary>
        private void helpMenuContents_Click(object sender, EventArgs e)
        {
            //if (File.Exists("Documents\\SCI SEQ Tool Help.chm"))
            //    Help.ShowHelp(this, helpProvider.HelpNamespace, HelpNavigator.TableOfContents);
            //else
            //    MessageBox.Show("SCI Seq Tool Help File not found.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }


        /// <summary>
        /// Opens Help File index
        /// </summary>
        private void helpMenuIndex_Click(object sender, EventArgs e)
        {
            //if (File.Exists("Documents\\SCI SEQ Tool Help.chm"))
            //    Help.ShowHelp(this, helpProvider.HelpNamespace, HelpNavigator.Index);
            //else
            //    MessageBox.Show("SCI Seq Tool Help File not found.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }


        /// <summary>
        /// Opens Help File search
        /// </summary>
        private void helpMenuSearch_Click(object sender, EventArgs e)
        {
            //if (File.Exists("Documents\\SCI SEQ Tool Help.chm"))
            //    Help.ShowHelp(this, helpProvider.HelpNamespace, HelpNavigator.Find, "");
            //else
            //    MessageBox.Show("SCI Seq Tool Help File not found.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }


        /// <summary>
        /// Go to SCI Community forum
        /// </summary>
        private void helpMenuSCICommunity_Click(object sender, EventArgs e)
        {
            Process.Start("http://sciprogramming.com/community/");
        }


        /// <summary>
        /// Go to the SCI Wiki
        /// </summary>
        private void helpMenuSCIWiki_Click(object sender, EventArgs e)
        {
            Process.Start("http://sciwiki.sierrahelp.com/index.php?title=Main_Page");
        }


        /// <summary>
        /// Open About form
        /// </summary>
        private void helpMenuAbout_Click(object sender, EventArgs e)
        {
            AboutBox aboutBox = new AboutBox();
            aboutBox.ShowDialog();
        }

        #endregion Help Menu

        #endregion Menubar


        #region Main Toolbar

        /// <summary>
        /// Opens the New SEQ file dialog
        /// </summary>
        private void newBtn_Click(object sender, EventArgs e)
        {
            New();
        }


        /// <summary>
        /// Opens a file browser to open an existing SEQ file
        /// </summary>
        private void openBtn_Click(object sender, EventArgs e)
        {
            Open();
        }


        /// <summary>
        /// Saves current SEQ file
        /// </summary>
        private void saveBtn_Click(object sender, EventArgs e)
        {
            Save();
        }


        /// <summary>
        /// Opens a save file dialog to save current SEQ file with a new name
        /// </summary>
        private void saveAsBtn_Click(object sender, EventArgs e)
        {
            SaveAs();
        }


        /// <summary>
        /// Opens a file browser dialog to select animated GIF to import
        /// </summary>
        private void importBtn_Click(object sender, EventArgs e)
        {
            Import();
        }


        /// <summary>
        /// Opens a save folder browser dialog to Export current SEQ file
        /// </summary>
        private void exportBtn_Click(object sender, EventArgs e)
        {
            if (!File.Exists(seqFile)) return;

            //ExportFrm exportFrm = new ExportFrm();
            //exportFrm.Show();
            Export();
        }


        /// <summary>
        /// Add movieDir line to RESOURCE.CFG
        /// </summary>
        private void appendCFGBtn_Click(object sender, EventArgs e)
        {
            AppendResCFG();
        }

        #endregion Main Toolbar


        #region SEQ Toolbar

        /// <summary>
        /// Invokes OpenSEQ() to open SEQ in Player
        /// </summary>
        private void openSEQBtn_Click(object sender, EventArgs e)
        {
            OpenSEQ();
        }


        /// <summary>
        /// Saves current SEQ
        /// </summary>
        private void saveSEQBtn_Click(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// Invokes SaveSEQAs() to saves current to a new file
        /// </summary>
        private void saveSEQAsBtn_Click(object sender, EventArgs e)
        {
            SaveSEQAs();
        }


        /// <summary>
        /// Invokes Extract() to extract frames for editing
        /// </summary>
        private void editSEQBtn_Click(object sender, EventArgs e)
        {
            Extract();
        }

        #endregion SEQ Toolbar


        #region Frame List

        #region Button Events

        /// <summary>
        /// Moves selectd item up one place
        /// </summary>
        private void upBtn_Click(object sender, EventArgs e)
        {
            // Return if no item is selected
            if (frameListBox.SelectedIndex == -1) return;

            // Return if selected item is already at top of list
            if (frameListBox.SelectedIndex == 0) return;

            // Move item up one
            string item = frameListBox.SelectedItem.ToString(); // Store the Selected Item
            int index = frameListBox.SelectedIndex; // Get Index of Selected Item
            index = index - 1; // Decrease Index by one
            frameListBox.Items.RemoveAt(frameListBox.SelectedIndex); // Remove at Old Index
            frameListBox.Items.Insert(index, item); // Reinsert one Index Up
            frameListBox.SelectedIndex = index; // Reselect Item at New Index

            // Rearrange thumbnails to match
            AddThumbs();
        }


        /// <summary>
        /// Moves selectd item down one place
        /// </summary>
        private void dnBtn_Click(object sender, EventArgs e)
        {
            // Return if no item is selected
            if (frameListBox.SelectedIndex == -1) return;

            // Return if selected item is already at bottom of list
            if (frameListBox.SelectedIndex == frameListBox.Items.Count + 1) return;

            // Move item down one
            string item = frameListBox.SelectedItem.ToString(); // Store the Selected Item
            int index = frameListBox.SelectedIndex; // Get Index of Selected Item
            index = index + 1; // Increase Index by one
            frameListBox.Items.RemoveAt(frameListBox.SelectedIndex); // Remove at Old Index
            frameListBox.Items.Insert(index, item); // Reinsert one Index Down
            frameListBox.SelectedIndex = index; // Reselect Item at New Index

            // Rearrange thumbnails to match
            AddThumbs();
        }


        /// <summary>
        /// Adds new image(s) to frame list
        /// </summary>
        private void addBtn_Click(object sender, EventArgs e)
        {
            AddImg();
        }


        /// <summary>
        /// Removes selectd item 
        /// </summary>
        private void removeBtn_Click(object sender, EventArgs e)
        {
            RemoveImg();
        }


        /// <summary>
        /// Clears image list
        /// </summary>
        private void clearAllBtn_Click(object sender, EventArgs e)
        {
            ClearAllImgs();
        }


        /// <summary>
        /// Calls MakeSeq method
        /// </summary>
        private void makeSEQBtn_Click(object sender, EventArgs e)
        {
            List<string> inFiles = new List<string>();
            foreach (string item in frameListBox.Items)
                inFiles.Add(item);

            // Trim user added extension to avoid double extention
            if (!String.IsNullOrEmpty(seqNameTxtBox.Text))
            {
                seqNameTxtBox.Text = seqNameTxtBox.Text.ToUpper();
                if (seqNameTxtBox.Text.EndsWith(".SEQ"))
                    seqNameTxtBox.Text = seqNameTxtBox.Text.Replace(".SEQ", "");
            }

            MakeSeq(inFiles);
        }

        #endregion Button Events


        #region frameListBox

        /// <summary>
        /// Select thumb on frameListBox SelectedIndexChanged
        /// </summary>
        private void frameListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            // If no item selected, return
            if (frameListBox.SelectedIndex == -1)
            {
                ManageButtons();
                return;
            }

            // Clear thumb selection
            foreach (Control thumb in thumbPreviewPnl.Controls)
                thumb.BackColor = Color.Transparent;

            // Select and highlight corresponding thumb
            string imgFile = frameListBox.SelectedItem.ToString();
            foreach (Control thumb in thumbPreviewPnl.Controls)
            {
                if (imgFile == ((PictureBox)thumb).ImageLocation)
                {
                    thumb.BackColor = System.Drawing.Color.Blue;

                    // Temporarily shift focus to show selected thumb
                    thumb.Focus();

                    // Shift focus back to frameListBox
                    frameListBox.Focus();
                }
            }

            // If SEQ is not playing, display selected in seqPicBox
            if (playBtn.Enabled == true || !File.Exists(seqFile))
            {
                Bitmap img = LoadBitmapNolock(frameListBox.SelectedItem.ToString());
                string imgName = Path.GetFileName(frameListBox.SelectedItem.ToString());
                if (File.Exists(frameListBox.SelectedItem.ToString()))
                {
                    seekBar.Value = 0;
                    seqPicBox.Image = img;
                    SEQNameLbl.Text = imgName;
                }

                seqPicBox.SizeMode = PictureBoxSizeMode.StretchImage;
            }

            ManageButtons();
        }


        /// <summary>
        /// Changes cursor to drag cursor and checks data being dragged for right types
        /// </summary>
        private void frameListBox_DragEnter(object sender, DragEventArgs e)
        {
            dragDestination = frameListBox.Name;
            Debug.WriteLine("OnDragEnter");
            string filename;
            validData = GetFilename(out filename, e);
            if (validData)
            {
                if (lastFilename != filename)
                {
                    listThumbPicBox.Image = null;
                    listThumbPicBox.Visible = false;
                    lastFilename = filename;
                    getImageThread = new Thread(new ThreadStart(LoadImage));
                    getImageThread.Start();
                }
                else
                {
                    listThumbPicBox.Visible = true;
                }
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }


        /// <summary>
        /// Get cursor location
        /// </summary>
        private void frameListBox_DragOver(object sender, DragEventArgs e)
        {
            Debug.WriteLine("OnDragOver");
            if (validData)
            {
                if ((e.X != lastX) || (e.Y != lastY))
                {
                    SetThumbnailLocation(this.PointToClient(new Point(e.X, e.Y)));
                }
            }
        }


        /// <summary>
        /// Adds new images on Drag & Drop
        /// </summary>
        private void frameListBox_DragDrop(object sender, DragEventArgs e)
        {
            // Release dragged thumbnail cursor
            Debug.WriteLine("OnDragDrop");
            if (validData)
            {
                while (getImageThread.IsAlive)
                {
                    Application.DoEvents();
                    Thread.Sleep(0);
                }
                listThumbPicBox.Visible = false;
                image = nextImage;
            }

            #region Process Drop Files

            string img = null;
            int numbFiles = 0;

            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] droppedFiles = (string[])e.Data.GetData(DataFormats.FileDrop);
                foreach (string filename in droppedFiles)
                {
                    FileAttributes attr = System.IO.File.GetAttributes(filename);

                    // If Item Dropped Is Folder
                    if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                    {
                        string[] objs = (string[])e.Data.GetData(DataFormats.FileDrop);

                        foreach (string obj in objs)
                        {
                            DirectoryInfo dirInf = new DirectoryInfo(obj);

                            FileInfo[] dirFileArr = dirInf.GetFiles("*.*");

                            // list the names of all files in the specified directory
                            foreach (FileInfo dirFile in dirFileArr)
                            {
                                // Add file(s) to list if image
                                string ext = Path.GetExtension(filename).ToLower();
                                if (ext == ".pcx" || ext == ".gif" || ext == ".bmp" || ext == ".tif" || ext == ".tiff" || ext == ".png")
                                {
                                    this.statusbarMainMessage.Text = "Adding images in " + obj.ToString();

                                    frameListBox.Items.Add(filename);

                                    // Count number of files added
                                    numbFiles = ++numbFiles;
                                }
                            }

                            // Bring app to front after drop
                            SetForegroundWindow(Handle.ToInt32());
                        }
                    }
                    else // Dropped item Is file
                    {
                        // Add file(s) to list if image
                        string ext = Path.GetExtension(filename).ToLower();
                        if (ext == ".pcx" || ext == ".gif" || ext == ".bmp" || ext == ".tif" || ext == ".tiff" || ext == ".png")
                        {
                            this.statusbarMainMessage.Text = "Adding images";

                            frameListBox.Items.Add(filename);

                            // Count number of files added
                            numbFiles = ++numbFiles;
                        }

                        // Bring app to front after drop
                        SetForegroundWindow(Handle.ToInt32());
                    }
                }
            }

            // Add/Rearrange thumbnails to match
            AddThumbs();

            // Set Statusbar Message
            if (numbFiles >= 0)
                this.statusbarMainMessage.Text = "No images added";
            if (numbFiles == 1)
                this.statusbarMainMessage.Text = img + " added";
            if (numbFiles > 1)
                this.statusbarMainMessage.Text = numbFiles + " images added";

            #endregion Process Drop Files

            // Set tracking image list to compare against changes
            if (trackingImgList == null)
            {
                foreach (string listImg in frameListBox.Items)
                    trackingImgList.Add(listImg);
                changedImgList = trackingImgList;
            }
            else
            {
                changedImgList.Clear();
                foreach (string listImg in frameListBox.Items)
                    changedImgList.Add(listImg);
            }

            ManageButtons();
            statusTimer.Start();
        }


        /// <summary>
        /// 
        /// </summary>
        private void frameListBox_DragLeave(object sender, EventArgs e)
        {
            Debug.WriteLine("OnDragLeave");
            listThumbPicBox.Visible = false;
        }


        /// <summary>
        /// Give frameListBox focus on mouseover
        /// </summary>
        private void frameListBox_MouseHover(object sender, EventArgs e)
        {
            //ResetNullName();
            frameListBox.Focus();
        }


        /// <summary>
        /// Gets item under cursor
        /// </summary>
        private void frameListBox_MouseDown(object sender, MouseEventArgs e)
        {
            int tempIndex = 0;

            // Start Swap operation if Left mouse button is down
            if (e.Button == MouseButtons.Left)
            {
                listLastMouseCoords = this.frameListBox.PointToClient(Control.MousePosition);

                // Get the index of the item the mouse is hovering above
                tempIndex = frameListBox.IndexFromPoint(e.X, e.Y);

                // Index exists in the list
                if ((tempIndex != ListBox.NoMatches))
                    swapDragIndex = tempIndex;
            }

            if (e.Button == MouseButtons.None) return;

            ListBox sourceControl = (ListBox)sender;

            // Select Corresponding Item
            sourceControl.SelectedIndex = sourceControl.IndexFromPoint(e.X, e.Y);

            ManageButtons();
        }


        /// <summary>
        /// Swaps ListBox itme position on drag
        /// </summary>
        private void frameListBox_MouseMove(object sender, MouseEventArgs e)
        {
            #region frameListBox tooltip

            // Generate tooltips for each item in list
            if (e.Button == MouseButtons.None)
            {
                Point listMouseCoords = this.frameListBox.PointToClient(Control.MousePosition);

                // Set minimum mouse movement to prevent flashing
                if (!(listMouseCoords.Y > listLastMouseCoords.Y - 5 &
                    listMouseCoords.Y < listLastMouseCoords.Y + 5 &
                    listMouseCoords.X > listLastMouseCoords.X - 10 &
                    listMouseCoords.X < listLastMouseCoords.X + 10))
                {
                    listLastMouseCoords = listMouseCoords; //Save the current mouse position as the last mouse position
                    int indexUnderTheMouse = this.frameListBox.IndexFromPoint(listMouseCoords);

                    // If there is an item under mouse
                    if (indexUnderTheMouse > -1)
                    {
                        string name = this.frameListBox.Items[indexUnderTheMouse].ToString();
                        string itmName = Path.GetFileName(frameListBox.Items[indexUnderTheMouse].ToString());
                        Graphics g = this.frameListBox.CreateGraphics();

                        if (g.MeasureString(name, this.frameListBox.Font).Width > this.frameListBox.ClientRectangle.Width)
                        {
                            this.toolTip.SetToolTip(this.frameListBox, itmName);
                        }
                        else
                        {
                            foreach (var itm in frameListBox.Items)
                            {
                                this.toolTip.SetToolTip(frameListBox, itmName);
                            }
                        }
                        g.Dispose();
                    }
                    else
                        this.toolTip.SetToolTip(this.frameListBox, null);
                }
                return;
            }

            #endregion frameListBox tooltip

            // If none selected, return
            if (frameListBox.SelectedIndex == -1) return;

            #region Move Selected Item

            // Start Swap operation if Left mouse button is down
            if (e.Button == MouseButtons.Left)
            {
                Point listMouseCoords = this.frameListBox.PointToClient(Control.MousePosition);

                // Disallow cursor change if less than minimum mouse movement
                if (!(listMouseCoords.Y > listLastMouseCoords.Y - 5 &
                    listMouseCoords.Y < listLastMouseCoords.Y + 5 &
                    listMouseCoords.X > listLastMouseCoords.X - 10 &
                    listMouseCoords.X < listLastMouseCoords.X + 10))
                {
                    listLastMouseCoords = listMouseCoords; //Save the current mouse position as the last mouse position

                    MemoryStream move = new MemoryStream(Properties.Resources.CursorMove);
                    this.Cursor = new Cursor(move);
                    Cursor.CopyHandle();

                    if (swapDragIndex != ListBox.NoMatches)
                    {
                        int tempIndex = 0;
                        object item = null;
                        drag = true;

                        // Check position of mouse
                        tempIndex = frameListBox.IndexFromPoint(e.X, e.Y);

                        if (tempIndex != ListBox.NoMatches & tempIndex != swapDragIndex)
                        {
                            // Store the Dragged Item
                            item = frameListBox.Items[swapDragIndex];

                            // Remove the Dragged Item
                            frameListBox.Items.RemoveAt(swapDragIndex);

                            // Re-Insert the Dragged Item
                            frameListBox.Items.Insert(tempIndex, item);

                            // Reset Drag Index
                            swapDragIndex = tempIndex;

                            // Select Dragged Index
                            frameListBox.SelectedIndex = swapDragIndex;
                            newSelectedIdx = swapDragIndex;
                        }

                        ManageButtons();
                    }
                }
            }
            
            #endregion Move Selected Item
        }


        /// <summary>
        /// Gets position when mouse has released item
        /// </summary>
        private void frameListBox_MouseUp(object sender, MouseEventArgs e)
        {
            // Restore Cursor When Mouse Button Released
            if (this.frameListBox.SelectedIndex == -1)
            {
                this.Cursor = Cursors.Arrow;
                return;
            }

            // Set PreviousSelectedItem to currently selected item
            if (frameListBox.SelectedItems.Count >= 1)
                previousSelectedItem = frameListBox.SelectedItem.ToString();

            // Clear Swap operation index
            swapDragIndex = -1;

            // If moving frame in list, rearrange thumbnails to match
            if (drag == true)
            {
                // Set tracking image list to compare against changes
                changedImgList.Clear();
                foreach (string listImg in frameListBox.Items)
                    changedImgList.Add(listImg);

                AddThumbs();
                drag = false;

                // Select and highlight corresponding thumb
                string imgFile = frameListBox.SelectedItem.ToString();
                foreach (Control thumb in thumbPreviewPnl.Controls)
                {
                    if (imgFile == ((PictureBox)thumb).ImageLocation)
                    {
                        thumb.BackColor = System.Drawing.Color.Blue;
                        thumb.Focus();
                    }
                }
            }

            // Restore Cursor When Mouse Button Released
            this.Cursor = Cursors.Arrow;

            ManageButtons();
        }


        /// <summary>
        /// Reset null SEQ name
        /// </summary>
        private void frameListBox_Click(object sender, EventArgs e)
        {
            ResetNullName();
            frameListBox.Focus();
        }


        /// <summary>
        /// Opens selected image in default viewer
        /// </summary>
        private void frameListBox_DoubleClick(object sender, EventArgs e)
        {
            if (this.frameListBox.SelectedIndex == -1)
            {
                // Restore Cursor When Mouse Button Released
                this.Cursor = Cursors.Arrow;
                return;
            }

            ViewImg(frameListBox.SelectedItem.ToString());
        }


        /// <summary>
        /// Simulate side-to-side scroll for thumbnails with cursor keys
        /// </summary>
        private void frameListBox_KeyDown(object sender, KeyEventArgs e)
        {
            // If none selected, return
            if (frameListBox.SelectedIndex == -1) return;

            if (e.KeyData == Keys.Left)
            {
                // Return if first item is selected
                if (frameListBox.SelectedIndex == 0)
                {
                    e.SuppressKeyPress = true;
                    return;
                }

                // Decrement selection left
                int i = frameListBox.SelectedIndex - 1;
                frameListBox.SelectedIndex = i;
                frameListBox.Focus();
                e.SuppressKeyPress = true;
            }

            if (e.KeyData == Keys.Right)
            {
                // Return if last item is selected
                if (frameListBox.SelectedIndex == frameListBox.Items.Count - 1)
                {
                    e.SuppressKeyPress = true;
                    return;
                }

                // Increment selection right
                int i = frameListBox.SelectedIndex + 1;
                frameListBox.SelectedIndex = i;
                frameListBox.Focus();
                e.SuppressKeyPress = true;
            }
        }

        #endregion frameListBox


        #region seqNameTxtBox Events

        /// <summary>
        /// Shift focus from seqNameTxtBox on click
        /// </summary>
        private void seqNamePanel_Click(object sender, EventArgs e)
        {
            seqNameTxtBox_MouseDown(null, null);
            seqNameTxtBox.Focus();
        }


        /// <summary>
        /// Set entry to DOS 8.3 naming convention
        /// </summary>
        private void seqNameTxtBox_TextChanged(object sender, EventArgs e)
        {
            // Get caret location
            int i = seqNameTxtBox.SelectionStart;

            // Set SEQ Name to DOS 8.3 name
            if (!String.IsNullOrEmpty(seqNameTxtBox.Text) && seqNameTxtBox.Text != "SEQ Name   ")
            {
                string dosName;
                dosName = seqNameTxtBox.Text.Trim();
                Regex rgx = new Regex("[^a-zA-Z0-9]");
                dosName = rgx.Replace(seqNameTxtBox.Text, "");
                dosName = dosName.Replace(" ", null);
                dosName = dosName.Replace("-", null);
                dosName = dosName.Replace("'", null);
                dosName = dosName.ToUpper();

                seqNameTxtBox.Text = dosName;
            }

            // Reset caret location
            seqNameTxtBox.SelectionStart = i;

            // Set changed SEQ name for tracking
            changedName = seqNameTxtBox.Text;

            ManageButtons();
        }


        /// <summary>
        /// Clears SEQ name hint
        /// </summary>
        private void seqNameTxtBox_MouseDown(object sender, MouseEventArgs e)
        {
            //ResetNullName();
            if (seqNameTxtBox.Text == "SEQ Name   ")
            {
                seqNameTxtBox.Text = null;
                seqNameTxtBox.ForeColor = System.Drawing.SystemColors.WindowText;
                seqLbl.Text = ".SEQ";
            }
        }


        /// <summary>
        /// Get existing seqName on seqNameTxtBox Enter Event for tracking
        /// </summary>
        private void seqNameTxtBox_Enter(object sender, EventArgs e)
        {
            currentName = seqNameTxtBox.Text;
        }


        /// <summary>
        /// Handle 'Enter' and 'Escape' keypresses
        /// </summary>
        private void seqNameTxtBox_KeyDown(object sender, KeyEventArgs e)
        {
            // Reset seqNameTxtBox if Escape pressed
            if (e.KeyData == Keys.Escape)
            {
                if (String.IsNullOrEmpty(currentName) || currentName == "SEQ Name   ")
                {
                    seqNameTxtBox.Text = null;
                    ResetNullName();
                }
                else if (seqNameTxtBox.Text != currentName)
                {
                    seqNameTxtBox.Text = currentName;
                }

                frameListBox.Focus();
                e.Handled = e.SuppressKeyPress = true;
            }

            // Accept seqNameTxtBoxText if Enter is pressed
            if (e.KeyData == Keys.Enter)
            {
                frameListBox.Focus();
                ResetNullName();
                e.Handled = e.SuppressKeyPress = true;
            }
        }


        /// <summary>
        /// Prevent SEQ name of more than 8 characters
        /// </summary>
        private void seqNameTxtBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Return if SEQ name is 8 characters long
            if (!char.IsControl(e.KeyChar) && seqNameTxtBox.Text.Length > 7)
                e.Handled = true;

        }


        /// <summary>
        /// Restores SEQ name hint if empty
        /// </summary>
        private void seqNameTxtBox_LostFocus(object sender, EventArgs e)
        {
            //ResetNullName();
        }


        /// <summary>
        /// Move focus to seqNameTxtBox on seqLbl click
        /// </summary>
        private void seqLbl_Click(object sender, EventArgs e)
        {
            seqNameTxtBox_MouseDown(null, null);
            seqNameTxtBox.Focus();
        }


        /// <summary>
        /// Move focus to seqNameTxtBox on seqLbl click
        /// </summary>
        private void seqLbl_MouseDown(object sender, MouseEventArgs e)
        {
            seqNameTxtBox_MouseDown(null, null);
            seqNameTxtBox.Focus();
        }


        /// <summary>
        /// Change cursor to I beam for asthetic effect
        /// </summary>
        private void seqLbl_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.IBeam;
        }


        /// <summary>
        /// Change cursor to I beam for asthetic effect
        /// </summary>
        private void seqNamePanel_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.IBeam;
        }


        /// <summary>
        /// Reset cursor
        /// </summary>
        private void seqLbl_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }

        #endregion seqNameTxtBox Events

        #endregion Frame List


        #region thumbPreviewPnl

        /// <summary>
        /// Changes cursor to drag cursor and checks data being dragged for right types
        /// </summary>
        private void thumbPreviewPnl_DragEnter(object sender, DragEventArgs e)
        {
            dragDestination = thumbPreviewPnl.Name;
            Debug.WriteLine("OnDragEnter");
            string filename;

            if (thumbDrag == false)
            {
                validData = GetFilename(out filename, e);
                if (validData)
                {
                    if (lastFilename != filename)
                    {
                        previewThumbPicBox.Image = null;
                        previewThumbPicBox.Visible = false;
                        lastFilename = filename;
                        getImageThread = new Thread(new ThreadStart(LoadImage));
                        getImageThread.Start();
                    }
                    else
                    {
                        previewThumbPicBox.Visible = true;
                    }
                    e.Effect = DragDropEffects.Copy;
                }
                else
                {
                    e.Effect = DragDropEffects.None;
                }
            }
            else
            {
                //clickedThumb = (PictureBox)sender;
                filename = clickedThumb.ImageLocation;
                sender = filename;

                if (lastFilename != filename)
                {
                    previewThumbPicBox.Image = null;
                    previewThumbPicBox.Visible = false;
                    lastFilename = filename;
                    getImageThread = new Thread(new ThreadStart(LoadImage));
                    getImageThread.Start();
                }
                else
                {
                    previewThumbPicBox.Visible = true;
                }

                e.Effect = DragDropEffects.Move;
            }
        }


        /// <summary>
        /// Get cursor location to set dragged thumb location
        /// </summary>
        private void thumbPreviewPnl_DragOver(object sender, DragEventArgs e)
        {
            Debug.WriteLine("OnDragOver");
            if (validData)
            {
                if ((e.X != lastX) || (e.Y != lastY))
                {
                    SetThumbnailLocation(this.PointToClient(new Point(e.X, e.Y)));
                }
            }
        }


        /// <summary>
        /// Adds new images on Drag & Drop
        /// </summary>
        private void thumbPreviewPnl_DragDrop(object sender, DragEventArgs e)
        {
            Debug.WriteLine("OnDragDrop");
            if (validData)
            {
                while (getImageThread.IsAlive)
                {
                    Application.DoEvents();
                    Thread.Sleep(0);
                }
                previewThumbPicBox.Visible = false;
                image = nextImage;
            }

            #region thunbnail cursor (disabled due to flashing)

            //PictureBox clickedThumb = (PictureBox)sender;
            //string imgHov = null;
            //Control hoveredThumb = FindControlAtCursor(this);
            //if (hoveredThumb is PictureBox)
            //{
            //    imgHov = ((PictureBox)hoveredThumb).ImageLocation;
            //    int thumbIndex = thumbPreviewPnl.Controls.GetChildIndex(hoveredThumb);
            //    //int thumbIndex = ((PictureBox)hoveredThumb).GetChildIndex(clickedThumb);
            //    MessageBox.Show(imgHov);
            //    //MessageBox.Show(thumbIndex.ToString());
            //}

            //foreach (Control child in this.thumbPreviewPnl.Controls)
            //{
            //    //child.MouseDown += new System.EventHandler(this.childs_MouseDown);
            //}

            //Control source = (Control)e.Data.GetData(dragtype);

            //Control target = this.thumbPreviewPnl.GetChildAtPoint(this.thumbPreviewPnl.PointToClient(new Point(e.X, e.Y)));
            //if (target != null)
            //{
            //    int ix = this.thumbPreviewPnl.Controls.GetChildIndex(target);
            //    this.thumbPreviewPnl.Controls.SetChildIndex(source, ix);
            //}

            #endregion thunbnail cursor
            
            #region Process Dropped Files

            string img = null;
            int numbFiles = 0;

            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] droppedFiles = (string[])e.Data.GetData(DataFormats.FileDrop);
                foreach (string filename in droppedFiles)
                {
                    FileAttributes attr = System.IO.File.GetAttributes(filename);

                    // If Item Dropped Is Folder
                    if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                    {
                        string[] objs = (string[])e.Data.GetData(DataFormats.FileDrop);

                        foreach (string obj in objs)
                        {
                            DirectoryInfo dirInf = new DirectoryInfo(obj);

                            FileInfo[] dirFileArr = dirInf.GetFiles("*.*");

                            // list the names of all files in the specified directory
                            foreach (FileInfo dirFile in dirFileArr)
                            {
                                // Add file(s) to list if image
                                string ext = Path.GetExtension(filename).ToLower();
                                if (ext == ".pcx" || ext == ".gif" || ext == ".bmp" || ext == ".tif" || ext == ".tiff" || ext == ".png")
                                {
                                    this.statusbarMainMessage.Text = "Adding images in " + obj.ToString();

                                    frameListBox.Items.Add(filename);

                                    // Count number of files added
                                    numbFiles = ++numbFiles;
                                }
                            }

                            // Bring app to front
                            SetForegroundWindow(Handle.ToInt32());
                            //this.BringToFront();
                        }
                    }
                    else // Dropped item Is file
                    {
                        // Add file(s) to list if image
                        string ext = Path.GetExtension(filename).ToLower();
                        if (ext == ".pcx" || ext == ".gif" || ext == ".bmp" || ext == ".tif" || ext == ".tiff" || ext == ".png")
                        {
                            this.statusbarMainMessage.Text = "Adding images";

                            frameListBox.Items.Add(filename);

                            // Count number of files added
                            numbFiles = ++numbFiles;
                        }

                        // Bring app to front
                        SetForegroundWindow(Handle.ToInt32());
                    }
                }
            }

            // Set tracking image list to compare against changes
            if (trackingImgList == null)
            {
                foreach (string listImg in frameListBox.Items)
                    trackingImgList.Add(listImg);
                changedImgList = trackingImgList;
            }
            else
            {
                changedImgList.Clear();
                foreach (string listImg in frameListBox.Items)
                    changedImgList.Add(listImg);
            }

            // Add/Rearrange thumbnails to match
            AddThumbs();

            // Set Statusbar Message
            if (numbFiles >= 0)
                this.statusbarMainMessage.Text = "No images added";
            if (numbFiles == 1)
                this.statusbarMainMessage.Text = img + " added";
            if (numbFiles > 1)
                this.statusbarMainMessage.Text = numbFiles + " images added";

            #endregion Process Dropped Files

            ManageButtons();
            statusTimer.Start();
        }


        /// <summary>
        /// Restore cursor
        /// </summary>
        private void thumbPreviewPnl_DragLeave(object sender, EventArgs e)
        {
            Debug.WriteLine("OnDragLeave");
            previewThumbPicBox.Visible = false;
        }


        /// <summary>
        /// Remove?
        /// </summary>
        private void thumbPreviewPnl_MouseMove(object sender, MouseEventArgs e)
        {
            //Debug.WriteLine("OnMouseMove");
            //string filename;
            //validData = GetFilename(out filename, e);

            //validData = GetFilename(draggedName, MouseEventArgs e);

            //if (validData)
            //{
            //    if (lastFilename != filename)
            //    {
            //        previewThumb.Image = null;
            //        previewThumb.Visible = false;
            //        lastFilename = filename;
            //        getImageThread = new Thread(new ThreadStart(LoadImage));
            //        getImageThread.Start();
            //    }
            //    else
            //    {
            //        previewThumb.Visible = true;
            //    }
            //    e.Effect = DragDropEffects.Copy;
            //}
            //else
            //{
            //    e.Effect = DragDropEffects.None;
            //}

            //if (e.Button == MouseButtons.Left)
            //{
            //    MemoryStream move = new MemoryStream(Properties.Resources.CursorMove);
            //    this.Cursor = new Cursor(move);
            //}
        }


        /// <summary>
        /// Delselect frameListBox selected item on click
        /// </summary>
        private void thumbPreviewPnl_MouseDown(object sender, MouseEventArgs e)
        {
            this.frameListBox.SelectedIndex = -1;
        }

        #endregion thumbPreviewPnl


        #region seqPicBox

        /// <summary>
        /// Changes cursor to drag cursor and checks data being dragged for right types
        /// </summary>
        private void seqPicBox_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }


        /// <summary>
        /// Loads SEQ in player on Drag & Drop
        /// </summary>
        private void seqPicBox_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] filePaths = (string[])e.Data.GetData(DataFormats.FileDrop);

                foreach (string filePath in filePaths)
                {
                    string chkExt = Path.GetExtension(filePath).ToUpper();

                    if (chkExt == ".SEQ")
                    {
                        // Stop current play
                        try
                        {
                            fileStream.Close();
                            fileStream.Dispose();
                        }
                        catch { }

                        seqFile = filePath;
                        string seqName = Path.GetFileName(filePath);

                        fileStream = new BinaryReader(File.Open(seqFile, FileMode.Open));
                        int totalFrames = fileStream.ReadUInt16();
                        fileStream.Close();
                        fileStream.Dispose();

                        this.Text = seqName + " - " + totalFrames.ToString() + " frames - " + appTitle;

                        GetFirstFrame();
                        statusbarMainMessage.Text = string.Format("Frame 1 of " + totalFrames);
                        SEQNameLbl.Text = seqName;
                        seqLoaded = true;

                        ManageButtons();
                        playBtn.Enabled = true;
                        playCntxtMenu.Enabled = true;
                        stopBtn.Enabled = false;
                        stopCntxtMenu.Enabled = false;
                        editSEQBtn.Enabled = true;
                        editSEQCntxtMenu.Enabled = true;
                        seekBar.Value = 0;
                    }
                }
            }
        }

        #endregion seqPicBox


        #region Play Toolbar

        /// <summary>
        /// Rewind to frame 1
        /// </summary>
        private void rewindBtn_Click(object sender, EventArgs e)
        {
            Rewind();
        }


        /// <summary>
        /// Invokes StepBack() to step back one frame
        ///     Possible feature
        /// </summary>
        private void stepBackBtn_Click(object sender, EventArgs e)
        {
            StepBack();
        }


        /// <summary>
        /// Invokes Stop() to stop SEQ play
        /// </summary>
        private void stopBtn_Click(object sender, EventArgs e)
        {
            Stop();
        }


        /// <summary>
        /// Invokes Pause() to pause SEQ play
        ///     Possible feature
        /// </summary>
        private void pauseBtn_Click(object sender, EventArgs e)
        {
            Pause();
        }


        /// <summary>
        /// Invokes Play() to play loaded SEQ file
        /// </summary>
        private void playBtn_Click(object sender, EventArgs e)
        {
            Play(seqFile);
        }


        /// <summary>
        /// Invokes StepNext() to step forward one frame
        ///     Possible feature
        /// </summary>
        private void stepNextBtn_Click(object sender, EventArgs e)
        {
            StepNext();
        }


        /// <summary>
        /// Fast forwards to end
        /// </summary>
        private void ffBtn_Click(object sender, EventArgs e)
        {
            FastForward();
        }


        /// <summary>
        /// Adjusts accompanying SND/AUD resource volume
        ///     Possible feature
        /// </summary>
        private void volumeScrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            //SetVolume();
        }


        /// <summary>
        /// Mutes accompanying SND/AUD resource volume
        ///     Possible feature
        /// </summary>
        private void muteBtn_Click(object sender, EventArgs e)
        {
            //Mute();
        }

        #endregion Play Toolbar


        #region Context Menus

        /// <summary>
        /// Manage cntxtMenu items on opening
        /// </summary>
        private void cntxtMenu_Opening(object sender, CancelEventArgs e)
        {
            if (frameListBox.Items.Count < 1)
            {
                this.removeCntxtMenu.Enabled = false;
                this.deleteCntxtMenu.Enabled = false;
                this.clearAllCntxtMenu.Enabled = false;
                this.viewCntxtMenu.Enabled = false;
                this.copyCntxtMenu.Enabled = false;
                this.editCntxtMenu.Enabled = false;
                this.propsCntxtMenu.Enabled = false;
                return;
            }
            else
                this.clearAllCntxtMenu.Enabled = true;

            if (this.frameListBox.SelectedIndex == -1)
            {
                this.removeCntxtMenu.Enabled = false;
                this.deleteCntxtMenu.Enabled = false;
                this.viewCntxtMenu.Enabled = false;
                this.copyCntxtMenu.Enabled = false;
                this.editCntxtMenu.Enabled = false;
                this.propsCntxtMenu.Enabled = false;
            }
            else
            {
                this.removeCntxtMenu.Enabled = true;
                this.deleteCntxtMenu.Enabled = true;
                this.viewCntxtMenu.Enabled = true;
                this.copyCntxtMenu.Enabled = true;
                this.editCntxtMenu.Enabled = true;
                this.propsCntxtMenu.Enabled = true;
            }
        }


        /// <summary>
        /// Opens file browser dialog to add image
        /// </summary>
        private void addCntxtMenu_Click(object sender, EventArgs e)
        {
            AddImg();
        }


        /// <summary>
        /// Removes  selected image from list
        /// </summary>
        private void removeCntxtMenu_Click(object sender, EventArgs e)
        {
            RemoveImg();
        }


        /// <summary>
        /// Sends selected file to Recycle Bin
        /// </summary>
        private void deleteCntxtMenu_Click(object sender, EventArgs e)
        {
            //if (this.frameListBox.SelectedIndex == -1) return;

            string file = frameListBox.SelectedItem.ToString();
            Recycle(file);
            RemoveImg();
        }


        /// <summary>
        /// Clears image list
        /// </summary>
        private void clearAllCntxtMenu_Click(object sender, EventArgs e)
        {
            ClearAllImgs();
        }


        /// <summary>
        /// Opens selected image in defaut viewer
        /// </summary>
        private void viewCntxtMenu_Click(object sender, EventArgs e)
        {
            ViewImg(frameListBox.SelectedItem.ToString());
        }


        /// <summary>
        /// Copies selected image to clipboard
        /// </summary>
        private void copyCntxtMenu_Click(object sender, EventArgs e)
        {
            CopyImg(frameListBox.SelectedItem.ToString());
        }


        /// <summary>
        /// Opens selected image in defaut image editor
        /// </summary>
        private void editCntxtMenu_Click(object sender, EventArgs e)
        {
            EditImg(frameListBox.SelectedItem.ToString());
        }


        /// <summary>
        /// Displays file properties for selected image
        /// </summary>
        private void propsCntxtMenu_Click(object sender, EventArgs e)
        {
            FileProps(frameListBox.SelectedItem.ToString());
        }


        /// <summary>
        /// Invokes OpenSEQ() to open SEQ in Player
        /// </summary>
        private void loadSEQCntxtMenu_Click(object sender, EventArgs e)
        {
            OpenSEQ();
        }


        /// <summary>
        /// Invokes Stop() to stop SEQ play
        /// </summary>
        private void stopCntxtMenu_Click(object sender, EventArgs e)
        {
            Stop();
        }


        /// <summary>
        /// Invokes Play() to play loaded SEQ file
        /// </summary>
        private void playCntxtMenu_Click(object sender, EventArgs e)
        {
            Play(seqFile);
        }


        /// <summary>
        /// Invokes Extract() to extract frames for editing
        /// </summary>
        private void editSEQCntxtMenu_Click(object sender, EventArgs e)
        {
            Extract();
        }

        #endregion Context Menus


        #region Extra Functions

        #region File Methods

        /// <summary>
        /// Clears image list & thumbs and nulls all values
        /// </summary>
        private void New()
        {
            PromptSave();

            // Reset app title
            this.Text = "SEQTool";

            // Clear frames
            frameListBox.Items.Clear();
            frames.Clear();

            // Clear thumbnails
            thumbPreviewPnl.Controls.Clear();

            // Clear Player
            seqPicBox.Image = null;
            seqPicBox.Invalidate();
            SEQNameLbl.Text = null;

            // Reset subDirChckBox to default
            toolsMenuUseSubDir.Checked = true;
            seqSubDir = true;
            SaveProject.seqSubDir = true;

            // Clear projName
            projName = null;
            SaveProject.projName = null;

            // Clear projFile
            projFile = null;
            SaveProject.projFile = null;

            // Clear SEQ name
            seqName = null;
            seqNameTxtBox.Text = seqName;
            ResetNullName();

            ManageButtons();

            // Clear SaveProject variables
            SaveProject.frames.Clear();
            SaveProject.projName = null;
            SaveProject.projFile = null;
            SaveProject.seqSubDir = true;
            SaveProject.seqName = null;

            // Clear tracking SEQ name
            trackingImgList.Clear();
            changedImgList.Clear();

            // Clear tracking image lists
            changedName = null;
            trackingName = null;


            string value = string.Empty;
            try
            {
                value = Registry.GetValue(@"HKEY_CURRENT_USER\Software\mtnPhilms\SCICompanion\SCICompanion\", "OriginalAspectRatio", 0).ToString();
            }
            catch (Exception ex)
            {
                string err = ("Error: " + ex.Message);
                this.statusbarMainMessage.Text = (err);
                statusTimer.Start();

                // Log exception
                ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + err);
            }

            if (value == "0")
            {
                aspect = false;
                viewMenuAspectCorrection.Checked = false;
            }

            if (value == "1" || aspect == null)
            {
                aspect = true;
                viewMenuAspectCorrection.Checked = true;
            }
        }


        /// <summary>
        /// Opens a file browser dialog to open a save file
        /// </summary>
        private void Open()
        {
            PromptSave();

            OpenFileDialog openFileDlg = new OpenFileDialog();
            //openFileDlg.InitialDirectory = Props.projPath; // Get files from Add Files Button dialog
            openFileDlg.AddExtension = true;
            openFileDlg.CheckFileExists = true;
            openFileDlg.CheckPathExists = true;
            openFileDlg.DefaultExt = "sqp";
            openFileDlg.Filter = "SEQTool project files (*.sqp)|*.sqp|All files|*.*";
            //openFileDlg.FileName = ".sqp";
            openFileDlg.Multiselect = false;
            openFileDlg.RestoreDirectory = true;
            openFileDlg.ShowReadOnly = false;
            openFileDlg.Title = "Select a SQP file";
            openFileDlg.ValidateNames = true;

            // Check if Browse Files Dialog Cancel Button pressed and return if Cancel is pressed
            if (openFileDlg.ShowDialog() == DialogResult.Cancel)
            {
                this.statusbarMainMessage.Text = "Browse canceled";
                statusTimer.Start();
                return;
            }

            projFile = openFileDlg.FileName;
            open = true;
            ReadSaveFile();
        }


        /// <summary>
        /// Reads save file to populate saved fields
        /// </summary>
        public void ReadSaveFile()
        {
            XmlDocument projDoc = new XmlDocument();

            if (File.Exists(projFile))
            {
                if (open == true)
                    PromptSave();
                open = false;

                #region Load Project file

                try
                {
                    projDoc.Load(projFile);
                }
                catch { }

                #endregion Load Project file

                this.Text = "SEQTool";
                seqNameTxtBox.Text = null;
                ClearAllImgs();

                #region Get Project

                // Name - Get project name
                XmlNode node = null;
                node = projDoc.DocumentElement.SelectSingleNode("/root/Project/ProjName");
                if (node != null)
                    projName = node.InnerText;
                this.Text = projName + " - SEQTool";

                // BaseName - Get project path
                node = null;
                node = projDoc.DocumentElement.SelectSingleNode("/root/Project/ProjPath");
                if (node != null)
                    projPath = node.InnerText;
                //projPathTextBox.Text = projPath;

                // ProjAspect - Get project aspect ratio
                node = null;
                string projAspect = string.Empty;
                node = projDoc.DocumentElement.SelectSingleNode("/root/Project/ProjAspect");
                if (node != null)
                    projAspect = node.InnerText;
                if (projAspect == "true")
                {
                    aspect = true;
                    viewMenuAspectCorrection.Checked = true;
                }
                if (projAspect == "false")
                {
                    aspect = false;
                    viewMenuAspectCorrection.Checked = false;
                }

                // Get outfile name
                node = null;
                node = projDoc.DocumentElement.SelectSingleNode("/root/Project/SEQName");
                if (node != null)
                    seqName = node.InnerText;
                if (String.IsNullOrEmpty(seqName) || seqName == "SEQ Name   ")
                {
                    seqNameTxtBox.ForeColor = System.Drawing.SystemColors.GrayText;
                    seqNameTxtBox.Text = "SEQ Name   ";
                }
                else if (seqName != "SEQ Name   ")
                {
                    seqNameTxtBox.ForeColor = System.Drawing.SystemColors.WindowText;
                    seqNameTxtBox.Text = seqName;
                }

                // Get if SEQ Subdirectory is to be used
                node = null;
                node = projDoc.DocumentElement.SelectSingleNode("/root/Project/SEQSubDir");
                if (node != null)
                    seqSubDir = bool.Parse(node.InnerText);
                toolsMenuUseSubDir.Checked = seqSubDir;

                // Get frame images
                node = null;
                node = projDoc.DocumentElement.SelectSingleNode("/root/Project/Frames");
                if (node != null)
                    foreach (XmlNode nd in node.ChildNodes)
                        if (nd != null)
                            frameListBox.Items.Add(nd.InnerText);

                // Add thumbnails
                AddThumbs();

                ManageButtons();

                #endregion Get Project

                ManageButtons();

                // Set tracking image list to compare against changes
                trackingImgList.Clear();
                foreach (string listImg in frameListBox.Items)
                    trackingImgList.Add(listImg);
                changedImgList = trackingImgList;

                // Set tracking SEQ name
                trackingName = seqNameTxtBox.Text;
                changedName = trackingName;

                ManageButtons();
            }
            else
            {
                MessageBox.Show("Project file not found", "Missing Project File", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
        }


        /// <summary>
        /// Saves current project
        /// </summary>
        private void Save()
        {
            SaveProject.frames.Clear();

            SaveProject.projName = Directory.GetParent(projPath + "\\src").Name;
            SaveProject.projFile = projPath + "\\" + SaveProject.projName + ".sqp";

            if (!seqNameTxtBox.Text.ToUpper().EndsWith(".SEQ"))
                SaveProject.seqName = seqNameTxtBox.Text + ".SEQ";
            else
                SaveProject.seqName = seqNameTxtBox.Text;
            SaveProject.seqSubDir = true;

            foreach (string item in frameListBox.Items)
                SaveProject.frames.Add(item);

            SaveProject.Save();

            // Set tracking image list to compare against changes
            trackingImgList.Clear();
            foreach (string listImg in frameListBox.Items)
                trackingImgList.Add(listImg);
            changedImgList = trackingImgList;

            // Set tracking SEQ name
            trackingName = seqNameTxtBox.Text;
            changedName = trackingName;
        }


        /// <summary>
        /// Saves current project to a new file
        /// </summary>
        private void SaveAs()
        {
            SaveProject.frames.Clear();

            SaveProject.projName = Directory.GetParent(projPath + "\\src").Name;
            SaveProject.projFile = projPath + "\\" + SaveProject.projName + ".sqp";
            if (!seqNameTxtBox.Text.ToUpper().EndsWith(".SEQ"))
                SaveProject.seqName = seqNameTxtBox.Text + ".SEQ";
            else
                SaveProject.seqName = seqNameTxtBox.Text;
            SaveProject.seqSubDir = true;

            foreach (string item in frameListBox.Items)
                SaveProject.frames.Add(item);

            SaveProject.SaveAs();

            // Set tracking image list to compare against changes
            trackingImgList.Clear();
            foreach (string listImg in frameListBox.Items)
                trackingImgList.Add(listImg);
            changedImgList = trackingImgList;

            // Set tracking SEQ name
            trackingName = seqNameTxtBox.Text;
            changedName = trackingName;
        }


        /// <summary>
        /// Opens a file browser dialog to select an existing SEQ file
        /// </summary>
        private void OpenSEQ()
        {
            OpenFileDialog openFileDlg = new OpenFileDialog();
            //openFileDlg.InitialDirectory = Props.projPath; // Get files from Add Files Button dialog
            openFileDlg.AddExtension = true;
            openFileDlg.CheckFileExists = true;
            openFileDlg.CheckPathExists = true;
            openFileDlg.DefaultExt = "seq";
            openFileDlg.Filter = "SEQ files (*.seq)|*.seq|All files|*.*";
            //openFileDlg.FileName = ".seq";
            openFileDlg.Multiselect = false;
            openFileDlg.RestoreDirectory = true;
            openFileDlg.ShowReadOnly = false;
            openFileDlg.Title = "Select a SEQ file";
            openFileDlg.ValidateNames = true;

            // Check if Browse Files Dialog Cancel Button pressed and return if Cancel is pressed
            if (openFileDlg.ShowDialog() == DialogResult.Cancel)
            {
                this.statusbarMainMessage.Text = "Browse canceled";
                statusTimer.Start();
                return;
            }

            seqFile = openFileDlg.FileName;
            LoadSEQ();
        }


        /// <summary>
        /// Loads SEQ file in Player
        /// </summary>
        private void LoadSEQ()
        {
            // Stop current play
            try
            {
                fileStream.Close();
                fileStream.Dispose();
            }
            catch { }

            seekBar.Value = 0;

            string seqName = Path.GetFileName(seqFile);

            fileStream = new BinaryReader(File.Open(seqFile, FileMode.Open));
            int totalFrames = fileStream.ReadUInt16();
            fileStream.Close();
            fileStream.Dispose();

            this.Text = seqName + " - " + totalFrames.ToString() + " frames - " + appTitle;

            GetFirstFrame();
            statusbarMainMessage.Text = string.Format("Frame 1 of " + totalFrames);
            SEQNameLbl.Text = seqName;
            seqLoaded = true;
            seekBar.Value = 0;

            ManageButtons();
            playBtn.Enabled = true;
            playCntxtMenu.Enabled = true;
            stopBtn.Enabled = false;
            stopCntxtMenu.Enabled = false;
            editSEQBtn.Enabled = true;
            editSEQCntxtMenu.Enabled = true;
            seekBar.Value = 0;
        }


        /// <summary>
        /// Saves current SEQ file to a new file
        /// </summary>
        private void SaveSEQ()
        {
            //string seqName = Path.GetFileName(seqFile);
            //string seqPath = Path.GetDirectoryName(seqFile);

            if (File.Exists(seqFile))
            {
                List<string> inFiles = new List<string>();
                foreach (string item in frameListBox.Items)
                    inFiles.Add(item);

                MakeSeq(inFiles);

                // Disable saveSEQBtn
                saveSEQBtn.Enabled = false;
                trackedIMG = null;
                trackingMD5 = null;
            }
        }


        /// <summary>
        /// Saves current SEQ file to a new file
        /// </summary>
        private void SaveSEQAs()
        {
            SaveFileDialog saveFileDlg = new SaveFileDialog();
            saveFileDlg.InitialDirectory = projPath;
            saveFileDlg.Title = "Save SEQ to new file";
            saveFileDlg.DefaultExt = "seq";
            saveFileDlg.AddExtension = true;
            saveFileDlg.Filter = "SEQ files (*.seq)|*.seq|All files (*.*)|*.*";
            //saveFileDlg.FilterIndex = 1;
            //saveFileDlg.RestoreDirectory = true;
            saveFileDlg.FileName = seqFile;

            if (saveFileDlg.ShowDialog() == DialogResult.Cancel) return;

            string newSEQ = saveFileDlg.FileName;
            string newSEQName = Path.GetFileName(newSEQ);
            string newSEQPath = Path.GetDirectoryName(newSEQ);

            if (File.Exists(seqFile))
            {
                if (File.Exists(newSEQ))
                {
                    DialogResult dialogResult = MessageBox.Show("The " + newSEQPath + " folder already contains a file named \"" + 
                        newSEQName + @""" already exists, do you wish to overwrite?",
                        "Overwrite Warning!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.No) return;
                }

                File.Copy(seqFile, newSEQ, true);
            }

            seqFile = newSEQ;

            //trackingName = seqNameTxtBox.Text;

            SEQNameLbl.Text = Path.GetFileName(seqFile);
        }


        /// <summary>
        /// Extracts SEQ to individual images
        /// </summary>
        public void Extract()
        {
            if (!File.Exists(seqFile)) return;

            PromptSave();

            //seqPicBox.Controls.Add(waitLbl);
            viewMenuExamMode.Checked = false;

            try
            {
                playMode = null;

                // Clear frames from frameListBox
                frameListBox.Items.Clear();
                trackingImgList.Clear();
                changedImgList.Clear();

                playBtn.Enabled = false;
                playCntxtMenu.Enabled = false;
                stopBtn.Enabled = false;
                stopCntxtMenu.Enabled = false;
                editSEQCntxtMenu.Enabled = false;
                waitLbl.Visible = true;

                // Release fileStream
                if (fileStream != null)
                    try
                    {
                        fileStream.Close();
                        fileStream.Dispose();
                    }
                    catch { }

                frameCount = 0;
                if (!File.Exists(seqFile)) return;

                seqLoaded = true;
                seqPicBox.Image = null;

                string exportFormat = formatCmboBox.Text;
                
                string framesPath = string.Empty;
                if (Directory.Exists(projPath))
                    framesPath = Path.Combine(projPath + "\\SEQ_SRC", Path.GetFileNameWithoutExtension(seqFile));
                else
                {
                    MessageBox.Show("Project folder not found.", "Folder Not Found", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                if (!Directory.Exists(framesPath))
                {
                    try
                    {
                        Directory.CreateDirectory(framesPath);
                    }
                    catch
                    {
                        MessageBox.Show("Unable to create output folder not found.", "Folder Not Found", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }
                }

                fileStream = new BinaryReader(File.Open(seqFile, FileMode.Open));

                screen = new byte[320 * 200];
                bitmap = new Bitmap(320, 200, PixelFormat.Format8bppIndexed);

                seqPicBox.SizeMode = PictureBoxSizeMode.StretchImage;

                if (viewMenuExamMode.Checked == true)
                    exam = true;

                string seqName = Path.GetFileNameWithoutExtension(seqFile);
                outputFrame = Path.Combine(framesPath, seqName + "-0001" + exportFormat);

                curFrame = 1;
                frameCount = fileStream.ReadUInt16();

                // Overwrite Prompt
                int i = 1;
                string baseFile = Path.Combine(framesPath, seqName);
                while (i <= frameCount)
                {
                    string eachFrame = i.ToString().PadLeft(4, '0');
                    string currentFrame = baseFile + "-" + eachFrame + exportFormat;
                    if (File.Exists(currentFrame))
                    {
                        DialogResult dlg = MessageBox.Show("Images in the destination folder already exist. Do you wish to overwrite?", 
                            "Overwrite File?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dlg == DialogResult.No)
                        {
                            waitLbl.Visible = false;
                            LoadSEQ();
                            return;
                        }
                        break;
                    }
                    i++;
                }

                ReadPaletteChunk(fileStream.ReadInt32());
                DecodeNextFrame();

                timer = new System.Windows.Forms.Timer();
                timer.Interval = 100;
                timer.Tick += (s, e) =>
                {
                    DecodeNextFrame();
                    if (curFrame > frameCount)
                    {
                        timer.Stop();

                        fileStream.Close();
                        fileStream.Dispose();
                        int numFrame = 1;

                        //foreach (Image frame in frames)
                        foreach (string obj in Directory.GetFiles(framesPath, "*.*"))
                        {
                            //string tempPath = Path.GetTempPath();
                            //frame.Save(framesPath + "Frame " + numFrame + exportFormat);
                            frameListBox.Items.Add(obj);

                            numFrame++;
                        }

                        // Set tracking image list to compare against changes
                        foreach (string listImg in frameListBox.Items)
                            trackingImgList.Add(listImg);
                        changedImgList = trackingImgList;

                        // Add thumbnails to match
                        AddThumbs();

                        seqName = Path.GetFileNameWithoutExtension(seqFile);
                        seqNameTxtBox.Text = seqName;
                        ResetNullName();

                        // Set tracking SEQ name
                        changedName = seqNameTxtBox.Text;
                        trackingName = changedName;

                        ManageButtons();
                        playMode = null;
                        playBtn.Enabled = true;
                        playCntxtMenu.Enabled = true;
                        stopBtn.Enabled = false;
                        stopCntxtMenu.Enabled = false;
                        editSEQCntxtMenu.Enabled = true;
                        waitLbl.Visible = false;

                        statusTimer.Start();
                    }
                };

                timer.Start();
            }
            catch (Exception ex)
            {
                string err = ("Error: " + ex.Message);
                MessageBox.Show(err, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.statusbarMainMessage.Text = ("Error: " + ex.Message);
                statusTimer.Start();

                // Log exception
                ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + ex.Message);
            }
        }


        /// <summary>
        /// Imports animated GIF file and loads frames into form
        /// </summary>
        private void Import()
        {
            PromptSave();

            OpenFileDialog openFileDlg = new OpenFileDialog();
            //openFileDlg.InitialDirectory = projPath;
            openFileDlg.AddExtension = true;
            openFileDlg.CheckFileExists = true;
            openFileDlg.CheckPathExists = true;
            openFileDlg.DefaultExt = "gif";
            openFileDlg.Filter = "Animated GIF files (*.gif)|*.gif|All files|*.*";
            openFileDlg.Multiselect = false;
            openFileDlg.RestoreDirectory = true;
            openFileDlg.ShowReadOnly = false;
            openFileDlg.Title = "Select an animated GIF file";
            openFileDlg.ValidateNames = true;

            // Check if Browse Files Dialog Cancel Button pressed and return if Cancel is pressed
            if (openFileDlg.ShowDialog() == DialogResult.Cancel)
            {
                this.statusbarMainMessage.Text = "Browse canceled";
                statusTimer.Start();
                return;
            }

            waitLbl.Visible = true;
            this.statusbarMainMessage.Text = "Importing frames...";
            aniGIFPath = openFileDlg.FileName;

            // Extract frames in new thread
            importTimer.Start();
            Thread thread = new Thread(GIFFrames);
            thread.Start();
        }


        /// <summary>
        /// Extracts 
        /// </summary>
        private void GIFFrames()
        {
            // Set frames path
            string framesPath = Path.Combine(projPath + "\\SEQ_SRC", Path.GetFileNameWithoutExtension(aniGIFPath));
            if (!Directory.Exists(framesPath))
            {
                Directory.CreateDirectory(framesPath);
            }
            framesPath = framesPath + "\\";

            // Extract individual GIFs
            gifFrames = GetFrames(Image.FromFile(aniGIFPath));

            extracted = true;
        }


        /// <summary>
        /// Exports SEQ to individual images
        /// </summary>
        public void Export()
        {
            if (!File.Exists(seqFile)) return;

            FolderBrowserDialog folderBrowserDlg = new FolderBrowserDialog();
            folderBrowserDlg.Description = "Select Output Folder";
            folderBrowserDlg.RootFolder = Environment.SpecialFolder.Desktop;

            // Recall last path
            if (Directory.Exists(Properties.Settings.Default.exportPath))
                folderBrowserDlg.SelectedPath = Properties.Settings.Default.exportPath;
            else
                folderBrowserDlg.SelectedPath = projPath;

            if (folderBrowserDlg.ShowDialog() == DialogResult.Cancel) return;

            exportPath = folderBrowserDlg.SelectedPath;

            // Remember last path
            Properties.Settings.Default.exportPath = exportPath;

            viewMenuExamMode.Checked = false;
            playMode = "Export";

            try
            {
                playMode = null;
                playBtn.Enabled = false;
                playCntxtMenu.Enabled = false;
                stopBtn.Enabled = false;
                stopCntxtMenu.Enabled = false;
                editSEQCntxtMenu.Enabled = false;

                waitLbl.Visible = true;

                // Release fileStream
                if (fileStream != null)
                    Stop();

                frameCount = 0;
                if (!File.Exists(seqFile)) return;

                seqLoaded = true;
                seqPicBox.Image = null;

                string exportFormat = formatCmboBox.Text;

                fileStream = new BinaryReader(File.Open(seqFile, FileMode.Open));

                screen = new byte[320 * 200];
                bitmap = new Bitmap(320, 200, PixelFormat.Format8bppIndexed);
                seqPicBox.SizeMode = PictureBoxSizeMode.StretchImage;

                if (viewMenuExamMode.Checked == true)
                    exam = true;

                string seqName = Path.GetFileNameWithoutExtension(seqFile);
                outputFrame = Path.Combine(exportPath, seqName + "-0001" + exportFormat);

                curFrame = 1;
                frameCount = fileStream.ReadUInt16();

                // Overwrite Prompt
                int i = 0;
                string baseFile = Path.Combine(exportPath, seqName);
                while (i <= frameCount)
                {
                    string eachFrame = i.ToString().PadLeft(4, '0');
                    string currentFrame = baseFile + "-" + eachFrame + exportFormat;
                    if (File.Exists(currentFrame))
                    {
                        DialogResult dlg = MessageBox.Show("Images in the destination folder already exist. Do you wish to overwrite?", 
                            "Overwrite File?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dlg == DialogResult.No)
                        {
                            waitLbl.Visible = false;
                            LoadSEQ();
                            return;
                        }
                        break;
                    }
                    i++;
                }

                ReadPaletteChunk(fileStream.ReadInt32());
                DecodeNextFrame();

                timer = new System.Windows.Forms.Timer();
                timer.Interval = 100;
                timer.Tick += (s, e) =>
                {
                    DecodeNextFrame();
                    if (curFrame > frameCount)
                    {
                        timer.Stop();

                        fileStream.Close();
                        fileStream.Dispose();

                        outputFrame = null;
                        waitLbl.Visible = false;
                        playMode = null;

                        statusTimer.Start();
                    }
                };

                timer.Start();
            }
            catch (Exception ex)
            {
                outputFrame = null;
                playMode = null;
                string err = ("Error: " + ex.Message);
                MessageBox.Show(err, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.statusbarMainMessage.Text = ("Error: " + ex.Message);
                statusTimer.Start();

                // Log exception
                ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + ex.Message);
            }


            // To do - assemble individual images into ani GIF
            // http://stackoverflow.com/questions/3688870/create-animated-gif-from-a-set-of-jpeg-images ?
        }
        

        /// <summary>
        /// Add movieDir line to RESOURCE.CFG
        /// </summary>
        private void AppendResCFG()
        {
            string resCFG = Path.Combine(projPath, "RESOURCE.CFG");

            if (File.Exists(resCFG))
            {
                string ln = string.Empty;
                bool movieDirLine = false;
                int counter = 0;

                StreamReader sr = new StreamReader(resCFG);
                {
                    while ((ln = sr.ReadLine()) != null)
                    {
                        if (!String.IsNullOrEmpty(ln))
                            if (ln.ToLower().Contains("moviedir"))
                            {
                                movieDirLine = true;
                            }
                        counter++;
                    }
                    sr.Close();
                }

                if (movieDirLine == false)
                {
                    if (toolsMenuUseSubDir.Checked == true)
                        File.AppendAllText(resCFG, Environment.NewLine + @" movieDir  = .\SEQ");
                    else
                        File.AppendAllText(resCFG, Environment.NewLine + @" movieDir  = .\");
                }
                else if (movieDirLine == true)
                {
                    DialogResult dlg = MessageBox.Show("The \"RESOURCE.CFG\" file already contains a \"movieDir =\" line. Would you like to manually edit it?", "Entry Exists", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (dlg == DialogResult.No) return;
                    else if (dlg == DialogResult.Yes)
                    {
                        try
                        {
                            Process.Start("Notepad", resCFG);
                        }
                        catch { }
                    }
                }
            }
        }


        /// <summary>
        /// Manages Recycle file and folder dialogs
        /// </summary>
        public void Recycle(string file)
        {
            if (!File.Exists(file)) return;

            string selectedName = Path.GetFileName(file);
            FileAttributes attrib = System.IO.File.GetAttributes(file);
            //FileInfo fi = new FileInfo(file);
            var att = System.IO.File.GetAttributes(file);
            bool del = false;

            if ((attrib & FileAttributes.Directory) == FileAttributes.Directory)
            {
                // If Directory is hidden
                if ((att & FileAttributes.Hidden) == FileAttributes.ReadOnly)
                {
                    DialogResult dialogResult = MessageBox.Show("The folder " + selectedName + " is Read Only. Are you sure you want to move this folder to the Recycle Bin?", "Confirm Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        del = true;
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        del = false;
                        return;
                    }
                }
                else if ((att & FileAttributes.Hidden) == FileAttributes.Hidden) // If Directory is hidden
                {
                    DialogResult dialogResult = MessageBox.Show("The folder " + selectedName + " is Hidden. Are you sure you want to move this folder to the Recycle Bin?", "Confirm Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        del = true;
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        del = false;
                        return;
                    }
                }
                else if ((att & FileAttributes.System) == FileAttributes.System) // If Directory is System Folder
                {
                    DialogResult dialogResult = MessageBox.Show("The folder " + selectedName + " is a System folder. Are you sure you want to move this folder to the Recycle Bin?", "Confirm Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        del = true;
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        del = false;
                        return;
                    }
                }
                else
                {
                    DialogResult dialogResult = MessageBox.Show("Are you sure you want to move this folder to the Recycle Bin?", "Confirm Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        del = true;
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        del = false;
                        return;
                    }
                }

                if (del == true)
                {
                    if (Directory.Exists(file))
                    {
                        RecyleBin.Send(file);
                    }
                }
            }
            else // Is File
            {
                // If file is hidden
                if ((att & FileAttributes.Hidden) == FileAttributes.ReadOnly)
                {
                    DialogResult dialogResult = MessageBox.Show("The folder " + selectedName + " is Read Only. Are you sure you want to move this file to the Recycle Bin?", "Confirm Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        del = true;
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        del = false;
                        return;
                    }
                }
                else if ((att & FileAttributes.Hidden) == FileAttributes.Hidden) // If file is hidden
                {
                    DialogResult dialogResult = MessageBox.Show("The folder " + selectedName + " is Hidden. Are you sure you want to move this file to the Recycle Bin?", "Confirm Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        del = true;
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        del = false;
                        return;
                    }
                }
                else if ((att & FileAttributes.System) == FileAttributes.System) // If item is system file
                {
                    DialogResult dialogResult = MessageBox.Show("The folder " + selectedName + " is a System folder. Are you sure you want to move this file to the Recycle Bin?", "Confirm Deletion", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        del = true;
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        del = false;
                        return;
                    }
                }
                else
                {
                    DialogResult dialogResult = MessageBox.Show("Are you sure you want to move this file to the Recycle Bin?", "Delete File", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        del = true;
                    }
                    else if (dialogResult == DialogResult.No)
                    {
                        del = false;
                        return;
                    }
                }

                if (del == true)
                {
                    if (System.IO.File.Exists(file))
                    {
                        if (RecyleBin.Send(file))
                        {
                            // Delete selected file
                        }
                        else
                        {
                            MessageBox.Show("There was an error trying to move the file to the recycle bin", "Error");
                        }
                    }
                }
            }
        }

        #endregion File Methods


        #region Image Methods

        /// <summary>
        /// Opens Browse dialog to select image(s)
        /// </summary>
        private void AddImg()
        {
            this.statusbarMainMessage.Text = "Browsing for image files";

            int numbFiles = 0;
            string img = null;

            // Open file browser dialog
            OpenFileDialog openFileDlg = new OpenFileDialog();
            //openFileDlg.InitialDirectory = projPath;
            openFileDlg.AddExtension = true;
            openFileDlg.CheckFileExists = true;
            openFileDlg.CheckPathExists = true;
            openFileDlg.DefaultExt = "BMP";
            openFileDlg.Filter = "Image files (*.pcx, *.gif, *.bmp, *.tif, *.tiff, *.png) | *.pcx; *.gif; *.bmp; *.tif; *.tiff; *.png| All files|*.*";
            openFileDlg.Title = "Select an image";
            openFileDlg.Multiselect = true;
            openFileDlg.RestoreDirectory = true;
            openFileDlg.ShowReadOnly = false;
            openFileDlg.ValidateNames = true;

            //  Check if Browse Files Dialog Cancel Button pressed and return if pressed
            if (openFileDlg.ShowDialog() == DialogResult.Cancel)
            {
                this.statusbarMainMessage.Text = "Browse canceled";
                statusTimer.Start();
                return;
            }

            try
            {
                //  Add file(s) to list.
                foreach (string imgFile in openFileDlg.FileNames)
                {
                    //// Overwrite Prompt
                    //string imgName = Path.GetFileName(imgFile);
                    //if (File.Exists(imgFile))
                    //{
                    //    DialogResult dlg = MessageBox.Show("A file named " + imgName + " already exists in the destination folder. Do you wish to overwrite it?",
                    //        "Overwrite File?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    //    if (dlg == DialogResult.No)
                    //    {
                    //        waitLbl.Visible = false;
                    //        LoadSEQ();
                    //        return;
                    //    }
                    //}

                    this.statusbarMainMessage.Text = "Adding " + imgFile;

                    frameListBox.Items.Add(imgFile);

                    // Count number of files added
                    numbFiles = ++numbFiles;
                }
            }
            catch (Exception ex)
            {
                this.statusbarMainMessage.Text = ("Error: " + ex.Message);
                string err = ("Error: " + ex.Message);
                MessageBox.Show(err, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                // Log exception
                ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + ex.Message);
            }

            // Add/Rearrange thumbnails to match
            AddThumbs();

            // Set tracking image list to compare against changes
            changedImgList.Clear();
            foreach (string listImg in frameListBox.Items)
                changedImgList.Add(listImg);

            // Set status message
            if (numbFiles >= 0)
                this.statusbarMainMessage.Text = "No images added";
            if (numbFiles == 1)
                this.statusbarMainMessage.Text = img + " added";
            if (numbFiles > 1)
                this.statusbarMainMessage.Text = numbFiles + " images added";

            ManageButtons();
            statusTimer.Start();
        }


        /// <summary>
        /// Removes selected image from list
        /// </summary>
        private void RemoveImg()
        {
            // Get frameListBox index of frames
            int listIndex = frameListBox.SelectedIndex;
            string[] elementName = new string[frameListBox.SelectedItems.Count];
            frameListBox.SelectedItems.CopyTo(elementName, 0);

            // Remove from frameListBox
            while ((frameListBox.SelectedItems.Count > 0))
                frameListBox.Items.Remove(frameListBox.SelectedItem);

            // Rearrange thumbnails to match
            AddThumbs();

            // Set tracking image list to compare against changes
            changedImgList.Clear();
            foreach (string listImg in frameListBox.Items)
                changedImgList.Add(listImg);

            ManageButtons();
        }


        /// <summary>
        /// Clears all images from list
        /// </summary>
        private void ClearAllImgs()
        {
            // Clear frames from frameListBox
            frameListBox.Items.Clear();

            // Clear thumbnails
            thumbPreviewPnl.Controls.Clear();

            // Track changes
            changedImgList.Clear();

            ManageButtons();
        }


        /// <summary>
        /// Opens selected image in default viewer
        /// </summary>
        /// <param name="path">Path to selected image file</param>
        private void ViewImg(string path)
        {
            if (frameListBox.SelectedIndex == -1) return;

            string imgName = Path.GetFileName(path);

            statusbarMainMessage.Text = imgName + " selected";

            try
            {
                Process.Start(path);

                statusbarMainMessage.Text = ("Viewing " + imgName);
            }
            catch (Exception ex)
            {
                statusbarMainMessage.Text = ("Error: " + ex.Message);
                string err = ("Error: " + ex.Message);
                MessageBox.Show(err, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                // Log exception
                ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + ex.Message);
                statusTimer.Start();
            }

            statusTimer.Start();
        }


        /// <summary>
        /// Copies selected image to clipboard
        /// </summary>
        /// <param name="path">Path to selected image file</param>
        private void CopyImg(string path)
        {
            if (frameListBox.SelectedIndex == -1) return;

            string imgName = Path.GetFileName(path);

            try
            {
                ShellClass.CopyFile(path);

                statusbarMainMessage.Text = ("Copying " + imgName);
            }
            catch (Exception ex)
            {
                statusbarMainMessage.Text = ("Error: " + ex.Message);
                string err = ("Error: " + ex.Message);
                MessageBox.Show(err, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                // Log exception
                ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + ex.Message);
            }

            statusTimer.Start();
        }


        /// <summary>
        /// Edits selected image
        /// </summary>
        /// <param name="path">Path to selected image file</param>
        private void EditImg(string path)
        {
            if (frameListBox.SelectedIndex == -1) return;

            // null traking variables
            trackedIMG = null;
            trackingMD5 = null;

            string imgName = Path.GetFileName(path);
            trackedIMG = path;
            trackingMD5 = FileMethods.GetMD5Hash(path);

            try
            {
                var runFile = new ProcessStartInfo(path) { Verb = "edit" };
                Process.Start(runFile);

                statusbarMainMessage.Text = ("Editing " + imgName);
            }
            catch (Exception ex)
            {
                statusbarMainMessage.Text = ("Error: " + ex.Message);
                string err = ("Error: " + ex.Message);
                MessageBox.Show(err, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                // Log exception
                ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + ex.Message);
            }

            statusTimer.Start();
        }


        /// <summary>
        /// Displays selected image's file properties
        /// </summary>
        /// <param name="path">Path to selected image file</param>
        private void FileProps(string path)
        {
            if (frameListBox.SelectedIndex == -1) return;

            string imgName = Path.GetFileName(path);

            try
            {
                ShellClass.DisplayFileProperties(path);
                statusbarMainMessage.Text = ("Displaying properties for " + imgName);
            }
            catch (Exception ex)
            {
                statusbarMainMessage.Text = ("Error: " + ex.Message);
                string err = ("Error: " + ex.Message);
                MessageBox.Show(err, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                // Log exception
                ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + ex.Message);
            }

            statusTimer.Start();
        }


        /// <summary>
        /// Method to load media scan to labelPicBox without locking image file 
        /// </summary>
        private Bitmap LoadBitmapNolock(string path)
        {
            string ext = Path.GetExtension(path).ToLower();

            if (ext != ".pcx")
            {
                try
                {
                    if (File.Exists(path))
                        using (var img = Image.FromFile(path))
                        {
                            return new Bitmap(img);
                        }
                    else
                        return null;
                }
                catch
                {
                    return null;
                }
            }
            else
            {
                try
                {
                    if (File.Exists(path))
                    {
                        try
                        {
                            using (var img = Kawa.Tools.BitmapData.ReadPCX(path))
                            {
                                return new Bitmap(img);
                            }
                        }
                        catch (FormatException ex)
                        {
                            string err = ("Error adding " + path + ": " + ex.Message);
                            MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            this.statusbarMainMessage.Text = string.Format("Error adding " + path);
                            statusTimer.Start();

                            // Log exception
                            ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + ex.Message);
                            return null;
                        }
                    }

                    else
                        return null;
                }
                catch
                {
                    return null;
                }
            }
        }
        
        
        /// <summary>
        /// Extracts idividual frames from animated GIF 
        /// </summary>
        /// <param name="aniGIF">Input file</param>
        /// <returns>idividual frames</returns>
        private static Image[] GetFrames(Image aniGIF)
        {
            int numFrames = aniGIF.GetFrameCount(FrameDimension.Time);
            Image[] frames = new Image[numFrames];

            //double i = 0.000;
            for (int i = 0; i < numFrames; i++)
            {
                aniGIF.SelectActiveFrame(FrameDimension.Time, i);
                frames[i] = ((Image)aniGIF.Clone());
            }

            return frames;
        }

        #endregion Image Methods

        
        #region Thumnail Functions

        /// <summary>
        /// Add/Rearrange thumbnails to match frameListBox.Items
        /// </summary>
        private void AddThumbs()
        {
            // Remove existing thumns
            thumbPreviewPnl.Controls.Clear();

            // Sequentially readd thumbs for each frameListBox item
            foreach (string frame in frameListBox.Items)
            {
                // Add PictureBox for thumbnail display
                PictureBox thumb = new PictureBox();
                thumbPreviewPnl.Controls.Add(thumb);
                thumb.Size = new System.Drawing.Size(160, 100);
                thumb.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
                thumb.SizeMode = PictureBoxSizeMode.Zoom;
                thumb.AllowDrop = true;

                // Add context menu
                thumb.ContextMenuStrip = cntxtMenu;

                // Load image as thumbnail
                Bitmap prevImg = LoadBitmapNolock(frame);
                string imgName = Path.GetFileName(frame);
                if (File.Exists(frame))
                {
                    thumb.Image = prevImg;
                    thumb.ImageLocation = frame;
                }

                // Set tooltip for thumb
                this.toolTip.SetToolTip(thumb, imgName);

                // Add event handlers
                thumb.Click += new System.EventHandler(this.Thumb_Click);
                thumb.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Thumb_MouseDown);
                thumb.DoubleClick += new System.EventHandler(this.Thumb_DoubleClick);
                thumb.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Thumb_MouseUp);
                thumb.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Thumb_MouseMove);
            }
        }


        /// <summary>
        /// Change cursor when dragging
        /// </summary>
        private void Thumb_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                PictureBox thumb = (PictureBox)sender;

                Point tmbsMouseCoords = this.clickedThumb.PointToClient(Control.MousePosition);

                // Disallow cursor change if less than minimum mouse movement
                if (!(tmbsMouseCoords.Y > thumbsLastMouseCoords.Y - 5 &
                    tmbsMouseCoords.Y < thumbsLastMouseCoords.Y + 5 &
                    tmbsMouseCoords.X > thumbsLastMouseCoords.X - 10 &
                    tmbsMouseCoords.X < thumbsLastMouseCoords.X + 10))
                {
                    //Save the current mouse position as the last mouse position
                    thumbsLastMouseCoords = tmbsMouseCoords;

                    MemoryStream move = new MemoryStream(Properties.Resources.CursorMove);
                    this.Cursor = new Cursor(move);

                    thumb.BackColor = System.Drawing.SystemColors.ControlDark; //Color.Transparent;
                    thumb.Image = null;
                }
            }
        }


        /// <summary>
        /// Swap frame positions on drop
        /// </summary>
        private void Thumb_MouseUp(object sender, MouseEventArgs e)
        {
            thumbDrag = false;

            // Restore Cursor When Mouse Button Released
            this.Cursor = Cursors.Arrow;

            PictureBox clickedThumb = (PictureBox)sender;
            string img = null;
            Control hoveredThumb = FindControlAtCursor(this);
            int hoveredIndex;
            if (hoveredThumb is PictureBox)
            {
                if (hoveredThumb.Parent == thumbPreviewPnl)
                {
                    hoveredIndex = thumbPreviewPnl.Controls.GetChildIndex(hoveredThumb);

                    img = ((PictureBox)hoveredThumb).ImageLocation;

                    // Reload thumbnail image
                    Bitmap prevImg = LoadBitmapNolock(clickedFrameImg);
                    if (File.Exists(clickedFrameImg))
                    {
                        clickedThumb.Image = prevImg;
                        clickedThumb.ImageLocation = clickedFrameImg;
                    }

                    // Return if dropped index is the same
                    if (hoveredIndex == draggedThumbIdx) return;

                    // Move thumb to dropped index
                    thumbPreviewPnl.Controls.SetChildIndex(clickedThumb, hoveredIndex);

                    // Deselect previously selected thumb
                    foreach (Control thumb in thumbPreviewPnl.Controls)
                        thumb.BackColor = Color.Transparent;

                    // Select clicked thumbnail
                    clickedThumb.BackColor = System.Drawing.Color.Blue;

                    // Reorder frameListBox items to match
                    frameListBox.Items.Clear();
                    foreach (Control thumb in thumbPreviewPnl.Controls)
                    {
                        string newImg = ((PictureBox)thumb).ImageLocation;
                        frameListBox.Items.Add(newImg);
                    }

                    // Track changes
                    changedImgList.Clear();
                    foreach (string listImg in frameListBox.Items)
                        changedImgList.Add(listImg);

                    img = ((PictureBox)clickedThumb).ImageLocation;
                    frameListBox.SelectedItem = img;
                }
            }
        }


        /// <summary>
        /// Selects image on thumbnail click
        /// </summary>
        private void Thumb_MouseDown(object sender, MouseEventArgs e)
        {
            thumbDrag = true;
            
            // Get clicked thumnail and cursor position
            clickedThumb = (PictureBox)sender;
            thumbsLastMouseCoords = this.clickedThumb.PointToClient(Control.MousePosition);

            // Get image path
            clickedFrameImg = clickedThumb.ImageLocation;
            frameListBox.SelectedItem = clickedFrameImg;

            // Remove selection color from any previously selected thumb
            foreach (Control thumb in thumbPreviewPnl.Controls)
                thumb.BackColor = Color.Transparent;

            // Select clicked thumbnail
            clickedThumb.BackColor = System.Drawing.Color.Blue;

            // Get current thumb index
            int thumbIndex = thumbPreviewPnl.Controls.GetChildIndex(clickedThumb);
            draggedThumbIdx = thumbIndex;
        }


        /// <summary>
        /// Select thumb on click
        /// </summary>
        private void Thumb_Click(object sender, EventArgs e)
        {
            ResetNullName();

            // Get clicked thumnail
            clickedThumb = (PictureBox)sender;
            string frame = clickedThumb.ImageLocation;
            frameListBox.SelectedItem = frame;

            // Remove selection color from any previously selected thumb
            foreach (Control thumb in thumbPreviewPnl.Controls)
                thumb.BackColor = Color.Transparent;

            // Select clicked thumbnail
            clickedThumb.BackColor = System.Drawing.Color.Blue;
        }


        /// <summary>
        /// Get control at specified point (used by FindControlAtCursor())
        /// </summary>
        public static Control FindControlAtPoint(Control container, Point pos)
        {
            Control child;
            foreach (Control c in container.Controls)
            {
                if (c.Visible && c.Bounds.Contains(pos))
                {
                    child = FindControlAtPoint(c, new Point(pos.X - c.Left, pos.Y - c.Top));
                    if (child == null) return c;
                    else return child;
                }
            }
            return null;
        }


        /// <summary>
        /// Get thumb under cursor
        /// </summary>
        public static Control FindControlAtCursor(Form form)
        {
            Point pos = Cursor.Position;
            if (form.Bounds.Contains(pos))
                return FindControlAtPoint(form, form.PointToClient(pos));
            return null;
        }


        /// <summary>
        /// Opens image in default viewer on thumbnail double click
        /// </summary>
        private void Thumb_DoubleClick(object sender, EventArgs e)
        {
            // frameListBox.SelectedItem selected on mouse down
            ViewImg(frameListBox.SelectedItem.ToString());
        }

        #endregion Thumnail Functions


        #region Player Methods

        /// <summary>
        /// Rewinds to beginning
        /// </summary>
        private void Rewind()
        {
            frameListBox.SelectedIndex = 0;
        }


        /// <summary>
        /// Step back one frame
        /// </summary>
        private void StepBack()
        {
            if (frameListBox.SelectedIndex > 0)
                frameListBox.SelectedIndex = frameListBox.SelectedIndex - 1;
        }


        /// <summary>
        /// Stops SEQ play
        /// </summary>
        private void Stop()
        {
            try
            {
                fileStream.Close();
                fileStream.Dispose();
            }
            catch { }

            curFrame = 0;
            seekBar.Value = 0;
            seqPicBox.Image = null;
            LoadSEQ();

            ManageButtons();
            playBtn.Enabled = true;
            playCntxtMenu.Enabled = true;
            stopBtn.Enabled = false;
            stopCntxtMenu.Enabled = false;
            editSEQCntxtMenu.Enabled = true;
        }


        /// <summary>
        /// Pause SEQ play
        ///     (To be implemented)
        /// </summary>
        private void Pause()
        {
            //int prevCurFrame = 0;

            fileStream.Close();
            fileStream.Dispose();
            //frameCount = 0;
            //curFrame = 0;
        }


        /// <summary>
        /// Plays loaded SEQ file
        /// </summary>
        private void Play(string seqFile)
        {
            try
            {
                playMode = null;
                playBtn.Enabled = false;
                playCntxtMenu.Enabled = false;
                stopBtn.Enabled = true;
                stopCntxtMenu.Enabled = true;
                editSEQCntxtMenu.Enabled = false;

                // Release fileStream
                if (fileStream != null)
                    try
                    {
                        fileStream.Close();
                        fileStream.Dispose();
                    }
                    catch { }

                //frameCount = 0;
                if (!File.Exists(seqFile)) return;

                seqLoaded = true;
                seqPicBox.Image = null;

                fileStream = new BinaryReader(File.Open(seqFile, FileMode.Open));

                //palette = new int[256 * 3];
                screen = new byte[320 * 200];
                bitmap = new Bitmap(320, 200, PixelFormat.Format8bppIndexed);
                seqPicBox.Image = bitmap;
                seqPicBox.SizeMode = PictureBoxSizeMode.StretchImage;

                //horizSplitContainer Panel2 size 902, 132
                if (viewMenuAspectCorrection.Checked == true)
                {
                    //horizSplitContainer splitter distance 565
                    if (this.Height < 810) //918, 810
                        this.Height = 810;
                    this.MinimumSize = new System.Drawing.Size(918, 815);
                    playerPanel.Size = new System.Drawing.Size(650, 540);
                    playerControlPnl.Location = new System.Drawing.Point(251, 507);
                    seqPicBox.Size = new System.Drawing.Size(640, 480);
                }
                else
                {
                    //horizSplitContainer splitter distance 480
                    if (this.Height == 810) //918, 725
                        this.Height = 725;
                    this.MinimumSize = new System.Drawing.Size(918, 730);
                    playerPanel.Size = new System.Drawing.Size(650, 460);
                    playerControlPnl.Location = new System.Drawing.Point(251, 427);
                    seqPicBox.Size = new System.Drawing.Size(640, 400);
                }

                if (viewMenuExamMode.Checked == true)
                    exam = true;

                curFrame = 1;
                frameCount = fileStream.ReadUInt16();
                ReadPaletteChunk(fileStream.ReadInt32());
                seekBar.Maximum = frameCount;
                seekBar.Value = 0;

                DecodeNextFrame();

                timer = new System.Windows.Forms.Timer();
                timer.Interval = 100;
                timer.Tick += (s, e) =>
                {
                    if (curFrame <= frameCount)
                        seekBar.Value = curFrame;

                    DecodeNextFrame();
                    if (curFrame > frameCount)
                    {
                        stopBtn.Enabled = false;
                        timer.Stop();
                        timer.Dispose();
                        fileStream.Close();
                        fileStream.Dispose();
                        endPlayTimer.Start();
                        playBtn.Enabled = true;
                        playCntxtMenu.Enabled = true;
                        stopBtn.Enabled = false;
                        stopCntxtMenu.Enabled = false;
                        editSEQCntxtMenu.Enabled = true;
                        statusTimer.Start();
                        return;
                    }
                };

                timer.Start();
            }
            catch (Exception ex)
            {
                string err = ("Error: " + ex.Message);
                MessageBox.Show(err, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.statusbarMainMessage.Text = ("Error: " + ex.Message);
                statusTimer.Start();

                // Log exception
                ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + ex.Message);
            }
        }


        /// <summary>
        /// Gets First Frame of SEQ
        /// </summary>
        private void GetFirstFrame()
        {
            try
            {
                playMode = "FirstFrame";

                playBtn.Enabled = false;
                playCntxtMenu.Enabled = false;
                stopBtn.Enabled = false;
                stopCntxtMenu.Enabled = false;
                editSEQCntxtMenu.Enabled = false;

                // Release fileStream
                if (fileStream != null) try
                    {
                        fileStream.Close();
                        fileStream.Dispose();
                    }
                    catch { }

                frameCount = 0;
                if (!File.Exists(seqFile)) return;

                seqLoaded = true;
                seqPicBox.Image = null;

                fileStream = new BinaryReader(File.Open(seqFile, FileMode.Open));

                screen = new byte[320 * 200];
                bitmap = new Bitmap(320, 200, PixelFormat.Format8bppIndexed);
                seqPicBox.SizeMode = PictureBoxSizeMode.StretchImage;

                outputFrame = null;

                frameCount = fileStream.ReadUInt16();
                ReadPaletteChunk(fileStream.ReadInt32());

                DecodeNextFrame();

                timer = new System.Windows.Forms.Timer();
                timer.Interval = 100;
                timer.Tick += (s, e) =>
                {
                    if (curFrame == 2)
                    {
                        timer.Stop();
                        playMode = null;
                        curFrame = -2;
                        fileStream.Close();
                        fileStream.Dispose(); try
                        {
                            fileStream.Close();
                            fileStream.Dispose();
                        }
                        catch { }

                        ManageButtons();
                    }
                };
            }
            catch (Exception ex)
            {
                playMode = null;
                string err = ("Error: " + ex.Message);
                MessageBox.Show(err, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.statusbarMainMessage.Text = ("Error: " + ex.Message);
                statusTimer.Start();

                // Log exception
                ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + ex.Message);
            }
            seekBar.Value = 0;
        }


        /// <summary>
        /// 
        /// </summary>
        private void DrawScreen()
        {
            if (playMode == "FirstFrame" && curFrame == 2)
            {
                outputFrame = null;
                statusbarMainMessage.Text = string.Format("Frame {0} of {1}", curFrame, frameCount);
                return;
            }
            if (playMode == "Export")
                statusbarMainMessage.Text = string.Format("Processing frame {0} of {1}", curFrame, frameCount);

            if (String.IsNullOrEmpty(outputFrame))
                statusbarMainMessage.Text = string.Format("Frame {0} of {1}", curFrame, frameCount);

            // New player
            var bitmapData = bitmap.LockBits(new Rectangle(0, 0, 320, 200), ImageLockMode.ReadOnly, bitmap.PixelFormat);
            Marshal.Copy(screen, 0, bitmapData.Scan0, 320 * 200);
            bitmap.UnlockBits(bitmapData);

            seqPicBox.Image = bitmap;
            seqPicBox.Invalidate();

            if (!string.IsNullOrEmpty(outputFrame))
            {
                try
                {
                    bitmap.SaveEx(outputFrame);
                }
                catch (ArgumentException ex)
                {
                    string err = ("Error: " + ex.Message);
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    statusbarMainMessage.Text = string.Format("Error drawing frame {0}:", curFrame);
                    statusTimer.Start();

                    // Log exception
                    ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + ex.Message);
                    Stop();
                    return;
                }

                outputFrame = outputFrame.IncreaseSequence();
            }
        }


        /// <summary>
        /// 
        /// </summary>
        private void ReadPaletteChunk(int chunkSize)
        {
            // New player
            var paletteData = new byte[chunkSize];
            fileStream.Read(paletteData, 0, chunkSize);

            //SCI1.1 palette
            var palFormat = paletteData[32];
            var palColorStart = ((paletteData[25]) | (paletteData[26] << 8));
            var palColorCount = ((paletteData[29]) | (paletteData[30] << 8));

            var palOffset = 37;
            var palette = bitmap.Palette;

            for (var colorNo = palColorStart; colorNo < palColorStart + palColorCount; colorNo++)
            {
                if (palFormat == 0) //kSeqPalVariable
                    palOffset++;
                palette.Entries[colorNo] = Color.FromArgb(paletteData[palOffset++], paletteData[palOffset++], paletteData[palOffset++]);
            }
            bitmap.Palette = palette;

            DrawScreen();
        }


        /// <summary>
        /// 
        /// </summary>
        private void DecodeNextFrame()
        {
            if (playMode == "FirstFrame" && curFrame == 2)
            {
                outputFrame = null;
                seekBar.Value = 0;
                return;
            }

            try
            {
                var frameWidth = fileStream.ReadUInt16();
                var frameHeight = fileStream.ReadUInt16();
                var frameLeft = fileStream.ReadUInt16();
                var frameTop = fileStream.ReadUInt16();
                var colorKey = fileStream.ReadByte();
                var frameType = fileStream.ReadByte();
                fileStream.BaseStream.Seek(2, SeekOrigin.Current);
                var frameSize = fileStream.ReadUInt16();
                fileStream.BaseStream.Seek(2, SeekOrigin.Current);
                var rleSize = fileStream.ReadUInt16();
                fileStream.BaseStream.Seek(6, SeekOrigin.Current);
                var offset = fileStream.ReadUInt32();
                fileStream.BaseStream.Seek((long)offset, SeekOrigin.Begin); //is this right?

                if (exam)
                {
                    Array.Clear(screen, 0, screen.Length);
                    if (frameLeft > 0)
                        for (var i = frameTop; i < frameTop + frameHeight; i += 2)
                            screen[(i * 320) + frameLeft - 1] = 255;
                    if (frameLeft + frameWidth < 320)
                        for (var i = frameTop; i < frameTop + frameHeight; i += 2)
                            screen[(i * 320) + (frameLeft + frameWidth)] = 255;
                    if (frameTop > 0)
                        for (var i = frameLeft; i < frameLeft + frameWidth; i += 2)
                            screen[((frameTop - 1) * 320) + i] = 255;
                    if (frameTop + frameHeight < 200)
                        for (var i = frameLeft; i < frameLeft + frameWidth; i += 2)
                            screen[((frameTop + frameHeight) * 320) + i] = 255;
                }

                if (frameType == 0) //kSeqFrameFull
                {
                    //fileStream.Read(screen, frameTop * 320, frameWidth * frameHeight);				
                    var lineBuf = new byte[frameWidth];
                    do
                    {
                        fileStream.Read(lineBuf, 0, frameWidth);
                        Array.Copy(lineBuf, 0, screen, (frameTop * 320) + frameLeft, frameWidth);
                        frameTop++;
                    } while (--frameHeight > 0);
                }
                else
                {
                    var buf = new byte[frameSize];
                    fileStream.Read(buf, 0, frameSize);
                    DecodeFrame(buf, rleSize, frameSize - rleSize, frameTop, frameLeft, frameWidth, frameHeight, colorKey);
                }

                DrawScreen();
                curFrame++;
            }
            catch (Exception ex)
            {
                // Log exception
                string err = ("Error: " + ex.Message);
                ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + ex.Message);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        private bool DecodeFrame(byte[] rleData, int rleSize, int litSize, int top, int left, int width, int height, int colorKey)
        {
            var writeRow = top;
            int writeCol = left;
            int litPos = rleSize;
            int rlePos = 0;

            while (rlePos < rleSize)
            {
                var op = rleData[rlePos++];
                if ((op & 0xC0) == 0xC0)
                {
                    op &= 0x3F;
                    if (op == 0)
                    {
                        //Go to next line in target buffer
                        writeRow++;
                        writeCol = left;
                    }
                    else
                    {
                        //Skip bytes on the current line
                        writeCol += op;
                    }
                }
                else if ((op & 0x80) == 0x80)
                {
                    op &= 0x3F;
                    if (op == 0)
                    {
                        //Copy remainder of current line
                        var rem = width - (writeCol - left);
                        Array.Copy(rleData, litPos, screen, (writeRow * 320) + writeCol, rem);
                        writeRow++;
                        writeCol = left;
                        litPos += rem;
                    }
                    else
                    {
                        //Copy bytes
                        Array.Copy(rleData, litPos, screen, (writeRow * 320) + writeCol, op);
                        writeCol += op;
                        litPos += op;
                    }
                }
                else
                {
                    var count = ((op & 7) << 8) | rleData[rlePos++];
                    switch (op >> 3)
                    {
                        case 2: //Skip bytes
                            writeCol += count;
                            break;
                        case 3: //Copy bytes
                            Array.Copy(rleData, litPos, screen, (writeRow * 320) + writeCol, count);
                            writeCol += count;
                            litPos += count;
                            break;
                        case 6: //Copy rows
                            if (count == 0)
                                count = height - writeRow;

                            for (var i = 0; i < count; i++)
                            {
                                Array.Copy(rleData, litPos, screen, (writeRow * 320) + writeCol, width);
                                litPos += width;
                                writeRow++;
                            }
                            break;
                        case 7: //Skip rows
                            if (count == 0)
                                count = height - writeRow;
                            writeRow += count;
                            break;
                        default:
                            MessageBox.Show(string.Format("Unsupported operation {0} encountered.", op >> 3));
                            return false;
                    }
                }
            }

            return true;
        }


        /// <summary>
        /// Step forward one frame
        /// </summary>
        private void StepNext()
        {
            if (frameListBox.SelectedIndex < frameListBox.Items.Count - 1)
                frameListBox.SelectedIndex = frameListBox.SelectedIndex + 1;
        }


        /// <summary>
        /// Fast forwards to end
        /// </summary>
        private void FastForward()
        {
            frameListBox.SelectedIndex = frameListBox.Items.Count - 1;
        }


        /// <summary>
        /// Sets playback volume
        ///     Possible feature
        /// </summary>
        private void SetVolume()
        {

        }


        /// <summary>
        /// Sets playback volume
        ///     Possible feature
        /// </summary>
        private void Mute()
        {

        }

        #endregion Player Methods


        #region MakeSeq

        /// <summary>
        /// Writes SEQ from input images
        /// </summary>
        private void MakeSeq(List<string> inFiles)
        {
            if (Directory.Exists(projPath))
            {
                seqPicBox.Image = null;
                seqLoaded = true;

                var inFile = Path.GetDirectoryName(inFiles.First()) + "\\" + Path.GetFileNameWithoutExtension(inFiles.First());

                string seqTarget = projPath;

                if (toolsMenuUseSubDir.Checked == true)
                {
                    seqTarget = projPath + "\\SEQ\\";
                    if (!Directory.Exists(seqTarget))
                        Directory.CreateDirectory(seqTarget);
                }

                var outFile = seqTarget + seqNameTxtBox.Text + ".SEQ";
                string outName = seqNameTxtBox.Text + ".SEQ";

                var frameCount = inFiles.Count;

                statusbarMainMessage.Text = "Writing " + frameCount + " frames from " + inFile + " to " + outFile + "...";

                // Check if outFile is in use
                var fi = new FileInfo(outFile);
                if (File.Exists(outFile) && IsFileLocked(fi)) return;

                var bitmap = new byte[64000];
                var previous = new byte[64000];
                var palette = new byte[768];

                var fileStream = new BinaryWriter(File.Open(outFile, FileMode.Create));
                fileStream.Write((UInt16)inFiles.Count);

                var palChunk = new byte[1024];
                palChunk[10] = 1;
                palChunk[31] = 1;
                palChunk[32] = 1;
                palChunk[29] = 255;
                fileStream.Write(palChunk.Length);

                var file = string.Empty;
                try
                {
                    file = inFiles[0];
                    try
                    {
                        Kawa.Tools.BitmapData.GetPixels(file, ref palette, ref bitmap);
                    }
                    catch (Exception ex)
                    {
                        string fileName = Path.GetFileName(file);
                        //string err = ("Error: " + ex.Message);
                        MessageBox.Show("Error adding " + fileName + " to " + outName + ": " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        statusbarMainMessage.Text = string.Format("Error working on frame {0}:", file);
                        statusTimer.Start();

                        // Log exception
                        ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + "Error adding " + fileName + " to " + outName + ": " + ex.Message);

                        // Release fileStream
                        try
                        {
                            fileStream.Close();
                            fileStream.Dispose();
                        }
                        catch { }

                        return;
                    }

                    Array.Copy(palette, 0, palChunk, 37, 768);

                    fileStream.Write(palChunk);

                    foreach (var frame in inFiles)
                    {
                        file = frame;

                        var frameLeft = 0;
                        var frameTop = 0;
                        var frameRight = 319;
                        var frameBottom = 199;

                        try
                        {
                            Kawa.Tools.BitmapData.GetPixels(file, ref palette, ref bitmap);
                        }
                        catch (Exception ex)
                        {
                            string fileName = Path.GetFileName(file);
                            string err = ("Error: " + ex.Message);
                            MessageBox.Show("Error adding " + fileName + " to " + outName + ": " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            statusbarMainMessage.Text = string.Format("Error working on frame {0}:", file);
                            statusTimer.Start();

                            // Log exception
                            ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + "Error adding " + fileName + " to " + outName + ": " + ex.Message);

                            // Release fileStream
                            try
                            {
                                fileStream.Close();
                                fileStream.Dispose();
                            }
                            catch { }

                            return;
                        }

                        var found = false;
                        for (frameTop = 0; frameTop < 200; frameTop++)
                        {
                            for (var c = 0; c < 320; c++)
                            {
                                if (bitmap[(frameTop * 320) + c] != previous[(frameTop * 320) + c])
                                {
                                    found = true;
                                    break;
                                }
                            }
                            if (found)
                                break;
                        }
                        found = false;
                        for (frameBottom = 199; frameBottom > frameTop; frameBottom--)
                        {
                            for (var c = 0; c < 320; c++)
                            {
                                if (bitmap[(frameBottom * 320) + c] != previous[(frameBottom * 320) + c])
                                {
                                    found = true;
                                    break;
                                }
                            }
                            if (found)
                                break;
                        }
                        frameBottom++;
                        found = false;
                        for (frameLeft = 0; frameLeft < 320; frameLeft++)
                        {
                            for (var c = 0; c < 200; c++)
                            {
                                if (bitmap[(c * 320) + frameLeft] != previous[(c * 320) + frameLeft])
                                {
                                    found = true;
                                    break;
                                }
                            }
                            if (found)
                                break;
                        }
                        found = false;
                        for (frameRight = 319; frameRight > frameLeft; frameRight--)
                        {
                            for (var c = 0; c < 200; c++)
                            {
                                if (bitmap[(c * 320) + frameRight] != previous[(c * 320) + frameRight])
                                {
                                    found = true;
                                    break;
                                }
                            }
                            if (found)
                                break;
                        }
                        frameRight++;

                        var frameWidth = frameRight - frameLeft;
                        var frameHeight = frameBottom - frameTop;

                        if (frameHeight + frameWidth == 0)
                        {
                            frameWidth = frameHeight = frameRight = frameBottom = 1;
                            frameTop = frameLeft = 0;
                        }

                        var actualF = new byte[frameHeight * frameWidth];
                        for (var l = 0; l < frameHeight; l++)
                        {
                            Array.Copy(bitmap, ((frameTop + l) * 320) + frameLeft, actualF, (l * frameWidth) /* + frameLeft */, frameWidth);
                        }

                        var colorKey = 255;
                        var frameType = 0; //full frame
                        var frameSize = frameWidth * frameHeight;
                        var rleSize = 0;
                        fileStream.Write((UInt16)frameWidth);
                        fileStream.Write((UInt16)frameHeight);
                        fileStream.Write((UInt16)frameLeft);
                        fileStream.Write((UInt16)frameTop);
                        fileStream.Write((byte)colorKey);
                        fileStream.Write((byte)frameType);
                        fileStream.Write((UInt16)0);
                        fileStream.Write((UInt16)frameSize);
                        fileStream.Write((UInt16)0);
                        fileStream.Write((UInt16)rleSize);
                        fileStream.Write((UInt32)0);
                        fileStream.Write((UInt16)0);
                        var offset = fileStream.BaseStream.Position + 4;
                        fileStream.Write((UInt32)offset);
                        fileStream.Write(actualF);

                        Array.Copy(bitmap, 0, previous, 0, 64000);
                    }
                }
                catch (FormatException ex)
                {
                    string err = ("Error: " + ex.Message);
                    statusbarMainMessage.Text = string.Format("Error working on frame {0}:", file);
                    //MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    // Log exception
                    ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + ex.Message);
                }

                statusbarMainMessage.Text = "Done!";
                fileStream.Flush();
                fileStream.Close();

                seqFile = outFile;
                seqPicBox.SizeMode = PictureBoxSizeMode.Zoom;
                SEQNameLbl.Text = Path.GetFileName(outFile);
                Bitmap img = LoadBitmapNolock(inFiles.First());
                string imgName = Path.GetFileName(inFiles.First());
                seqLoaded = true;

                if (File.Exists(inFiles.First()))
                    seqPicBox.Image = img;
                //Play(seqFile);

                ManageButtons();
                statusTimer.Start();
            }
            else
            {
                MessageBox.Show("Project Folder not found! Please use from SCI Companion with project loaded.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        /// <summary>
        /// Checks if file is in use
        /// </summary>
        /// <param name="file">the file</param>
        /// <returns>if file is in use</returns>
        protected virtual bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException ex)
            {
                // File locked
                string err = ("Error: " + ex.Message);
                //MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.statusbarMainMessage.Text = (err);
                statusTimer.Start();

                // Log exception
                ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + ex.Message);

                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            // Not locked
            return false;
        }

        #endregion MakeSeq


        #region DragDrop Functions

        /// <summary>
        /// Set image for copy/move cursor
        /// </summary>
        protected void LoadImage()
        {
            try
            {
                nextImage = new Bitmap(LoadBitmapNolock(lastFilename));
                //nextImage = new Bitmap(lastFilename);
                this.Invoke(new AssignImageDlgt(AssignImage));
            }
            catch { }
        }


        /// <summary>
        /// Get/Check Dropped File (first if multiple files)
        /// </summary>
        /// <param name="filePath">Allowed file</param>
        /// <param name="e"></param>
        /// <returns></returns>
        protected bool GetFilename(out string filePath, DragEventArgs e)
        {
            bool ret = false;
            filePath = String.Empty;

            if ((e.AllowedEffect & DragDropEffects.Copy) == DragDropEffects.Copy)
            {
                Array data = ((IDataObject)e.Data).GetData("FileDrop") as Array;

                if (data != null)
                {
                    if ((data.Length >= 1) && (data.GetValue(0) is String))
                    {
                        filePath = ((string[])data)[0];
                        string ext = Path.GetExtension(filePath).ToLower();
                        if (ext == ".pcx" || ext == ".gif" || ext == ".bmp" || ext == ".tif" || ext == ".tiff" || ext == ".png") // || ext == ".jpg"
                        {
                            ret = true;
                        }
                    }
                }
            }
            return ret;
        }


        /// <summary>
        /// Set dragged thumb image
        /// </summary>
        protected void AssignImage()
        {
            Bitmap prevImg = LoadBitmapNolock(lastFilename);

            if (dragDestination == frameListBox.Name)
            {
                listThumbPicBox.Width = 100;
                // 100    iWidth
                // ---- = ------
                // tHeight  iHeight

                listThumbPicBox.Height = nextImage.Height * 100 / nextImage.Width;
                SetThumbnailLocation(this.PointToClient(new Point(lastX, lastY)));

                // Load image as thumbnail
                if (File.Exists(lastFilename))
                    listThumbPicBox.Image = prevImg;
            }

            if (dragDestination == thumbPreviewPnl.Name)
            {
                previewThumbPicBox.Width = 100;
                // 100    iWidth
                // ---- = ------
                // tHeight  iHeight

                previewThumbPicBox.Height = nextImage.Height * 100 / nextImage.Width;
                SetThumbnailLocation(this.PointToClient(new Point(lastX, lastY)));

                // Load image as thumbnail
                if (File.Exists(lastFilename))
                    previewThumbPicBox.Image = prevImg;
            }
        }


        /// <summary>
        /// Move dragged thumb with cursor
        /// </summary>
        /// <param name="p">location</param>
        protected void SetThumbnailLocation(Point p)
        {
            if (dragDestination == frameListBox.Name)
            {
                if (listThumbPicBox.Image == null)
                {
                    listThumbPicBox.Visible = false;
                }
                else
                {
                    p.X -= listThumbPicBox.Width / 2;
                    p.Y -= listThumbPicBox.Height / 2;
                    listThumbPicBox.Location = p;
                    listThumbPicBox.Visible = true;
                }
            }

            if (dragDestination == thumbPreviewPnl.Name)
            {
                if (previewThumbPicBox.Image == null)
                {
                    previewThumbPicBox.Visible = false;
                }
                else
                {
                    p.X -= previewThumbPicBox.Width / 2;
                    p.Y -= previewThumbPicBox.Height / 2;
                    previewThumbPicBox.Location = p;
                    previewThumbPicBox.Visible = true;
                }
            }
        }

        #endregion DragDrop Functions


        #region Form Methods

        /// <summary>
        /// Check if 2nd instance of assembly (i.e. another SEQ file double clicked in Explorer)
        /// </summary>
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == Program.NativeMethods.WM_SHOWME)
                Reload();

            base.WndProc(ref m);
        }


        /// <summary>
        /// Get arguments from new instance
        /// </summary>
        private void Reload()
        {
            if (WindowState == FormWindowState.Minimized)
                WindowState = FormWindowState.Normal;

            // get current "TopMost" value
            bool top = TopMost;

            // move form to the top
            TopMost = true;

            // set it back
            TopMost = top;

            string arg = string.Empty;
            string tempFile = Path.GetTempPath() + "argtmp";

            try
            {
                using (StreamReader writer = new StreamReader(tempFile))
                {
                    arg = writer.ReadLine();
                    writer.Close();
                }
                File.Delete(tempFile);
            }
            catch { }

            // Load SEQ if arg is seqFile
            if (arg.ToLower().EndsWith(".seq"))
            {
                seqFile = arg; try
                {
                    // Release fileStream
                    fileStream.Close();
                    fileStream.Dispose();
                }
                catch { }

                SEQNameLbl.Text = Path.GetFileName(seqFile);
                Play(seqFile);
            }

            // Load project if arg is projFile
            if (arg.ToLower().EndsWith(".sqp"))
            {
                projFile = arg;
                ReadSaveFile();
            }
        }


        /// <summary>
        /// Get aspect ratio correction preference
        /// </summary>
        private void GetAspect()
        {
            aspect = null;

            // Check game.ini for aspect preference
            string gameINI = projPath + "\\game.ini"; //Path.Combine(projPath, "game.ini");
            if (File.Exists(gameINI))
            {
                StreamReader reader = new StreamReader(gameINI);

                string line = null;
                while ((line = reader.ReadLine()) != null)
                {
                    if (line.Contains("UseSierraAspectRatio="))
                    {
                        line = line.Replace("UseSierraAspectRatio=", null);

                        if (!String.IsNullOrEmpty(line))
                            line = line.Trim();

                        if (line == "true")
                            aspect = true;

                        if (line == "false")
                            aspect = false;
                    }
                }
                reader.Close();
            }

            // If null, get default value
            if (aspect == null)
            {
                string value = string.Empty;
                try
                {
                    value = Registry.GetValue(@"HKEY_CURRENT_USER\Software\mtnPhilms\SCICompanion\SCICompanion\", "OriginalAspectRatio", 0).ToString();
                }
                catch (Exception ex)
                {
                    string err = ("Error: " + ex.Message);
                    this.statusbarMainMessage.Text = (err);
                    statusTimer.Start();

                    // Log exception
                    ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + err);
                }

                if (value == "0")
                    aspect = false;

                if (value == "1")
                    aspect = true;
            }

            // If still null, fall back on Sierra default
            if (aspect == null)
                aspect = true;

            if (aspect == true)
                viewMenuAspectCorrection.Checked = true;

            if (aspect == false)
                viewMenuAspectCorrection.Checked = false;
        }


        /// <summary>
        /// Set aspect ratio for playback
        /// </summary>
        private void SetAspect()
        {
            //horizSplitContainer Panel2 size 902, 132

            if (aspect == true) //if (viewMenuAspectCorrection.Checked == true)
            {
                //horizSplitContainer splitter distance 565
                if (this.Height < 815) //918, 810
                    this.Height = 815;
                this.MinimumSize = new System.Drawing.Size(918, 815);
                playerPanel.Size = new System.Drawing.Size(650, 540);
                playerControlPnl.Location = new System.Drawing.Point(251, 507);
                seqPicBox.Size = new System.Drawing.Size(640, 480);
            }
            
            if (aspect == false)
            {
                //horizSplitContainer splitter distance 480
                if (this.Height == 815) //918, 725
                    this.Height = 730;
                this.MinimumSize = new System.Drawing.Size(918, 730);
                playerPanel.Size = new System.Drawing.Size(650, 460);
                playerControlPnl.Location = new System.Drawing.Point(251, 427);
                seqPicBox.Size = new System.Drawing.Size(640, 400);
            }
        }


        /// <summary>
        /// Checks for changes and prompts for save
        /// </summary>
        private void PromptSave()
        {
            bool changed = false;
            if (trackingImgList != changedImgList)
                changed = true;
            if (trackingName != changedName)
                changed = true;

            if (changed == true)
            {
                DialogResult dialogResult = MessageBox.Show("Would you like to save your changes?", "Project Changed", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                    Save();
            }
        }


        /// <summary>
        /// Manages buttons according to status of list and selections
        /// </summary>
        private void ManageButtons()
        {
            // Disable Buttons if list is empty
            if (frameListBox.Items.Count == 0)
            {
                clearAllBtn.Enabled = false;
                removeBtn.Enabled = false;
                upBtn.Enabled = false;
                dnBtn.Enabled = false;
                removeCntxtMenu.Enabled = false;
                upBtn.Enabled = false;
                dnBtn.Enabled = false;
            }
            else if (frameListBox.SelectedIndex == -1)
            {
                // Disable Buttons if no items selected selected
                clearAllBtn.Enabled = true;
                removeBtn.Enabled = false;
                removeCntxtMenu.Enabled = false;
                upBtn.Enabled = false;
                dnBtn.Enabled = false;
            }
            else
            {
                // Enable Buttons if item is selected selected
                clearAllBtn.Enabled = true;
                removeBtn.Enabled = true;
                removeCntxtMenu.Enabled = true;
                upBtn.Enabled = true;
                dnBtn.Enabled = true;
            }

            // If selected item at top, disable UP Button
            if (frameListBox.SelectedIndex == 0)
                upBtn.Enabled = false;

            // If selected item at bottom, disable DOWN Button
            if (frameListBox.SelectedIndex == frameListBox.Items.Count - 1)
                dnBtn.Enabled = false;

            // Manage seqNameTxtBox entry
            if ((!String.IsNullOrEmpty(seqNameTxtBox.Text) || seqNameTxtBox.Text != "SEQ Name   ") && (frameListBox.Items.Count >= 2))
                makeSEQBtn.Enabled = true;

            if (String.IsNullOrEmpty(seqNameTxtBox.Text) || seqNameTxtBox.Text == "SEQ Name   " || frameListBox.Items.Count < 2)
                makeSEQBtn.Enabled = false;

            // Disable saveSEQBtn if not at least 2 images in list
            if (frameListBox.Items.Count <= 1)
            {
                saveSEQBtn.Enabled = false;
                trackedIMG = null;
                trackingMD5 = null;
            }

            // Disable edit SEQ button if list is not null
            if (frameListBox.Items.Count > 0)
            {
                editSEQBtn.Enabled = false;
            }
            else if (File.Exists(seqFile))
            {
                editSEQBtn.Enabled = true;
            }

            // Manage edit RESOURCE.CFG items
            string resCFG = string.Empty;
            if (Directory.Exists(projPath))
            {
                resCFG = Path.Combine(projPath, "RESOURCE.CFG");
                if (File.Exists(resCFG))
                {
                    toolsMenuAppendCFG.Enabled = true;
                    appendCFGBtn.Enabled = true;
                }
                else
                {
                    toolsMenuAppendCFG.Enabled = false;
                    appendCFGBtn.Enabled = false;
                }
            }

            // Manage player navigation buttons
            if (frameListBox.Items.Count > 1)
            {
                if (frameListBox.SelectedIndex == 0)
                {
                    rewindBtn.Enabled = false;
                    stepBackBtn.Enabled = false;
                }
                else
                {
                    rewindBtn.Enabled = true;
                    stepBackBtn.Enabled = true;
                }

                if (frameListBox.SelectedIndex == (frameListBox.Items.Count - 1))
                {
                    stepNextBtn.Enabled = false;
                    ffBtn.Enabled = false;
                }
                else
                {
                    stepNextBtn.Enabled = true;
                    ffBtn.Enabled = true;
                }
            }

            // Manage seqToolbar & player controls
            if (File.Exists(seqFile))
            {
                exportBtn.Enabled = true;
                fileMenuSaveSEQAs.Enabled = true;
                saveSEQAsBtn.Enabled = true;
                editSEQBtn.Enabled = true;
                editSEQCntxtMenu.Enabled = true;
                playBtn.Enabled = true;
                playCntxtMenu.Enabled = true;
                editSEQCntxtMenu.Enabled = true;
            }
            else
            {
                fileMenuSaveSEQAs.Enabled = false;
                saveSEQAsBtn.Enabled = false;
                playBtn.Enabled = false;
                playCntxtMenu.Enabled = false;
            }

            // Manage save controls
            if (File.Exists(projFile))
            {
                fileMenuSaveAs.Enabled = true;
                saveAsBtn.Enabled = true;
            }
            else
            {
                fileMenuSaveAs.Enabled = false;
                saveAsBtn.Enabled = false;
            }

            if (trackingImgList != changedImgList)
            {
                fileMenuSave.Enabled = true;
                saveBtn.Enabled = true;

                if (File.Exists(seqFile))
                {
                    fileMenuSaveSEQ.Enabled = true;
                    //saveSEQBtn.Enabled = true;
                }
                else
                {
                    fileMenuSaveSEQ.Enabled = false;
                    saveSEQBtn.Enabled = false;
                }
            }
        }
        

        /// <summary>
        /// Resets SEQ name tip text if name is null
        /// </summary>
        #region ResetNullName

        private void ResetNullName()
        {
            if (String.IsNullOrEmpty(seqNameTxtBox.Text))
            {
                seqNameTxtBox.ForeColor = System.Drawing.SystemColors.GrayText;
                seqNameTxtBox.Text = "SEQ Name   ";
                seqNameTxtBox.SelectionStart = seqNameTxtBox.Text.Length - 1;
                seqLbl.Text = null;
            }
            else if (seqNameTxtBox.Text != "SEQ Name   ")
            {
                seqNameTxtBox.ForeColor = System.Drawing.SystemColors.WindowText;
                seqLbl.Text = ".SEQ";
            }

            if (seqNameTxtBox.Text == "SEQ Name   ")
            {
                seqLbl.Text = null;
            }
        }

        private void MainFrm_MouseClick(object sender, MouseEventArgs e)
        {
            ResetNullName();
            this.Focus();
        }

        private void MainFrm_Click(object sender, EventArgs e)
        {
            ResetNullName();
            this.Focus();
        }

        private void horizSplitContainer_Click(object sender, EventArgs e)
        {
            ResetNullName();
            horizSplitContainer.Focus();
        }

        private void thumbPreviewPnl_Click(object sender, EventArgs e)
        {
            ResetNullName();
            thumbPreviewPnl.Focus();
        }

        private void splitContnr_Panel1_Click(object sender, EventArgs e)
        {
            ResetNullName();
            splitContnr.Focus();
        }

        private void playerPanel_Click(object sender, EventArgs e)
        {
            ResetNullName();
            playerPanel.Focus();
        }

        private void seqPicBox_Click(object sender, EventArgs e)
        {
            ResetNullName();
            seqPicBox.Focus();
        }

        private void playToolbar_Click(object sender, EventArgs e)
        {
            ResetNullName();
            playToolbar.Focus();
        }

        private void SEQNameLbl_Click(object sender, EventArgs e)
        {
            ResetNullName();
            SEQNameLbl.Focus();
        }

        private void seqToolbar_Click(object sender, EventArgs e)
        {
            ResetNullName();
            seqToolbar.Focus();
        }

        private void toolbar_Click(object sender, EventArgs e)
        {
            ResetNullName();
            toolbar.Focus();
        }

        private void menubar_Click(object sender, EventArgs e)
        {
            ResetNullName();
            menubar.Focus();
        }

        private void statusbar_Click(object sender, EventArgs e)
        {
            ResetNullName();
            statusbar.Focus();
        }

        #endregion ResetNullName


        /// <summary>
        /// Registers SEQ files with SEQTool
        /// </summary>
        private void AddSEQReg()
        {
            string seqToolEXE = new System.Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath.Replace("%20", " ");
            string seqToolPath = Path.GetDirectoryName(seqToolEXE);
            string value = "\"" + seqToolEXE.Replace("/", "\\") + "\" \"%1\"";

            Registry.ClassesRoot.CreateSubKey(".SEQ");
            Registry.SetValue("HKEY_CLASSES_ROOT\\.SEQ", "", "SEQ");

            Registry.ClassesRoot.CreateSubKey("SEQ\\shell\\open\\command");
            Registry.SetValue("HKEY_CLASSES_ROOT\\SEQ\\shell\\open\\command", "", value);
        }


        /// <summary>
        /// Registers project save files with SEQTool
        /// </summary>
        private void AddSQPReg()
        {
            string seqToolEXE = new System.Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath.Replace("%20", " ");
            string seqToolPath = Path.GetDirectoryName(seqToolEXE);
            string value = "\"" + seqToolEXE.Replace("/", "\\") + "\" \"%1\"";

            Registry.ClassesRoot.CreateSubKey(".SQP");
            Registry.SetValue("HKEY_CLASSES_ROOT\\.SQP", "", "SQP");

            Registry.ClassesRoot.CreateSubKey("SQP\\shell\\open\\command");
            Registry.SetValue("HKEY_CLASSES_ROOT\\SQP\\shell\\open\\command", "", value);
        }


        /// <summary>
        /// Resets status message
        /// </summary>
        private void statusTimer_Tick(object sender, EventArgs e)
        {
            playMode = null;
            statusTimer.Stop();
            this.statusbarMainMessage.Text = "Ready";
        }


        /// <summary>
        /// Resets seekbar
        /// </summary>
        private void endPlayTimer_Tick(object sender, EventArgs e)
        {
            seekBar.Value = 0;
            endPlayTimer.Stop();
        }


        /// <summary>
        /// Loads frames after ani GIF import
        /// </summary>
        private void importTimer_Tick(object sender, EventArgs e)
        {
            if (extracted == false)
                return;

            string framesPath = Path.Combine(projPath + "\\SEQ_SRC", Path.GetFileNameWithoutExtension(aniGIFPath));

            frameListBox.Items.Clear();
            trackingImgList.Clear();
            changedImgList.Clear();

            int numFrame = 1;
            foreach (Image frame in gifFrames)
            {
                frame.Save(framesPath + "Frame " + numFrame + ".gif");
                frameListBox.Items.Add(framesPath + "Frame " + numFrame + ".gif");

                numFrame++;
            }

            waitLbl.Visible = false;

            // Add thumbnails to match
            AddThumbs();

            frameListBox.SelectedIndex = 0;

            ManageButtons();

            extracted = false;
            importTimer.Stop();

            // Set tracking image list to compare against changes
            foreach (string listImg in frameListBox.Items)
                trackingImgList.Add(listImg);
            statusTimer.Start();
        }

        #endregion Form Methods


        #endregion Extra Functions
    }
}
