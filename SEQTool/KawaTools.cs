using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Globalization;
using Marshal = System.Runtime.InteropServices.Marshal;

namespace Kawa.Tools
{
	/// <summary>
	/// Adds a few handy extension methods.
	/// </summary>
	static public class Extensions	
	{
		/// <summary>
		/// Given something like a filename with a sequence number in it, returns the next filename.
		/// </summary>
		/// <remarks>
		/// The sequence number is the -last- stretch of consecutive digits in the string.
		/// </remarks>
		/// <example>"4.png" =&gt; "5.png"</example>
		/// <param name="filename">A string with a number in it, such as "frame003.png".</param>
		/// <param name="throwIfMissing">If true, throw an ArgumentException if there is no sequence in the string. If false, silently return the original string.</param>
		/// <returns>The next string in the sequence, such as "frame004.png".</returns>
		/// <exception cref="System.ArgumentException">Thrown when there is no sequence in the string and throwIfMissing is true.</exception>
		static public string IncreaseSequence(this string filename, bool throwIfMissing = false)
		{
			var lastNumberPos = -1;
			for (var i = 0; i < filename.Length; i++)
				if (char.IsDigit(filename[i]))
					lastNumberPos = i;
			if (lastNumberPos == -1)
			{
				if (throwIfMissing)
					throw new ArgumentException(string.Format("String \"{0}\" has no number sequence in it.", filename));
				else
					return filename;
			}
			var firstNumberPos = lastNumberPos;
			for (var i = lastNumberPos - 1; i >= 0; i--)
				if (char.IsDigit(filename[i]))
					firstNumberPos = i;
				else
					break;
			var length = lastNumberPos - firstNumberPos + 1;
			var numPart = filename.Substring(firstNumberPos, length);
			var number = int.Parse(numPart);
			number++;
			filename = filename.Substring(0, firstNumberPos) + number.ToString("D" + length) + filename.Substring(lastNumberPos + 1);
			return filename;
		}

		/// <summary>
		/// Given something like a filename with a sequence number in it, returns that filename without a sequence.
		/// </summary>
		/// <remarks>
		/// The sequence number is the -last- stretch of consecutive digits in the string.
		/// </remarks>
		/// <example>"foo-4.png" =&gt; "foo.png"</example>
		/// <param name="filename">A string with a number in it, such as "frame003.png".</param>
		/// <param name="blank">The string to use if the filename starts with a sequence, so that "0001.png" becomes "out.png" instead of ".png".</param>
		/// <returns>The filename with the sequence part removed.</returns>
		static public string RemoveSequence(this string filename, string blank = "out")
		{
			var lastNumberPos = -1;
			for (var i = 0; i < filename.Length; i++)
				if (char.IsDigit(filename[i]))
					lastNumberPos = i;
			if (lastNumberPos == -1)
				return filename;
			var firstNumberPos = lastNumberPos;
			for (var i = lastNumberPos - 1; i >= 0; i--)
				if (char.IsDigit(filename[i]))
					firstNumberPos = i;
				else
					break;
			while (firstNumberPos > 0 && char.IsPunctuation(filename[firstNumberPos - 1]))
				firstNumberPos--;
			if (firstNumberPos == 0)
				return blank + filename.Substring(lastNumberPos + 1);
			return filename.Substring(0, firstNumberPos) + filename.Substring(lastNumberPos + 1);
		}

		/// <summary>
		/// Truncates the given string to a specified length, with an ellipsis on the end.
		/// </summary>
		/// <param name="text">The string to truncate.</param>
		/// <param name="length">Maximum amount to retain.</param>
		/// <param name="suffix">The string to append at the end.</param>
		/// <returns>The truncated string.</returns>
		public static string Truncate(this string text, int length, string suffix = "...")
		{
			if (text.Length <= length)
				return text;
			return text.Remove(length - suffix.Length) + suffix;
		}

		/// <summary>
		/// Rounds a large file size to bigger orders of magnitude.
		/// </summary>
		/// <param name="fileSize">The file size in bytes.</param>
		/// <returns>The file size in bytes with "bytes" appended if it's less than 1024, or the best fitting suffix.</returns>
		public static string ToFileSize(this float size, CultureInfo cultureInfo)
		{
			var suffices = new[] { " bytes", " KiB", " MiB", " GiB", " TiB", " PiB", " EiB", " ZiB", " YiB" };
			var suffix = 0;
			while (size >= 1024 && suffix < suffices.Length)
			{
				size = size / 1024f;
				suffix++;
			}
			return (size.ToString("0.00", cultureInfo) + suffices[suffix]);
		}

		/// <summary>
		/// Rounds a large file size to bigger orders of magnitude.
		/// </summary>
		/// <param name="fileSize">The file size in bytes.</param>
		/// <returns>The file size in bytes with "bytes" appended if it's less than 1024, or the best fitting suffix.</returns>
		public static string ToFileSize(this float size)
		{
			return ToFileSize(size, CultureInfo.CurrentCulture);
		}

		/// <summary>
		/// Rounds a large file size to bigger orders of magnitude.
		/// </summary>
		/// <param name="fileSize">The file size in bytes.</param>
		/// <returns>The file size in bytes with "bytes" appended if it's less than 1024, or the best fitting suffix.</returns>
		public static string ToFileSize(this long size)
		{
			var suffices = new[] { " bytes", " KiB", " MiB", " GiB", " TiB", " PiB", " EiB", " ZiB", " YiB" };
			var suffix = 0;
			while (size >= 1024 && suffix < suffices.Length)
			{
				size = size / 1024;
				suffix++;
			}
			return (size + suffices[suffix]);
		}

		/// <summary>
		/// Rounds a large file size to bigger orders of magnitude.
		/// </summary>
		/// <param name="fileSize">The file size in bytes.</param>
		/// <returns>The file size in bytes with "bytes" appended if it's less than 1024, or the best fitting suffix.</returns>
		public static string ToFileSize(this int size)
		{
			return ToFileSize((long)size);
		}
		
		/// <summary>
		/// Saves the given Bitmap object to a file, selecting the proper ImageFormat from the filename's extension instead of defaulting to PNG.
		/// </summary>
		/// <param name="bitmap">The bitmap to save.</param>
		/// <param name="filename">The filename to save to.</param>
		/// <exception cref="System.ArgumentException">Thrown when saving to a filename with an unrecognized extension.</exception>
		public static void SaveEx(this Bitmap bitmap, string filename)
		{
			switch (Path.GetExtension(filename.ToLowerInvariant()))
			{
				case ".png": bitmap.Save(filename, ImageFormat.Png); break;
				case ".bmp": bitmap.Save(filename, ImageFormat.Bmp); break;
				case ".gif": bitmap.Save(filename, ImageFormat.Gif); break;
				case ".jpg": bitmap.Save(filename, ImageFormat.Jpeg); break;
				case ".jpeg": bitmap.Save(filename, ImageFormat.Jpeg); break;
				case ".tif": bitmap.Save(filename, ImageFormat.Tiff); break;
				case ".tiff": bitmap.Save(filename, ImageFormat.Tiff); break;
				default: throw new ArgumentException("Unsupported bitmap file format.");
			}
		}
	}

	/// <summary>
	/// Abstracts away the retrieval of an image's raw pixel and palette data, and allows reading ZSoft Paintbrush PCX files into Bitmap objects.
	/// </summary>
	public static class BitmapData
	{
		/// <summary>
		/// Extracts the raw pixel data and optionally palette from a 256-color 320 by 200 pixel Bitmap.
		/// </summary>
		/// <param name="from">The name of a bitmap file in any of the formats supported by Bitmap, or ZSoft Paintbrush PCX.</param>
		/// <param name="palette">An array of 768 bytes to recieve the color palette of the Bitmap, or null.</param>
		/// <param name="data">An array of 64000 bytes to recieve the pixel data of the Bitmap.</param>
		public static void GetPixels(string from, ref byte[] palette, ref byte[] data)
		{
			if (from.EndsWith(".pcx", StringComparison.InvariantCultureIgnoreCase))
				GetPixels(ReadPCX(from), ref palette, ref data);
			else
				GetPixels(new Bitmap(from), ref palette, ref data);
		}

		/// <summary>
		/// Extracts the raw pixel data and optionally palette from a 256-color 320 by 200 pixel Bitmap.
		/// </summary>
		/// <param name="from">A Bitmap object.</param>
		/// <param name="palette">An array of 768 bytes to recieve the color palette of the Bitmap, or null.</param>
		/// <param name="data">An array of 64000 bytes to recieve the pixel data of the Bitmap.</param>
		/// <exception cref="System.Exception">Thrown whenever the programmer fucked up.</exception>
		/// <exception cref="System.FormatException">Thrown whenever the Bitmap is not the right size or color depth.</exception>
		/// <exception cref="System.ArgumentNullException">Thrown when a null data array is given.</exception>
		public static void GetPixels(Bitmap from, ref byte[] palette, ref byte[] data)
		{
			if (data == null)
				throw new ArgumentNullException("No images selected."); //"Need a target array.");
			if (data.Length != 320 * 200)
                throw new Exception(from + "Target array isn't big enough.");
			if (palette != null && palette.Length != 256 * 3)
                throw new Exception("Palette array isn't big enough.");
			var victim = from;
			if (victim.PixelFormat != PixelFormat.Format8bppIndexed)
                throw new FormatException("Images can only be 8-bit (256) color.");
			if (victim.Width != 320 || victim.Height != 200)
                throw new FormatException("Input images can only be 320 by 200 pixels in size.");

			if (palette != null)
			{
				for (var i = 0; i < victim.Palette.Entries.Length; i++)
				{
					var color = victim.Palette.Entries[i];
					palette[i * 3 + 0] = color.R;
					palette[i * 3 + 1] = color.G;
					palette[i * 3 + 2] = color.B;
				}
			}

			var bitmapData = victim.LockBits(new Rectangle(0, 0, victim.Width, victim.Height), ImageLockMode.ReadOnly, victim.PixelFormat);
			Marshal.Copy(bitmapData.Scan0, data, 0, 320 * 200);
			victim.UnlockBits(bitmapData);
		}

		/// <summary>
		/// Reads a ZSoft Paintbrush PCX file into a Bitmap. Only supports 256-color version 5 files with a single plane.
		/// </summary>
		/// <param name="from">The name of a PCX file.</param>
		/// <returns>A Bitmap object representation of the PCX file.</returns>
		/// <exception cref="System.FormatException">Thrown when the PCX file is not of a supported format, broken, or not a PCX file at all.</exception>
		public static Bitmap ReadPCX(string from)
		{
			using (var stream = new BinaryReader(File.Open(from, FileMode.Open)))
			{
				if (stream.ReadByte() != 10)
					throw new FormatException("Filename says PCX, but first byte doesn't.");
				if (stream.ReadByte() != 5)
					throw new FormatException("Wrong PCX version.");
				if (stream.ReadByte() != 1)
					throw new FormatException("Wrong PCX encoding.");
				if (stream.ReadByte() != 8)
					throw new FormatException("Input images can only be in 256 color format.");
				var winLeft = stream.ReadInt16();
				var winTop = stream.ReadInt16();
				var winRight = stream.ReadInt16();
				var winBottom = stream.ReadInt16();
				var width = winRight + 1;
				var height = winBottom + 1;
				var size = width * height;
				if (width % 8 != 0)
					throw new FormatException("Can't work with widths that aren't divisible by 8.");
				stream.ReadInt16(); //HRes
				stream.ReadInt16(); //VRes
				stream.ReadBytes(48); //ColorMap
				stream.ReadByte(); //Reserved
				if (stream.ReadByte() != 1)
					throw new FormatException("Input images can only have the one plane.");
				var stride = stream.ReadInt16();
				if (stream.ReadByte() != 1)
					throw new FormatException("Unexpected palette interpretation.");

				var bitmap = new Bitmap(width, height, PixelFormat.Format8bppIndexed);
				var palette = bitmap.Palette;
				stream.BaseStream.Seek(-769, SeekOrigin.End);
				if (stream.ReadByte() != 12)
					throw new FormatException("Unexpected byte as palette marker.");
				for (var i = 0; i < 256; i++)
					palette.Entries[i] = Color.FromArgb(255, stream.ReadByte(), stream.ReadByte(), stream.ReadByte());
				bitmap.Palette = palette;

				stream.BaseStream.Seek(128, SeekOrigin.Begin);
				var data = new byte[size];
				for (var i = 0; i < size; i++)
				{
					var b = stream.ReadByte();
					if ((b & 0xC0) == 0xC0)
					{
						var length = b & 0x3F;
						b = stream.ReadByte();
						for (var j = 0; j < length; j++)
							data[i + j] = b;
						i += length - 1;
					}
					else
						data[i] = b;
				}

				var bitmapData = bitmap.LockBits(new Rectangle(0, 0, width, height), ImageLockMode.ReadWrite, PixelFormat.Format8bppIndexed);
				Marshal.Copy(data, 0, bitmapData.Scan0, size);
				bitmap.UnlockBits(bitmapData);
				return bitmap;
			}
		}
	}
}
