﻿/// To do list:
/// 
/// 

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Threading;


namespace SEQTool
{
    /// <summary>
    /// 
    /// </summary>

    public partial class ExportFrm : Form
    {
        //public string exportPath = MainFrm.projPath;
        public string exportPath = Properties.Settings.Default.exportPath;

        MainFrm mainFrm = (MainFrm)Application.OpenForms["MainFrm"];
        ErrorLogging errorLogging = new ErrorLogging();


        public ExportFrm()
        {
            InitializeComponent();
        }


        /// <summary>
        /// Loads saved settings
        /// </summary>
        private void Form_Load(object sender, EventArgs e)
        {
            if (!Properties.Settings.Default.expFrmLocation.IsEmpty)
                this.Location = Properties.Settings.Default.expFrmLocation;
            else
                this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;

            outputPathTxtBox.Text = Properties.Settings.Default.exportPath;
            exportPath = outputPathTxtBox.Text;
            formatCmboBox.Text = Properties.Settings.Default.exportFormat;
        }


        /// <summary>
        /// Saves settings on close
        /// </summary>
        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            // Save screen metrics
            Properties.Settings.Default.expFrmLocation = this.Location;

            // Remember last path
            Properties.Settings.Default.exportPath = exportPath;
            Properties.Settings.Default.exportFormat = formatCmboBox.Text;

            Properties.Settings.Default.Save();
        }


        /// <summary>
        /// Sets output path for exported files
        /// </summary>
        private void outputPathTxtBox_TextChanged(object sender, EventArgs e)
        {
            if (Directory.Exists(exportPath))
                exportPath = outputPathTxtBox.Text;
        }


        /// <summary>
        /// Browse to select output path for exported files
        /// </summary>
        private void browseBtn_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDlg = new FolderBrowserDialog();
            folderBrowserDlg.Description = "Select Output Folder";
            folderBrowserDlg.RootFolder = Environment.SpecialFolder.Desktop;

            // Recall last path
            if (Directory.Exists(Properties.Settings.Default.exportPath))
                folderBrowserDlg.SelectedPath = Properties.Settings.Default.exportPath;
            else
                folderBrowserDlg.SelectedPath = MainFrm.projPath;

            if (folderBrowserDlg.ShowDialog() == DialogResult.Cancel) return;

            exportPath = folderBrowserDlg.SelectedPath;
            outputPathTxtBox.Text = exportPath;
            mainFrm.exportPath = exportPath;

            // Remember last path
            Properties.Settings.Default.exportPath = exportPath;
        }


        /// <summary>
        /// Exports opened SEQ to selected path and format
        /// </summary>
        private void exportBtn_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(exportPath))
            {
                MessageBox.Show("Output folder not found. Please select a folder and try again.", "Path Not Set", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            mainFrm.exportPath = exportPath;
            mainFrm.exportFormat = formatCmboBox.Text;
            mainFrm.Export();
            this.Close();
        }


        /// <summary>
        /// Closes form without exporting
        /// </summary>
        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
