﻿namespace SEQTool
{
    partial class ExportFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.newFolderNameLabel = new System.Windows.Forms.Label();
            this.outputPathTxtBox = new System.Windows.Forms.TextBox();
            this.browseBtn = new System.Windows.Forms.Button();
            this.exportBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.formatCmboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // newFolderNameLabel
            // 
            this.newFolderNameLabel.AutoSize = true;
            this.newFolderNameLabel.Location = new System.Drawing.Point(12, 9);
            this.newFolderNameLabel.Name = "newFolderNameLabel";
            this.newFolderNameLabel.Size = new System.Drawing.Size(67, 13);
            this.newFolderNameLabel.TabIndex = 1;
            this.newFolderNameLabel.Text = "Output Path:";
            // 
            // outputPathTxtBox
            // 
            this.outputPathTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.outputPathTxtBox.Location = new System.Drawing.Point(12, 28);
            this.outputPathTxtBox.Name = "outputPathTxtBox";
            this.outputPathTxtBox.Size = new System.Drawing.Size(326, 20);
            this.outputPathTxtBox.TabIndex = 3;
            this.outputPathTxtBox.TextChanged += new System.EventHandler(this.outputPathTxtBox_TextChanged);
            // 
            // browseBtn
            // 
            this.browseBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.browseBtn.Location = new System.Drawing.Point(344, 26);
            this.browseBtn.Name = "browseBtn";
            this.browseBtn.Size = new System.Drawing.Size(30, 23);
            this.browseBtn.TabIndex = 4;
            this.browseBtn.Text = "...";
            this.browseBtn.UseVisualStyleBackColor = true;
            this.browseBtn.Click += new System.EventHandler(this.browseBtn_Click);
            // 
            // exportBtn
            // 
            this.exportBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.exportBtn.Location = new System.Drawing.Point(215, 93);
            this.exportBtn.Name = "exportBtn";
            this.exportBtn.Size = new System.Drawing.Size(75, 23);
            this.exportBtn.TabIndex = 107;
            this.exportBtn.Text = "Export";
            this.exportBtn.UseVisualStyleBackColor = true;
            this.exportBtn.Click += new System.EventHandler(this.exportBtn_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelBtn.Location = new System.Drawing.Point(296, 93);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 105;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // formatCmboBox
            // 
            this.formatCmboBox.FormattingEnabled = true;
            this.formatCmboBox.Items.AddRange(new object[] {
            ".BMP",
            ".GIF",
            ".JPG",
            ".PNG",
            ".TIF"});
            this.formatCmboBox.Location = new System.Drawing.Point(12, 69);
            this.formatCmboBox.Name = "formatCmboBox";
            this.formatCmboBox.Size = new System.Drawing.Size(64, 21);
            this.formatCmboBox.TabIndex = 1011;
            this.formatCmboBox.Text = ".BMP";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 13);
            this.label1.TabIndex = 1012;
            this.label1.Text = "Output Format";
            // 
            // ExportFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 128);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.exportBtn);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.formatCmboBox);
            this.Controls.Add(this.browseBtn);
            this.Controls.Add(this.outputPathTxtBox);
            this.Controls.Add(this.newFolderNameLabel);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ExportFrm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Export SEQ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Closing);
            this.Load += new System.EventHandler(this.Form_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label newFolderNameLabel;
        private System.Windows.Forms.Button browseBtn;
        internal System.Windows.Forms.Button exportBtn;
        internal System.Windows.Forms.Button cancelBtn;
        public System.Windows.Forms.TextBox outputPathTxtBox;
        private System.Windows.Forms.ComboBox formatCmboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer;
    }
}