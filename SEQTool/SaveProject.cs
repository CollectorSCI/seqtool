﻿/// To do list:
/// 
/// 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml.Linq;


namespace SEQTool
{
    /// <summary>
    /// A class for saving projects
    /// </summary>

    class SaveProject
    {
        static string tempPath = Path.GetTempPath();

        public static string projName;
        public static string projFile;
        public static string seqName;

        public static List<string> frames = new List<string>();

        public static bool seqSubDir;
        public static bool newSave;

        ErrorLogging errorLogging = new ErrorLogging();


        /// <summary>
        /// Opens SaveFileDialog to set name/path of save file, then calls Save()
        /// </summary>
        public static void SaveAs()
        {
            //projFile = MainFrm.projPath + "\\" + Directory.GetParent(MainFrm.projPath + "\\src").Name + ".sqp";
            SaveFileDialog saveFileDlg = new SaveFileDialog();
            saveFileDlg.InitialDirectory = MainFrm.projPath;
            saveFileDlg.Title = "Save SEQ Project";
            saveFileDlg.DefaultExt = "sqp";
            saveFileDlg.AddExtension = true;
            saveFileDlg.Filter = "SQP files (*.sqp)|*.sqp|All files (*.*)|*.*";
            saveFileDlg.FileName = projFile;

            if (saveFileDlg.ShowDialog() == DialogResult.Cancel) return;

            projFile = saveFileDlg.FileName;
            newSave = true;
            Save();
        }


        /// <summary>
        /// Saves current to specified save file
        /// </summary>
        public static void Save()
        {
            // Redirect as needed;
            if (!File.Exists(projFile) && newSave == false)
                SaveAs();

            try
            {
                XmlTextWriter objXmlTextWriter = new XmlTextWriter(projFile, null);
                objXmlTextWriter.Formatting = Formatting.Indented;

                objXmlTextWriter.WriteStartDocument();
                objXmlTextWriter.WriteStartElement("root");
                objXmlTextWriter.WriteStartElement("Project");

                objXmlTextWriter.WriteStartElement("ProjName");
                objXmlTextWriter.WriteString(projName);
                objXmlTextWriter.WriteEndElement();

                objXmlTextWriter.WriteStartElement("ProjPath");
                objXmlTextWriter.WriteString(MainFrm.projPath);
                objXmlTextWriter.WriteEndElement();

                objXmlTextWriter.WriteStartElement("ProjAspect");
                objXmlTextWriter.WriteString(MainFrm.aspect.ToString());
                objXmlTextWriter.WriteEndElement();

                objXmlTextWriter.WriteStartElement("SEQName");
                objXmlTextWriter.WriteString(seqName);
                objXmlTextWriter.WriteEndElement();

                objXmlTextWriter.WriteStartElement("SEQSubDir");
                objXmlTextWriter.WriteString(seqSubDir.ToString());
                objXmlTextWriter.WriteEndElement();

                objXmlTextWriter.WriteStartElement("Frames");

                foreach (string frame in frames)
                {
                    objXmlTextWriter.WriteStartElement("Frame");
                    objXmlTextWriter.WriteString(frame);
                    objXmlTextWriter.WriteEndElement();
                }

                objXmlTextWriter.WriteEndElement();
                objXmlTextWriter.WriteEndElement();
                objXmlTextWriter.WriteEndElement();

                objXmlTextWriter.WriteEndDocument();
                objXmlTextWriter.Flush();
                objXmlTextWriter.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable create save file", "Save Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + ex.Message);
            }
        }
    }
}
