﻿namespace SEQTool
{
    partial class MainFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainFrm));
            this.volumeScrollBar = new System.Windows.Forms.HScrollBar();
            this.statusbar = new System.Windows.Forms.StatusStrip();
            this.statusbarMainMessage = new System.Windows.Forms.ToolStripStatusLabel();
            this.clearAllBtn = new System.Windows.Forms.Button();
            this.upBtn = new System.Windows.Forms.Button();
            this.dnBtn = new System.Windows.Forms.Button();
            this.frameListBox = new System.Windows.Forms.ListBox();
            this.cntxtMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addCntxtMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.removeCntxtMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteCntxtMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.clearAllCntxtMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.viewCntxtMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.copyCntxtMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.editCntxtMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.propsCntxtMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.addBtn = new System.Windows.Forms.Button();
            this.removeBtn = new System.Windows.Forms.Button();
            this.splitContnr = new System.Windows.Forms.SplitContainer();
            this.makeSEQBtn = new System.Windows.Forms.Button();
            this.listThumbPicBox = new System.Windows.Forms.PictureBox();
            this.seqNamePanel = new System.Windows.Forms.Panel();
            this.seqNameTxtBox = new System.Windows.Forms.TextBox();
            this.seqLbl = new System.Windows.Forms.Label();
            this.playerPanel = new System.Windows.Forms.Panel();
            this.seekBar = new System.Windows.Forms.ProgressBar();
            this.waitLbl = new System.Windows.Forms.Label();
            this.playerControlPnl = new System.Windows.Forms.Panel();
            this.playToolbar = new System.Windows.Forms.ToolStrip();
            this.rewindBtn = new System.Windows.Forms.ToolStripButton();
            this.stepBackBtn = new System.Windows.Forms.ToolStripButton();
            this.stopBtn = new System.Windows.Forms.ToolStripButton();
            this.pauseBtn = new System.Windows.Forms.ToolStripButton();
            this.playBtn = new System.Windows.Forms.ToolStripButton();
            this.stepNextBtn = new System.Windows.Forms.ToolStripButton();
            this.ffBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.muteBtn2 = new System.Windows.Forms.ToolStripButton();
            this.spacerBtn = new System.Windows.Forms.ToolStripButton();
            this.SEQNameLbl = new System.Windows.Forms.Label();
            this.seqPicBox = new System.Windows.Forms.PictureBox();
            this.seqPicBoxCntxtMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.loadSEQCntxtMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.stopCntxtMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.playCntxtMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.editSEQCntxtMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.seqToolbar = new System.Windows.Forms.ToolStrip();
            this.openSEQBtn = new System.Windows.Forms.ToolStripButton();
            this.saveSEQBtn = new System.Windows.Forms.ToolStripButton();
            this.saveSEQAsBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.editSEQBtn = new System.Windows.Forms.ToolStripButton();
            this.thumbPreviewPnl = new System.Windows.Forms.FlowLayoutPanel();
            this.statusTimer = new System.Windows.Forms.Timer(this.components);
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.horizSplitContainer = new System.Windows.Forms.SplitContainer();
            this.previewThumbPicBox = new System.Windows.Forms.PictureBox();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.endPlayTimer = new System.Windows.Forms.Timer(this.components);
            this.toolbar = new System.Windows.Forms.ToolStrip();
            this.newBtn = new System.Windows.Forms.ToolStripButton();
            this.openBtn = new System.Windows.Forms.ToolStripButton();
            this.saveBtn = new System.Windows.Forms.ToolStripButton();
            this.saveAsBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.importBtn = new System.Windows.Forms.ToolStripButton();
            this.exportBtn = new System.Windows.Forms.ToolStripButton();
            this.formatCmboBox = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.appendCFGBtn = new System.Windows.Forms.ToolStripButton();
            this.menubar = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.fileMenuNew = new System.Windows.Forms.ToolStripMenuItem();
            this.fileMenuOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.fileMenuOpenSEQ = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.fileMenuSave = new System.Windows.Forms.ToolStripMenuItem();
            this.fileMenuSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.fileMenuSaveSEQ = new System.Windows.Forms.ToolStripMenuItem();
            this.fileMenuSaveSEQAs = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.fileMenuImport = new System.Windows.Forms.ToolStripMenuItem();
            this.fileMenuExport = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.fileMenuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenuAspectCorrection = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenuExamMode = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsMenuUseSubDir = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsMenuAppendCFG = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsMenuOptions = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsMenuRegisterSEQ = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsMenuRegisterSQP = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenuContents = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenuIndex = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenuSearch = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenuSCICommunity = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenuSCIWiki = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.helpMenuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.importTimer = new System.Windows.Forms.Timer(this.components);
            this.statusbar.SuspendLayout();
            this.cntxtMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContnr)).BeginInit();
            this.splitContnr.Panel1.SuspendLayout();
            this.splitContnr.Panel2.SuspendLayout();
            this.splitContnr.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.listThumbPicBox)).BeginInit();
            this.seqNamePanel.SuspendLayout();
            this.playerPanel.SuspendLayout();
            this.playerControlPnl.SuspendLayout();
            this.playToolbar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.seqPicBox)).BeginInit();
            this.seqPicBoxCntxtMenu.SuspendLayout();
            this.seqToolbar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.horizSplitContainer)).BeginInit();
            this.horizSplitContainer.Panel1.SuspendLayout();
            this.horizSplitContainer.Panel2.SuspendLayout();
            this.horizSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.previewThumbPicBox)).BeginInit();
            this.toolbar.SuspendLayout();
            this.menubar.SuspendLayout();
            this.SuspendLayout();
            // 
            // volumeScrollBar
            // 
            this.volumeScrollBar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.volumeScrollBar.Location = new System.Drawing.Point(193, 3);
            this.volumeScrollBar.Maximum = 1000;
            this.volumeScrollBar.Name = "volumeScrollBar";
            this.volumeScrollBar.Size = new System.Drawing.Size(97, 19);
            this.volumeScrollBar.TabIndex = 19;
            this.toolTip.SetToolTip(this.volumeScrollBar, "Adjust volume");
            this.volumeScrollBar.Value = 350;
            this.volumeScrollBar.Scroll += new System.Windows.Forms.ScrollEventHandler(this.volumeScrollBar_Scroll);
            // 
            // statusbar
            // 
            this.statusbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusbarMainMessage});
            this.statusbar.Location = new System.Drawing.Point(0, 670);
            this.statusbar.Name = "statusbar";
            this.statusbar.Size = new System.Drawing.Size(902, 22);
            this.statusbar.TabIndex = 124;
            this.statusbar.Text = "statusStrip1";
            this.statusbar.Click += new System.EventHandler(this.statusbar_Click);
            // 
            // statusbarMainMessage
            // 
            this.statusbarMainMessage.Name = "statusbarMainMessage";
            this.statusbarMainMessage.Size = new System.Drawing.Size(39, 17);
            this.statusbarMainMessage.Text = "Ready";
            // 
            // clearAllBtn
            // 
            this.clearAllBtn.Enabled = false;
            this.clearAllBtn.Location = new System.Drawing.Point(174, 1);
            this.clearAllBtn.Name = "clearAllBtn";
            this.clearAllBtn.Size = new System.Drawing.Size(75, 23);
            this.clearAllBtn.TabIndex = 1057;
            this.clearAllBtn.Text = "ClearAll";
            this.toolTip.SetToolTip(this.clearAllBtn, "Remove all images from list");
            this.clearAllBtn.UseVisualStyleBackColor = true;
            this.clearAllBtn.Click += new System.EventHandler(this.clearAllBtn_Click);
            // 
            // upBtn
            // 
            this.upBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.upBtn.Enabled = false;
            this.upBtn.Font = new System.Drawing.Font("Marlett", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.upBtn.Location = new System.Drawing.Point(223, 403);
            this.upBtn.Name = "upBtn";
            this.upBtn.Size = new System.Drawing.Size(26, 23);
            this.upBtn.TabIndex = 1052;
            this.upBtn.Text = "t";
            this.toolTip.SetToolTip(this.upBtn, "Move selected up one");
            this.upBtn.UseVisualStyleBackColor = true;
            this.upBtn.Click += new System.EventHandler(this.upBtn_Click);
            // 
            // dnBtn
            // 
            this.dnBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dnBtn.Enabled = false;
            this.dnBtn.Font = new System.Drawing.Font("Marlett", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.dnBtn.Location = new System.Drawing.Point(223, 432);
            this.dnBtn.Name = "dnBtn";
            this.dnBtn.Size = new System.Drawing.Size(26, 23);
            this.dnBtn.TabIndex = 1054;
            this.dnBtn.Text = "u";
            this.toolTip.SetToolTip(this.dnBtn, "Move selected down one");
            this.dnBtn.UseVisualStyleBackColor = true;
            this.dnBtn.Click += new System.EventHandler(this.dnBtn_Click);
            // 
            // frameListBox
            // 
            this.frameListBox.AllowDrop = true;
            this.frameListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.frameListBox.ContextMenuStrip = this.cntxtMenu;
            this.frameListBox.FormattingEnabled = true;
            this.frameListBox.HorizontalScrollbar = true;
            this.frameListBox.IntegralHeight = false;
            this.frameListBox.Location = new System.Drawing.Point(12, 30);
            this.frameListBox.Name = "frameListBox";
            this.frameListBox.Size = new System.Drawing.Size(205, 425);
            this.frameListBox.TabIndex = 1047;
            this.toolTip.SetToolTip(this.frameListBox, "Click and drag to rearrange");
            this.frameListBox.Click += new System.EventHandler(this.frameListBox_Click);
            this.frameListBox.SelectedIndexChanged += new System.EventHandler(this.frameListBox_SelectedIndexChanged);
            this.frameListBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.frameListBox_DragDrop);
            this.frameListBox.DragEnter += new System.Windows.Forms.DragEventHandler(this.frameListBox_DragEnter);
            this.frameListBox.DragOver += new System.Windows.Forms.DragEventHandler(this.frameListBox_DragOver);
            this.frameListBox.DragLeave += new System.EventHandler(this.frameListBox_DragLeave);
            this.frameListBox.DoubleClick += new System.EventHandler(this.frameListBox_DoubleClick);
            this.frameListBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frameListBox_KeyDown);
            this.frameListBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frameListBox_MouseDown);
            this.frameListBox.MouseHover += new System.EventHandler(this.frameListBox_MouseHover);
            this.frameListBox.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frameListBox_MouseMove);
            this.frameListBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.frameListBox_MouseUp);
            // 
            // cntxtMenu
            // 
            this.cntxtMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addCntxtMenu,
            this.removeCntxtMenu,
            this.deleteCntxtMenu,
            this.clearAllCntxtMenu,
            this.toolStripSeparator8,
            this.viewCntxtMenu,
            this.copyCntxtMenu,
            this.editCntxtMenu,
            this.propsCntxtMenu});
            this.cntxtMenu.Name = "ContextMenuStrip1";
            this.cntxtMenu.Size = new System.Drawing.Size(136, 186);
            this.cntxtMenu.Text = "Context Menu";
            this.cntxtMenu.Opening += new System.ComponentModel.CancelEventHandler(this.cntxtMenu_Opening);
            // 
            // addCntxtMenu
            // 
            this.addCntxtMenu.Image = global::SEQTool.Properties.Resources.NewImage;
            this.addCntxtMenu.Name = "addCntxtMenu";
            this.addCntxtMenu.Size = new System.Drawing.Size(135, 22);
            this.addCntxtMenu.Text = "Add Image";
            this.addCntxtMenu.Click += new System.EventHandler(this.addCntxtMenu_Click);
            // 
            // removeCntxtMenu
            // 
            this.removeCntxtMenu.Image = global::SEQTool.Properties.Resources.Remove;
            this.removeCntxtMenu.Name = "removeCntxtMenu";
            this.removeCntxtMenu.Size = new System.Drawing.Size(135, 22);
            this.removeCntxtMenu.Text = "Remove";
            this.removeCntxtMenu.Click += new System.EventHandler(this.removeCntxtMenu_Click);
            // 
            // deleteCntxtMenu
            // 
            this.deleteCntxtMenu.Image = global::SEQTool.Properties.Resources.Recycle;
            this.deleteCntxtMenu.Name = "deleteCntxtMenu";
            this.deleteCntxtMenu.Size = new System.Drawing.Size(135, 22);
            this.deleteCntxtMenu.Text = "Delete File";
            this.deleteCntxtMenu.Click += new System.EventHandler(this.deleteCntxtMenu_Click);
            // 
            // clearAllCntxtMenu
            // 
            this.clearAllCntxtMenu.Image = global::SEQTool.Properties.Resources.ClearAll;
            this.clearAllCntxtMenu.Name = "clearAllCntxtMenu";
            this.clearAllCntxtMenu.Size = new System.Drawing.Size(135, 22);
            this.clearAllCntxtMenu.Text = "Clear All";
            this.clearAllCntxtMenu.Click += new System.EventHandler(this.clearAllCntxtMenu_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(132, 6);
            // 
            // viewCntxtMenu
            // 
            this.viewCntxtMenu.Image = global::SEQTool.Properties.Resources.Image;
            this.viewCntxtMenu.Name = "viewCntxtMenu";
            this.viewCntxtMenu.Size = new System.Drawing.Size(135, 22);
            this.viewCntxtMenu.Text = "View Image";
            this.viewCntxtMenu.Click += new System.EventHandler(this.viewCntxtMenu_Click);
            // 
            // copyCntxtMenu
            // 
            this.copyCntxtMenu.Image = global::SEQTool.Properties.Resources.Copy;
            this.copyCntxtMenu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.copyCntxtMenu.Name = "copyCntxtMenu";
            this.copyCntxtMenu.Size = new System.Drawing.Size(135, 22);
            this.copyCntxtMenu.Text = "Copy";
            this.copyCntxtMenu.Click += new System.EventHandler(this.copyCntxtMenu_Click);
            // 
            // editCntxtMenu
            // 
            this.editCntxtMenu.Image = global::SEQTool.Properties.Resources.Edit;
            this.editCntxtMenu.Name = "editCntxtMenu";
            this.editCntxtMenu.Size = new System.Drawing.Size(135, 22);
            this.editCntxtMenu.Text = "Edit";
            this.editCntxtMenu.Click += new System.EventHandler(this.editCntxtMenu_Click);
            // 
            // propsCntxtMenu
            // 
            this.propsCntxtMenu.Image = global::SEQTool.Properties.Resources.Properties;
            this.propsCntxtMenu.Name = "propsCntxtMenu";
            this.propsCntxtMenu.Size = new System.Drawing.Size(135, 22);
            this.propsCntxtMenu.Text = "Properties";
            this.propsCntxtMenu.Click += new System.EventHandler(this.propsCntxtMenu_Click);
            // 
            // addBtn
            // 
            this.addBtn.Location = new System.Drawing.Point(12, 1);
            this.addBtn.Name = "addBtn";
            this.addBtn.Size = new System.Drawing.Size(75, 23);
            this.addBtn.TabIndex = 1050;
            this.addBtn.Text = "Add";
            this.toolTip.SetToolTip(this.addBtn, "Browse to add images to be included");
            this.addBtn.UseVisualStyleBackColor = true;
            this.addBtn.Click += new System.EventHandler(this.addBtn_Click);
            // 
            // removeBtn
            // 
            this.removeBtn.Enabled = false;
            this.removeBtn.Location = new System.Drawing.Point(93, 1);
            this.removeBtn.Name = "removeBtn";
            this.removeBtn.Size = new System.Drawing.Size(75, 23);
            this.removeBtn.TabIndex = 1049;
            this.removeBtn.Text = "Remove";
            this.toolTip.SetToolTip(this.removeBtn, "Remove selected from list");
            this.removeBtn.UseVisualStyleBackColor = true;
            this.removeBtn.Click += new System.EventHandler(this.removeBtn_Click);
            // 
            // splitContnr
            // 
            this.splitContnr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContnr.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContnr.IsSplitterFixed = true;
            this.splitContnr.Location = new System.Drawing.Point(0, 0);
            this.splitContnr.Name = "splitContnr";
            // 
            // splitContnr.Panel1
            // 
            this.splitContnr.Panel1.Controls.Add(this.makeSEQBtn);
            this.splitContnr.Panel1.Controls.Add(this.listThumbPicBox);
            this.splitContnr.Panel1.Controls.Add(this.addBtn);
            this.splitContnr.Panel1.Controls.Add(this.removeBtn);
            this.splitContnr.Panel1.Controls.Add(this.seqNamePanel);
            this.splitContnr.Panel1.Controls.Add(this.clearAllBtn);
            this.splitContnr.Panel1.Controls.Add(this.frameListBox);
            this.splitContnr.Panel1.Controls.Add(this.upBtn);
            this.splitContnr.Panel1.Controls.Add(this.dnBtn);
            this.splitContnr.Panel1.Click += new System.EventHandler(this.splitContnr_Panel1_Click);
            // 
            // splitContnr.Panel2
            // 
            this.splitContnr.Panel2.Controls.Add(this.playerPanel);
            this.splitContnr.Panel2.Controls.Add(this.seqToolbar);
            this.splitContnr.Size = new System.Drawing.Size(902, 485);
            this.splitContnr.SplitterDistance = 248;
            this.splitContnr.TabIndex = 1059;
            // 
            // makeSEQBtn
            // 
            this.makeSEQBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.makeSEQBtn.Enabled = false;
            this.makeSEQBtn.Location = new System.Drawing.Point(174, 461);
            this.makeSEQBtn.Name = "makeSEQBtn";
            this.makeSEQBtn.Size = new System.Drawing.Size(75, 23);
            this.makeSEQBtn.TabIndex = 1061;
            this.makeSEQBtn.Text = "Make SEQ";
            this.toolTip.SetToolTip(this.makeSEQBtn, "Make SEQ from included images");
            this.makeSEQBtn.UseVisualStyleBackColor = true;
            this.makeSEQBtn.Click += new System.EventHandler(this.makeSEQBtn_Click);
            // 
            // listThumbPicBox
            // 
            this.listThumbPicBox.Location = new System.Drawing.Point(12, 30);
            this.listThumbPicBox.Name = "listThumbPicBox";
            this.listThumbPicBox.Size = new System.Drawing.Size(100, 50);
            this.listThumbPicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.listThumbPicBox.TabIndex = 1062;
            this.listThumbPicBox.TabStop = false;
            this.listThumbPicBox.Visible = false;
            // 
            // seqNamePanel
            // 
            this.seqNamePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.seqNamePanel.BackColor = System.Drawing.SystemColors.Window;
            this.seqNamePanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.seqNamePanel.Controls.Add(this.seqNameTxtBox);
            this.seqNamePanel.Controls.Add(this.seqLbl);
            this.seqNamePanel.Location = new System.Drawing.Point(12, 463);
            this.seqNamePanel.Name = "seqNamePanel";
            this.seqNamePanel.Size = new System.Drawing.Size(98, 21);
            this.seqNamePanel.TabIndex = 1061;
            this.seqNamePanel.Click += new System.EventHandler(this.seqNamePanel_Click);
            this.seqNamePanel.MouseHover += new System.EventHandler(this.seqNamePanel_MouseHover);
            // 
            // seqNameTxtBox
            // 
            this.seqNameTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.seqNameTxtBox.ForeColor = System.Drawing.SystemColors.GrayText;
            this.seqNameTxtBox.Location = new System.Drawing.Point(2, 2);
            this.seqNameTxtBox.Name = "seqNameTxtBox";
            this.seqNameTxtBox.Size = new System.Drawing.Size(64, 13);
            this.seqNameTxtBox.TabIndex = 1059;
            this.seqNameTxtBox.Text = "SEQ Name   ";
            this.seqNameTxtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip.SetToolTip(this.seqNameTxtBox, "Enter desired SEQ name");
            this.seqNameTxtBox.TextChanged += new System.EventHandler(this.seqNameTxtBox_TextChanged);
            this.seqNameTxtBox.Enter += new System.EventHandler(this.seqNameTxtBox_Enter);
            this.seqNameTxtBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.seqNameTxtBox_KeyDown);
            this.seqNameTxtBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.seqNameTxtBox_KeyPress);
            this.seqNameTxtBox.LostFocus += new System.EventHandler(this.seqNameTxtBox_LostFocus);
            this.seqNameTxtBox.MouseDown += new System.Windows.Forms.MouseEventHandler(this.seqNameTxtBox_MouseDown);
            // 
            // seqLbl
            // 
            this.seqLbl.AutoSize = true;
            this.seqLbl.BackColor = System.Drawing.Color.Transparent;
            this.seqLbl.Location = new System.Drawing.Point(63, 2);
            this.seqLbl.Name = "seqLbl";
            this.seqLbl.Size = new System.Drawing.Size(31, 13);
            this.seqLbl.TabIndex = 1060;
            this.seqLbl.Text = "        ";
            this.seqLbl.Click += new System.EventHandler(this.seqLbl_Click);
            this.seqLbl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.seqLbl_MouseDown);
            this.seqLbl.MouseLeave += new System.EventHandler(this.seqLbl_MouseLeave);
            this.seqLbl.MouseHover += new System.EventHandler(this.seqLbl_MouseHover);
            // 
            // playerPanel
            // 
            this.playerPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.playerPanel.Controls.Add(this.seekBar);
            this.playerPanel.Controls.Add(this.waitLbl);
            this.playerPanel.Controls.Add(this.playerControlPnl);
            this.playerPanel.Controls.Add(this.SEQNameLbl);
            this.playerPanel.Controls.Add(this.seqPicBox);
            this.playerPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.playerPanel.Location = new System.Drawing.Point(0, 25);
            this.playerPanel.MaximumSize = new System.Drawing.Size(650, 540);
            this.playerPanel.MinimumSize = new System.Drawing.Size(650, 460);
            this.playerPanel.Name = "playerPanel";
            this.playerPanel.Size = new System.Drawing.Size(650, 460);
            this.playerPanel.TabIndex = 1059;
            this.playerPanel.Click += new System.EventHandler(this.playerPanel_Click);
            // 
            // seekBar
            // 
            this.seekBar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.seekBar.Location = new System.Drawing.Point(3, 417);
            this.seekBar.Name = "seekBar";
            this.seekBar.Size = new System.Drawing.Size(640, 8);
            this.seekBar.TabIndex = 1061;
            // 
            // waitLbl
            // 
            this.waitLbl.AutoSize = true;
            this.waitLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.waitLbl.Location = new System.Drawing.Point(262, 203);
            this.waitLbl.Name = "waitLbl";
            this.waitLbl.Size = new System.Drawing.Size(123, 24);
            this.waitLbl.TabIndex = 1062;
            this.waitLbl.Text = "Please Wait...";
            this.waitLbl.Visible = false;
            // 
            // playerControlPnl
            // 
            this.playerControlPnl.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.playerControlPnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.playerControlPnl.Controls.Add(this.volumeScrollBar);
            this.playerControlPnl.Controls.Add(this.playToolbar);
            this.playerControlPnl.Location = new System.Drawing.Point(225, 429);
            this.playerControlPnl.Name = "playerControlPnl";
            this.playerControlPnl.Size = new System.Drawing.Size(197, 27);
            this.playerControlPnl.TabIndex = 125;
            // 
            // playToolbar
            // 
            this.playToolbar.Dock = System.Windows.Forms.DockStyle.None;
            this.playToolbar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.playToolbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rewindBtn,
            this.stepBackBtn,
            this.stopBtn,
            this.pauseBtn,
            this.playBtn,
            this.stepNextBtn,
            this.ffBtn,
            this.toolStripLabel1,
            this.muteBtn2,
            this.spacerBtn});
            this.playToolbar.Location = new System.Drawing.Point(0, 0);
            this.playToolbar.Name = "playToolbar";
            this.playToolbar.Size = new System.Drawing.Size(357, 27);
            this.playToolbar.TabIndex = 21;
            this.playToolbar.Text = "toolStrip1";
            this.playToolbar.Click += new System.EventHandler(this.playToolbar_Click);
            // 
            // rewindBtn
            // 
            this.rewindBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.rewindBtn.Enabled = false;
            this.rewindBtn.Font = new System.Drawing.Font("Webdings", 11.25F);
            this.rewindBtn.ForeColor = System.Drawing.Color.ForestGreen;
            this.rewindBtn.Image = ((System.Drawing.Image)(resources.GetObject("rewindBtn.Image")));
            this.rewindBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.rewindBtn.Name = "rewindBtn";
            this.rewindBtn.Size = new System.Drawing.Size(28, 24);
            this.rewindBtn.Text = "7";
            this.rewindBtn.Click += new System.EventHandler(this.rewindBtn_Click);
            // 
            // stepBackBtn
            // 
            this.stepBackBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.stepBackBtn.Enabled = false;
            this.stepBackBtn.Font = new System.Drawing.Font("Webdings", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.stepBackBtn.ForeColor = System.Drawing.Color.ForestGreen;
            this.stepBackBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.stepBackBtn.Name = "stepBackBtn";
            this.stepBackBtn.Size = new System.Drawing.Size(28, 24);
            this.stepBackBtn.Text = "9";
            this.stepBackBtn.ToolTipText = "Steo back one frame";
            this.stepBackBtn.Click += new System.EventHandler(this.stepBackBtn_Click);
            // 
            // stopBtn
            // 
            this.stopBtn.CheckOnClick = true;
            this.stopBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.stopBtn.Enabled = false;
            this.stopBtn.Font = new System.Drawing.Font("Webdings", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.stopBtn.ForeColor = System.Drawing.Color.DarkRed;
            this.stopBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.stopBtn.Name = "stopBtn";
            this.stopBtn.Size = new System.Drawing.Size(28, 24);
            this.stopBtn.Text = "<";
            this.stopBtn.ToolTipText = "Stop";
            this.stopBtn.Click += new System.EventHandler(this.stopBtn_Click);
            // 
            // pauseBtn
            // 
            this.pauseBtn.CheckOnClick = true;
            this.pauseBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.pauseBtn.Enabled = false;
            this.pauseBtn.Font = new System.Drawing.Font("Webdings", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.pauseBtn.ForeColor = System.Drawing.Color.DarkRed;
            this.pauseBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pauseBtn.Name = "pauseBtn";
            this.pauseBtn.Size = new System.Drawing.Size(28, 24);
            this.pauseBtn.Text = ";";
            this.pauseBtn.ToolTipText = "Pause";
            this.pauseBtn.Click += new System.EventHandler(this.pauseBtn_Click);
            // 
            // playBtn
            // 
            this.playBtn.CheckOnClick = true;
            this.playBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.playBtn.Enabled = false;
            this.playBtn.Font = new System.Drawing.Font("Webdings", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.playBtn.ForeColor = System.Drawing.Color.ForestGreen;
            this.playBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.playBtn.Name = "playBtn";
            this.playBtn.Size = new System.Drawing.Size(28, 24);
            this.playBtn.Text = "4";
            this.playBtn.ToolTipText = "Play";
            this.playBtn.Click += new System.EventHandler(this.playBtn_Click);
            // 
            // stepNextBtn
            // 
            this.stepNextBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.stepNextBtn.Enabled = false;
            this.stepNextBtn.Font = new System.Drawing.Font("Webdings", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.stepNextBtn.ForeColor = System.Drawing.Color.ForestGreen;
            this.stepNextBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.stepNextBtn.Name = "stepNextBtn";
            this.stepNextBtn.Size = new System.Drawing.Size(28, 24);
            this.stepNextBtn.Text = ":";
            this.stepNextBtn.ToolTipText = "Step forward one frame";
            this.stepNextBtn.Click += new System.EventHandler(this.stepNextBtn_Click);
            // 
            // ffBtn
            // 
            this.ffBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.ffBtn.Enabled = false;
            this.ffBtn.Font = new System.Drawing.Font("Webdings", 11.25F);
            this.ffBtn.ForeColor = System.Drawing.Color.ForestGreen;
            this.ffBtn.Image = ((System.Drawing.Image)(resources.GetObject("ffBtn.Image")));
            this.ffBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ffBtn.Name = "ffBtn";
            this.ffBtn.Size = new System.Drawing.Size(28, 24);
            this.ffBtn.Text = "8";
            this.ffBtn.Click += new System.EventHandler(this.ffBtn_Click);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(106, 24);
            this.toolStripLabel1.Text = "                                 ";
            // 
            // muteBtn2
            // 
            this.muteBtn2.CheckOnClick = true;
            this.muteBtn2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.muteBtn2.Font = new System.Drawing.Font("Webdings", 11.25F);
            this.muteBtn2.Image = global::SEQTool.Properties.Resources.MuteOff;
            this.muteBtn2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.muteBtn2.Name = "muteBtn2";
            this.muteBtn2.Size = new System.Drawing.Size(23, 24);
            this.muteBtn2.Text = "Mute ";
            this.muteBtn2.ToolTipText = "Mute audio";
            this.muteBtn2.Click += new System.EventHandler(this.muteBtn_Click);
            // 
            // spacerBtn
            // 
            this.spacerBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.spacerBtn.Font = new System.Drawing.Font("Webdings", 11.25F);
            this.spacerBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.spacerBtn.Name = "spacerBtn";
            this.spacerBtn.Size = new System.Drawing.Size(29, 24);
            this.spacerBtn.Text = "  ";
            // 
            // SEQNameLbl
            // 
            this.SEQNameLbl.AutoSize = true;
            this.SEQNameLbl.Dock = System.Windows.Forms.DockStyle.Top;
            this.SEQNameLbl.Location = new System.Drawing.Point(0, 0);
            this.SEQNameLbl.MinimumSize = new System.Drawing.Size(640, 13);
            this.SEQNameLbl.Name = "SEQNameLbl";
            this.SEQNameLbl.Size = new System.Drawing.Size(640, 13);
            this.SEQNameLbl.TabIndex = 124;
            this.SEQNameLbl.Text = "SEQ Name";
            this.SEQNameLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.SEQNameLbl.Click += new System.EventHandler(this.SEQNameLbl_Click);
            // 
            // seqPicBox
            // 
            this.seqPicBox.AllowDrop = true;
            this.seqPicBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.seqPicBox.BackColor = System.Drawing.SystemColors.ControlDark;
            this.seqPicBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.seqPicBox.ContextMenuStrip = this.seqPicBoxCntxtMenu;
            this.seqPicBox.Location = new System.Drawing.Point(3, 16);
            this.seqPicBox.MaximumSize = new System.Drawing.Size(640, 480);
            this.seqPicBox.MinimumSize = new System.Drawing.Size(640, 400);
            this.seqPicBox.Name = "seqPicBox";
            this.seqPicBox.Size = new System.Drawing.Size(640, 400);
            this.seqPicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.seqPicBox.TabIndex = 0;
            this.seqPicBox.TabStop = false;
            this.seqPicBox.Click += new System.EventHandler(this.seqPicBox_Click);
            this.seqPicBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.seqPicBox_DragDrop);
            this.seqPicBox.DragEnter += new System.Windows.Forms.DragEventHandler(this.seqPicBox_DragEnter);
            // 
            // seqPicBoxCntxtMenu
            // 
            this.seqPicBoxCntxtMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadSEQCntxtMenu,
            this.stopCntxtMenu,
            this.playCntxtMenu,
            this.editSEQCntxtMenu});
            this.seqPicBoxCntxtMenu.Name = "seqPicBoxCntxtMenu";
            this.seqPicBoxCntxtMenu.Size = new System.Drawing.Size(125, 92);
            // 
            // loadSEQCntxtMenu
            // 
            this.loadSEQCntxtMenu.Name = "loadSEQCntxtMenu";
            this.loadSEQCntxtMenu.Size = new System.Drawing.Size(124, 22);
            this.loadSEQCntxtMenu.Text = "Load SEQ";
            this.loadSEQCntxtMenu.Click += new System.EventHandler(this.loadSEQCntxtMenu_Click);
            // 
            // stopCntxtMenu
            // 
            this.stopCntxtMenu.Enabled = false;
            this.stopCntxtMenu.Name = "stopCntxtMenu";
            this.stopCntxtMenu.Size = new System.Drawing.Size(124, 22);
            this.stopCntxtMenu.Text = "Stop";
            this.stopCntxtMenu.Click += new System.EventHandler(this.stopCntxtMenu_Click);
            // 
            // playCntxtMenu
            // 
            this.playCntxtMenu.Enabled = false;
            this.playCntxtMenu.Name = "playCntxtMenu";
            this.playCntxtMenu.Size = new System.Drawing.Size(124, 22);
            this.playCntxtMenu.Text = "Play";
            this.playCntxtMenu.Click += new System.EventHandler(this.playCntxtMenu_Click);
            // 
            // editSEQCntxtMenu
            // 
            this.editSEQCntxtMenu.Enabled = false;
            this.editSEQCntxtMenu.Name = "editSEQCntxtMenu";
            this.editSEQCntxtMenu.Size = new System.Drawing.Size(124, 22);
            this.editSEQCntxtMenu.Text = "Edit SEQ";
            this.editSEQCntxtMenu.Click += new System.EventHandler(this.editSEQCntxtMenu_Click);
            // 
            // seqToolbar
            // 
            this.seqToolbar.BackgroundImage = global::SEQTool.Properties.Resources.TB_BG2;
            this.seqToolbar.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.seqToolbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openSEQBtn,
            this.saveSEQBtn,
            this.saveSEQAsBtn,
            this.toolStripSeparator4,
            this.editSEQBtn});
            this.seqToolbar.Location = new System.Drawing.Point(0, 0);
            this.seqToolbar.Name = "seqToolbar";
            this.seqToolbar.Size = new System.Drawing.Size(650, 25);
            this.seqToolbar.TabIndex = 124;
            this.seqToolbar.Text = "toolStrip1";
            this.seqToolbar.Click += new System.EventHandler(this.seqToolbar_Click);
            // 
            // openSEQBtn
            // 
            this.openSEQBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openSEQBtn.Image = global::SEQTool.Properties.Resources.OpenSEQ;
            this.openSEQBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openSEQBtn.Name = "openSEQBtn";
            this.openSEQBtn.Size = new System.Drawing.Size(23, 22);
            this.openSEQBtn.Text = "&Open";
            this.openSEQBtn.ToolTipText = "Browse to open SEQ file in player";
            this.openSEQBtn.Click += new System.EventHandler(this.openSEQBtn_Click);
            // 
            // saveSEQBtn
            // 
            this.saveSEQBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveSEQBtn.Enabled = false;
            this.saveSEQBtn.Image = ((System.Drawing.Image)(resources.GetObject("saveSEQBtn.Image")));
            this.saveSEQBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveSEQBtn.Name = "saveSEQBtn";
            this.saveSEQBtn.Size = new System.Drawing.Size(23, 22);
            this.saveSEQBtn.Text = "&Save";
            this.saveSEQBtn.Click += new System.EventHandler(this.saveSEQBtn_Click);
            // 
            // saveSEQAsBtn
            // 
            this.saveSEQAsBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveSEQAsBtn.Enabled = false;
            this.saveSEQAsBtn.Image = global::SEQTool.Properties.Resources.SaveAs;
            this.saveSEQAsBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveSEQAsBtn.Name = "saveSEQAsBtn";
            this.saveSEQAsBtn.Size = new System.Drawing.Size(23, 22);
            this.saveSEQAsBtn.Text = "Save As...";
            this.saveSEQAsBtn.ToolTipText = "Save current SEQ file to a new name";
            this.saveSEQAsBtn.Click += new System.EventHandler(this.saveSEQAsBtn_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // editSEQBtn
            // 
            this.editSEQBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.editSEQBtn.Enabled = false;
            this.editSEQBtn.Image = global::SEQTool.Properties.Resources.EditSEQ;
            this.editSEQBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editSEQBtn.Name = "editSEQBtn";
            this.editSEQBtn.Size = new System.Drawing.Size(23, 22);
            this.editSEQBtn.Text = "Edit SEQ";
            this.editSEQBtn.Click += new System.EventHandler(this.editSEQBtn_Click);
            // 
            // thumbPreviewPnl
            // 
            this.thumbPreviewPnl.AllowDrop = true;
            this.thumbPreviewPnl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.thumbPreviewPnl.AutoScroll = true;
            this.thumbPreviewPnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.thumbPreviewPnl.ContextMenuStrip = this.cntxtMenu;
            this.thumbPreviewPnl.Location = new System.Drawing.Point(3, 3);
            this.thumbPreviewPnl.MaximumSize = new System.Drawing.Size(5000, 125);
            this.thumbPreviewPnl.MinimumSize = new System.Drawing.Size(896, 126);
            this.thumbPreviewPnl.Name = "thumbPreviewPnl";
            this.thumbPreviewPnl.Size = new System.Drawing.Size(896, 126);
            this.thumbPreviewPnl.TabIndex = 1;
            this.thumbPreviewPnl.WrapContents = false;
            this.thumbPreviewPnl.Click += new System.EventHandler(this.thumbPreviewPnl_Click);
            this.thumbPreviewPnl.DragDrop += new System.Windows.Forms.DragEventHandler(this.thumbPreviewPnl_DragDrop);
            this.thumbPreviewPnl.DragEnter += new System.Windows.Forms.DragEventHandler(this.thumbPreviewPnl_DragEnter);
            this.thumbPreviewPnl.DragOver += new System.Windows.Forms.DragEventHandler(this.thumbPreviewPnl_DragOver);
            this.thumbPreviewPnl.DragLeave += new System.EventHandler(this.thumbPreviewPnl_DragLeave);
            this.thumbPreviewPnl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.thumbPreviewPnl_MouseDown);
            this.thumbPreviewPnl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.thumbPreviewPnl_MouseMove);
            // 
            // statusTimer
            // 
            this.statusTimer.Interval = 2000;
            this.statusTimer.Tick += new System.EventHandler(this.statusTimer_Tick);
            // 
            // horizSplitContainer
            // 
            this.horizSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.horizSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.horizSplitContainer.IsSplitterFixed = true;
            this.horizSplitContainer.Location = new System.Drawing.Point(0, 49);
            this.horizSplitContainer.Name = "horizSplitContainer";
            this.horizSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // horizSplitContainer.Panel1
            // 
            this.horizSplitContainer.Panel1.Controls.Add(this.splitContnr);
            // 
            // horizSplitContainer.Panel2
            // 
            this.horizSplitContainer.Panel2.Controls.Add(this.previewThumbPicBox);
            this.horizSplitContainer.Panel2.Controls.Add(this.thumbPreviewPnl);
            this.horizSplitContainer.Size = new System.Drawing.Size(902, 621);
            this.horizSplitContainer.SplitterDistance = 485;
            this.horizSplitContainer.TabIndex = 1060;
            this.horizSplitContainer.Click += new System.EventHandler(this.horizSplitContainer_Click);
            // 
            // previewThumbPicBox
            // 
            this.previewThumbPicBox.Location = new System.Drawing.Point(0, 0);
            this.previewThumbPicBox.Name = "previewThumbPicBox";
            this.previewThumbPicBox.Size = new System.Drawing.Size(100, 50);
            this.previewThumbPicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.previewThumbPicBox.TabIndex = 1063;
            this.previewThumbPicBox.TabStop = false;
            this.previewThumbPicBox.Visible = false;
            // 
            // endPlayTimer
            // 
            this.endPlayTimer.Interval = 1000;
            this.endPlayTimer.Tick += new System.EventHandler(this.endPlayTimer_Tick);
            // 
            // toolbar
            // 
            this.toolbar.BackgroundImage = global::SEQTool.Properties.Resources.TB_BG;
            this.toolbar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.toolbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newBtn,
            this.openBtn,
            this.saveBtn,
            this.saveAsBtn,
            this.toolStripSeparator2,
            this.importBtn,
            this.exportBtn,
            this.formatCmboBox,
            this.toolStripSeparator6,
            this.appendCFGBtn});
            this.toolbar.Location = new System.Drawing.Point(0, 24);
            this.toolbar.Name = "toolbar";
            this.toolbar.Size = new System.Drawing.Size(902, 25);
            this.toolbar.TabIndex = 4;
            this.toolbar.Text = "toolStrip1";
            this.toolbar.Click += new System.EventHandler(this.toolbar_Click);
            // 
            // newBtn
            // 
            this.newBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newBtn.Image = ((System.Drawing.Image)(resources.GetObject("newBtn.Image")));
            this.newBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newBtn.Name = "newBtn";
            this.newBtn.Size = new System.Drawing.Size(23, 22);
            this.newBtn.Text = "&New";
            this.newBtn.ToolTipText = "New project";
            this.newBtn.Click += new System.EventHandler(this.newBtn_Click);
            // 
            // openBtn
            // 
            this.openBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openBtn.Image = ((System.Drawing.Image)(resources.GetObject("openBtn.Image")));
            this.openBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openBtn.Name = "openBtn";
            this.openBtn.Size = new System.Drawing.Size(23, 22);
            this.openBtn.Text = "&Open";
            this.openBtn.ToolTipText = "Open project";
            this.openBtn.Click += new System.EventHandler(this.openBtn_Click);
            // 
            // saveBtn
            // 
            this.saveBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveBtn.Enabled = false;
            this.saveBtn.Image = ((System.Drawing.Image)(resources.GetObject("saveBtn.Image")));
            this.saveBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(23, 22);
            this.saveBtn.Text = "&Save";
            this.saveBtn.ToolTipText = "Save project";
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // saveAsBtn
            // 
            this.saveAsBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveAsBtn.Enabled = false;
            this.saveAsBtn.Image = global::SEQTool.Properties.Resources.SaveAs;
            this.saveAsBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveAsBtn.Name = "saveAsBtn";
            this.saveAsBtn.Size = new System.Drawing.Size(23, 22);
            this.saveAsBtn.Text = "Save As...";
            this.saveAsBtn.ToolTipText = "Save current project to a new name";
            this.saveAsBtn.Click += new System.EventHandler(this.saveAsBtn_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // importBtn
            // 
            this.importBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.importBtn.Image = global::SEQTool.Properties.Resources.Import;
            this.importBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.importBtn.Name = "importBtn";
            this.importBtn.Size = new System.Drawing.Size(23, 22);
            this.importBtn.Text = "Import AniGIF";
            this.importBtn.ToolTipText = "Import Animated GIF";
            this.importBtn.Click += new System.EventHandler(this.importBtn_Click);
            // 
            // exportBtn
            // 
            this.exportBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.exportBtn.Enabled = false;
            this.exportBtn.Image = global::SEQTool.Properties.Resources.Export;
            this.exportBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.exportBtn.Name = "exportBtn";
            this.exportBtn.Size = new System.Drawing.Size(23, 22);
            this.exportBtn.Text = "Export SEQ";
            this.exportBtn.ToolTipText = "Export SEQ to Animated GIF";
            this.exportBtn.Click += new System.EventHandler(this.exportBtn_Click);
            // 
            // formatCmboBox
            // 
            this.formatCmboBox.AutoSize = false;
            this.formatCmboBox.DropDownWidth = 40;
            this.formatCmboBox.Items.AddRange(new object[] {
            ".BMP",
            ".GIF",
            ".JPG",
            ".PNG",
            ".TIF"});
            this.formatCmboBox.Name = "formatCmboBox";
            this.formatCmboBox.Size = new System.Drawing.Size(50, 23);
            this.formatCmboBox.Text = ".BMP";
            this.formatCmboBox.ToolTipText = "Sets export file format";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // appendCFGBtn
            // 
            this.appendCFGBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.appendCFGBtn.Enabled = false;
            this.appendCFGBtn.Image = global::SEQTool.Properties.Resources.PageEdit;
            this.appendCFGBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.appendCFGBtn.Name = "appendCFGBtn";
            this.appendCFGBtn.Size = new System.Drawing.Size(23, 22);
            this.appendCFGBtn.Text = "Append RESOURCE.CFG";
            this.appendCFGBtn.ToolTipText = "Add movieDir line to RESOURCE.CFG";
            this.appendCFGBtn.Click += new System.EventHandler(this.appendCFGBtn_Click);
            // 
            // menubar
            // 
            this.menubar.BackgroundImage = global::SEQTool.Properties.Resources.MenuBG;
            this.menubar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.viewMenu,
            this.toolsMenu,
            this.helpMenu});
            this.menubar.Location = new System.Drawing.Point(0, 0);
            this.menubar.Name = "menubar";
            this.menubar.Size = new System.Drawing.Size(902, 24);
            this.menubar.TabIndex = 3;
            this.menubar.Text = "menuStrip1";
            this.menubar.Click += new System.EventHandler(this.menubar_Click);
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenuNew,
            this.fileMenuOpen,
            this.fileMenuOpenSEQ,
            this.toolStripSeparator,
            this.fileMenuSave,
            this.fileMenuSaveAs,
            this.toolStripSeparator3,
            this.fileMenuSaveSEQ,
            this.fileMenuSaveSEQAs,
            this.toolStripSeparator10,
            this.fileMenuImport,
            this.fileMenuExport,
            this.toolStripSeparator1,
            this.fileMenuExit});
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(37, 20);
            this.fileMenu.Text = "&File";
            // 
            // fileMenuNew
            // 
            this.fileMenuNew.Image = ((System.Drawing.Image)(resources.GetObject("fileMenuNew.Image")));
            this.fileMenuNew.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.fileMenuNew.Name = "fileMenuNew";
            this.fileMenuNew.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.fileMenuNew.Size = new System.Drawing.Size(146, 22);
            this.fileMenuNew.Text = "&New";
            this.fileMenuNew.Click += new System.EventHandler(this.fileMenuNew_Click);
            // 
            // fileMenuOpen
            // 
            this.fileMenuOpen.Image = ((System.Drawing.Image)(resources.GetObject("fileMenuOpen.Image")));
            this.fileMenuOpen.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.fileMenuOpen.Name = "fileMenuOpen";
            this.fileMenuOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.fileMenuOpen.Size = new System.Drawing.Size(146, 22);
            this.fileMenuOpen.Text = "&Open";
            this.fileMenuOpen.Click += new System.EventHandler(this.fileMenuOpen_Click);
            // 
            // fileMenuOpenSEQ
            // 
            this.fileMenuOpenSEQ.Image = global::SEQTool.Properties.Resources.OpenSEQ;
            this.fileMenuOpenSEQ.Name = "fileMenuOpenSEQ";
            this.fileMenuOpenSEQ.Size = new System.Drawing.Size(146, 22);
            this.fileMenuOpenSEQ.Text = "Open SEQ";
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(143, 6);
            // 
            // fileMenuSave
            // 
            this.fileMenuSave.Enabled = false;
            this.fileMenuSave.Image = ((System.Drawing.Image)(resources.GetObject("fileMenuSave.Image")));
            this.fileMenuSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.fileMenuSave.Name = "fileMenuSave";
            this.fileMenuSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.fileMenuSave.Size = new System.Drawing.Size(146, 22);
            this.fileMenuSave.Text = "&Save";
            this.fileMenuSave.Click += new System.EventHandler(this.fileMenuSave_Click);
            // 
            // fileMenuSaveAs
            // 
            this.fileMenuSaveAs.Enabled = false;
            this.fileMenuSaveAs.Image = global::SEQTool.Properties.Resources.SaveAs;
            this.fileMenuSaveAs.Name = "fileMenuSaveAs";
            this.fileMenuSaveAs.Size = new System.Drawing.Size(146, 22);
            this.fileMenuSaveAs.Text = "Save &As";
            this.fileMenuSaveAs.Click += new System.EventHandler(this.fileMenuSaveAs_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(143, 6);
            // 
            // fileMenuSaveSEQ
            // 
            this.fileMenuSaveSEQ.Enabled = false;
            this.fileMenuSaveSEQ.Image = ((System.Drawing.Image)(resources.GetObject("fileMenuSaveSEQ.Image")));
            this.fileMenuSaveSEQ.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.fileMenuSaveSEQ.Name = "fileMenuSaveSEQ";
            this.fileMenuSaveSEQ.Size = new System.Drawing.Size(146, 22);
            this.fileMenuSaveSEQ.Text = "&Save SEQ";
            this.fileMenuSaveSEQ.Click += new System.EventHandler(this.fileMenuSaveSEQ_Click);
            // 
            // fileMenuSaveSEQAs
            // 
            this.fileMenuSaveSEQAs.Enabled = false;
            this.fileMenuSaveSEQAs.Image = global::SEQTool.Properties.Resources.SaveAs;
            this.fileMenuSaveSEQAs.Name = "fileMenuSaveSEQAs";
            this.fileMenuSaveSEQAs.Size = new System.Drawing.Size(146, 22);
            this.fileMenuSaveSEQAs.Text = "Save SEQ &As";
            this.fileMenuSaveSEQAs.Click += new System.EventHandler(this.fileMenuSaveSEQAs_Click);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(143, 6);
            // 
            // fileMenuImport
            // 
            this.fileMenuImport.Image = global::SEQTool.Properties.Resources.Import;
            this.fileMenuImport.Name = "fileMenuImport";
            this.fileMenuImport.Size = new System.Drawing.Size(146, 22);
            this.fileMenuImport.Text = "&Import";
            this.fileMenuImport.Click += new System.EventHandler(this.fileMenuImport_Click);
            // 
            // fileMenuExport
            // 
            this.fileMenuExport.Enabled = false;
            this.fileMenuExport.Image = global::SEQTool.Properties.Resources.Export;
            this.fileMenuExport.Name = "fileMenuExport";
            this.fileMenuExport.Size = new System.Drawing.Size(146, 22);
            this.fileMenuExport.Text = "&Export";
            this.fileMenuExport.Click += new System.EventHandler(this.fileMenuExport_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(143, 6);
            // 
            // fileMenuExit
            // 
            this.fileMenuExit.Image = global::SEQTool.Properties.Resources.Exit;
            this.fileMenuExit.Name = "fileMenuExit";
            this.fileMenuExit.Size = new System.Drawing.Size(146, 22);
            this.fileMenuExit.Text = "E&xit";
            this.fileMenuExit.Click += new System.EventHandler(this.fileMenuExit_Click);
            // 
            // viewMenu
            // 
            this.viewMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewMenuAspectCorrection,
            this.viewMenuExamMode});
            this.viewMenu.Name = "viewMenu";
            this.viewMenu.Size = new System.Drawing.Size(44, 20);
            this.viewMenu.Text = "&View";
            // 
            // viewMenuAspectCorrection
            // 
            this.viewMenuAspectCorrection.Checked = true;
            this.viewMenuAspectCorrection.CheckOnClick = true;
            this.viewMenuAspectCorrection.CheckState = System.Windows.Forms.CheckState.Checked;
            this.viewMenuAspectCorrection.Name = "viewMenuAspectCorrection";
            this.viewMenuAspectCorrection.Size = new System.Drawing.Size(173, 22);
            this.viewMenuAspectCorrection.Text = "Aspect Correction";
            this.viewMenuAspectCorrection.CheckedChanged += new System.EventHandler(this.viewMenuAspectCorrection_CheckedChanged);
            // 
            // viewMenuExamMode
            // 
            this.viewMenuExamMode.CheckOnClick = true;
            this.viewMenuExamMode.Name = "viewMenuExamMode";
            this.viewMenuExamMode.Size = new System.Drawing.Size(173, 22);
            this.viewMenuExamMode.Text = "Examination Mode";
            this.viewMenuExamMode.CheckedChanged += new System.EventHandler(this.viewMenuExamMode_CheckedChanged);
            // 
            // toolsMenu
            // 
            this.toolsMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolsMenuUseSubDir,
            this.toolsMenuAppendCFG,
            this.toolsMenuOptions});
            this.toolsMenu.Name = "toolsMenu";
            this.toolsMenu.Size = new System.Drawing.Size(48, 20);
            this.toolsMenu.Text = "&Tools";
            // 
            // toolsMenuUseSubDir
            // 
            this.toolsMenuUseSubDir.Checked = true;
            this.toolsMenuUseSubDir.CheckOnClick = true;
            this.toolsMenuUseSubDir.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolsMenuUseSubDir.Name = "toolsMenuUseSubDir";
            this.toolsMenuUseSubDir.Size = new System.Drawing.Size(201, 22);
            this.toolsMenuUseSubDir.Text = "Use SEQ Su&bdirectory";
            this.toolsMenuUseSubDir.CheckStateChanged += new System.EventHandler(this.toolsMenuUseSubDir_CheckStateChanged);
            // 
            // toolsMenuAppendCFG
            // 
            this.toolsMenuAppendCFG.Enabled = false;
            this.toolsMenuAppendCFG.Image = global::SEQTool.Properties.Resources.PageEdit;
            this.toolsMenuAppendCFG.Name = "toolsMenuAppendCFG";
            this.toolsMenuAppendCFG.Size = new System.Drawing.Size(201, 22);
            this.toolsMenuAppendCFG.Text = "Append RESOURCE.CFG";
            this.toolsMenuAppendCFG.Click += new System.EventHandler(this.toolsMenuAppendCFG_Click);
            // 
            // toolsMenuOptions
            // 
            this.toolsMenuOptions.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolsMenuRegisterSEQ,
            this.toolsMenuRegisterSQP});
            this.toolsMenuOptions.Image = global::SEQTool.Properties.Resources.Options;
            this.toolsMenuOptions.Name = "toolsMenuOptions";
            this.toolsMenuOptions.Size = new System.Drawing.Size(201, 22);
            this.toolsMenuOptions.Text = "&Options";
            this.toolsMenuOptions.Click += new System.EventHandler(this.toolsMenuOptions_Click);
            // 
            // toolsMenuRegisterSEQ
            // 
            this.toolsMenuRegisterSEQ.Image = global::SEQTool.Properties.Resources.Register;
            this.toolsMenuRegisterSEQ.Name = "toolsMenuRegisterSEQ";
            this.toolsMenuRegisterSEQ.Size = new System.Drawing.Size(225, 22);
            this.toolsMenuRegisterSEQ.Text = "Register SEQ with SEQTool";
            this.toolsMenuRegisterSEQ.Click += new System.EventHandler(this.toolsMenuRegisterSEQ_Click);
            // 
            // toolsMenuRegisterSQP
            // 
            this.toolsMenuRegisterSQP.Image = global::SEQTool.Properties.Resources.Register;
            this.toolsMenuRegisterSQP.Name = "toolsMenuRegisterSQP";
            this.toolsMenuRegisterSQP.Size = new System.Drawing.Size(225, 22);
            this.toolsMenuRegisterSQP.Text = "Register SEQTool Project File";
            this.toolsMenuRegisterSQP.Click += new System.EventHandler(this.toolsMenuRegisterSQP_Click);
            // 
            // helpMenu
            // 
            this.helpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpMenuContents,
            this.helpMenuIndex,
            this.helpMenuSearch,
            this.helpMenuSCICommunity,
            this.helpMenuSCIWiki,
            this.toolStripSeparator5,
            this.helpMenuAbout});
            this.helpMenu.Name = "helpMenu";
            this.helpMenu.Size = new System.Drawing.Size(44, 20);
            this.helpMenu.Text = "&Help";
            // 
            // helpMenuContents
            // 
            this.helpMenuContents.Enabled = false;
            this.helpMenuContents.Image = global::SEQTool.Properties.Resources.Help;
            this.helpMenuContents.Name = "helpMenuContents";
            this.helpMenuContents.Size = new System.Drawing.Size(158, 22);
            this.helpMenuContents.Text = "&Contents";
            this.helpMenuContents.Click += new System.EventHandler(this.helpMenuContents_Click);
            // 
            // helpMenuIndex
            // 
            this.helpMenuIndex.Enabled = false;
            this.helpMenuIndex.Image = global::SEQTool.Properties.Resources.HelpIndex;
            this.helpMenuIndex.Name = "helpMenuIndex";
            this.helpMenuIndex.Size = new System.Drawing.Size(158, 22);
            this.helpMenuIndex.Text = "&Index";
            this.helpMenuIndex.Click += new System.EventHandler(this.helpMenuIndex_Click);
            // 
            // helpMenuSearch
            // 
            this.helpMenuSearch.Enabled = false;
            this.helpMenuSearch.Image = global::SEQTool.Properties.Resources.Search;
            this.helpMenuSearch.Name = "helpMenuSearch";
            this.helpMenuSearch.Size = new System.Drawing.Size(158, 22);
            this.helpMenuSearch.Text = "&Search";
            this.helpMenuSearch.Click += new System.EventHandler(this.helpMenuSearch_Click);
            // 
            // helpMenuSCICommunity
            // 
            this.helpMenuSCICommunity.Image = global::SEQTool.Properties.Resources.SCICommunity;
            this.helpMenuSCICommunity.Name = "helpMenuSCICommunity";
            this.helpMenuSCICommunity.Size = new System.Drawing.Size(158, 22);
            this.helpMenuSCICommunity.Text = "SCI Community";
            this.helpMenuSCICommunity.Click += new System.EventHandler(this.helpMenuSCICommunity_Click);
            // 
            // helpMenuSCIWiki
            // 
            this.helpMenuSCIWiki.Image = global::SEQTool.Properties.Resources.SCIWiki;
            this.helpMenuSCIWiki.Name = "helpMenuSCIWiki";
            this.helpMenuSCIWiki.Size = new System.Drawing.Size(158, 22);
            this.helpMenuSCIWiki.Text = "SCI Wiki";
            this.helpMenuSCIWiki.Click += new System.EventHandler(this.helpMenuSCIWiki_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(155, 6);
            // 
            // helpMenuAbout
            // 
            this.helpMenuAbout.Image = ((System.Drawing.Image)(resources.GetObject("helpMenuAbout.Image")));
            this.helpMenuAbout.Name = "helpMenuAbout";
            this.helpMenuAbout.Size = new System.Drawing.Size(158, 22);
            this.helpMenuAbout.Text = "&About...";
            this.helpMenuAbout.Click += new System.EventHandler(this.helpMenuAbout_Click);
            // 
            // importTimer
            // 
            this.importTimer.Interval = 10;
            this.importTimer.Tick += new System.EventHandler(this.importTimer_Tick);
            // 
            // MainFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(902, 692);
            this.Controls.Add(this.horizSplitContainer);
            this.Controls.Add(this.statusbar);
            this.Controls.Add(this.toolbar);
            this.Controls.Add(this.menubar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(918, 725);
            this.Name = "MainFrm";
            this.Text = "SEQTool ";
            this.toolTip.SetToolTip(this, "Add movieDir= line to RESOURCE.CFG");
            this.Activated += new System.EventHandler(this.Form_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Closing);
            this.Load += new System.EventHandler(this.Form_Load);
            this.Click += new System.EventHandler(this.MainFrm_Click);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MainFrm_MouseClick);
            this.statusbar.ResumeLayout(false);
            this.statusbar.PerformLayout();
            this.cntxtMenu.ResumeLayout(false);
            this.splitContnr.Panel1.ResumeLayout(false);
            this.splitContnr.Panel2.ResumeLayout(false);
            this.splitContnr.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContnr)).EndInit();
            this.splitContnr.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.listThumbPicBox)).EndInit();
            this.seqNamePanel.ResumeLayout(false);
            this.seqNamePanel.PerformLayout();
            this.playerPanel.ResumeLayout(false);
            this.playerPanel.PerformLayout();
            this.playerControlPnl.ResumeLayout(false);
            this.playerControlPnl.PerformLayout();
            this.playToolbar.ResumeLayout(false);
            this.playToolbar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.seqPicBox)).EndInit();
            this.seqPicBoxCntxtMenu.ResumeLayout(false);
            this.seqToolbar.ResumeLayout(false);
            this.seqToolbar.PerformLayout();
            this.horizSplitContainer.Panel1.ResumeLayout(false);
            this.horizSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.horizSplitContainer)).EndInit();
            this.horizSplitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.previewThumbPicBox)).EndInit();
            this.toolbar.ResumeLayout(false);
            this.toolbar.PerformLayout();
            this.menubar.ResumeLayout(false);
            this.menubar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menubar;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem fileMenuNew;
        private System.Windows.Forms.ToolStripMenuItem fileMenuOpen;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem fileMenuSave;
        private System.Windows.Forms.ToolStripMenuItem fileMenuSaveAs;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem fileMenuExit;
        private System.Windows.Forms.ToolStripMenuItem toolsMenu;
        private System.Windows.Forms.ToolStripMenuItem toolsMenuOptions;
        private System.Windows.Forms.ToolStripMenuItem helpMenu;
        private System.Windows.Forms.ToolStripMenuItem helpMenuContents;
        private System.Windows.Forms.ToolStripMenuItem helpMenuIndex;
        private System.Windows.Forms.ToolStripMenuItem helpMenuSearch;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem helpMenuAbout;
        private System.Windows.Forms.ToolStripMenuItem helpMenuSCICommunity;
        private System.Windows.Forms.ToolStripMenuItem helpMenuSCIWiki;
        private System.Windows.Forms.HScrollBar volumeScrollBar;
        private System.Windows.Forms.PictureBox seqPicBox;
        private System.Windows.Forms.StatusStrip statusbar;
        public System.Windows.Forms.ToolStripStatusLabel statusbarMainMessage;
        private System.Windows.Forms.Button clearAllBtn;
        internal System.Windows.Forms.Button upBtn;
        internal System.Windows.Forms.Button dnBtn;
        internal System.Windows.Forms.Button addBtn;
        public System.Windows.Forms.Button removeBtn;
        private System.Windows.Forms.SplitContainer splitContnr;
        internal System.Windows.Forms.ContextMenuStrip cntxtMenu;
        private System.Windows.Forms.ToolStripMenuItem addCntxtMenu;
        internal System.Windows.Forms.ToolStripMenuItem removeCntxtMenu;
        private System.Windows.Forms.ToolStripMenuItem clearAllCntxtMenu;
        private System.Windows.Forms.ListBox frameListBox;
        private System.Windows.Forms.Timer statusTimer;
        private System.Windows.Forms.TextBox seqNameTxtBox;
        private System.Windows.Forms.Label SEQNameLbl;
        private System.Windows.Forms.FlowLayoutPanel thumbPreviewPnl;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem viewCntxtMenu;
        private System.Windows.Forms.ToolStripMenuItem editCntxtMenu;
        private System.Windows.Forms.ToolStripMenuItem propsCntxtMenu;
        private System.Windows.Forms.ToolStripMenuItem copyCntxtMenu;
        private System.Windows.Forms.ToolStrip seqToolbar;
        private System.Windows.Forms.ToolStripButton openSEQBtn;
        private System.Windows.Forms.ToolStripButton saveSEQAsBtn;
        private System.Windows.Forms.Panel playerControlPnl;
        private System.Windows.Forms.ToolStrip playToolbar;
        private System.Windows.Forms.ToolStripButton stepBackBtn;
        private System.Windows.Forms.ToolStripButton stopBtn;
        private System.Windows.Forms.ToolStripButton pauseBtn;
        private System.Windows.Forms.ToolStripButton playBtn;
        private System.Windows.Forms.ToolStripButton stepNextBtn;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton muteBtn2;
        private System.Windows.Forms.ToolStripButton spacerBtn;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.SplitContainer horizSplitContainer;
        private System.Windows.Forms.Panel playerPanel;
        private System.Windows.Forms.Label seqLbl;
        private System.Windows.Forms.Panel seqNamePanel;
        private System.Windows.Forms.ToolStripMenuItem fileMenuImport;
        private System.Windows.Forms.ToolStripMenuItem fileMenuExport;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        protected System.Windows.Forms.PictureBox listThumbPicBox;
        protected System.Windows.Forms.PictureBox previewThumbPicBox;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.ToolStripMenuItem toolsMenuRegisterSEQ;
        private System.Windows.Forms.ToolStripMenuItem toolsMenuRegisterSQP;
        private System.Windows.Forms.ToolStripMenuItem toolsMenuAppendCFG;
        private System.Windows.Forms.ToolStripButton newBtn;
        private System.Windows.Forms.ToolStripButton openBtn;
        private System.Windows.Forms.ToolStripButton saveBtn;
        private System.Windows.Forms.ToolStripButton saveAsBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton appendCFGBtn;
        private System.Windows.Forms.ToolStrip toolbar;
        private System.Windows.Forms.ToolStripButton importBtn;
        private System.Windows.Forms.ToolStripButton exportBtn;
        private System.Windows.Forms.ToolStripMenuItem fileMenuOpenSEQ;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripComboBox formatCmboBox;
        private System.Windows.Forms.Label waitLbl;
        private System.Windows.Forms.ToolStripButton editSEQBtn;
        private System.Windows.Forms.ToolStripButton saveSEQBtn;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.Button makeSEQBtn;
        private System.Windows.Forms.ToolStripMenuItem toolsMenuUseSubDir;
        private System.Windows.Forms.ToolStripMenuItem viewMenu;
        private System.Windows.Forms.ToolStripMenuItem viewMenuAspectCorrection;
        private System.Windows.Forms.ToolStripMenuItem viewMenuExamMode;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem fileMenuSaveSEQ;
        private System.Windows.Forms.ToolStripMenuItem fileMenuSaveSEQAs;
        private System.Windows.Forms.ToolStripButton rewindBtn;
        private System.Windows.Forms.ToolStripButton ffBtn;
        private System.Windows.Forms.ProgressBar seekBar;
        private System.Windows.Forms.Timer endPlayTimer;
        private System.Windows.Forms.ToolStripMenuItem deleteCntxtMenu;
        private System.Windows.Forms.ContextMenuStrip seqPicBoxCntxtMenu;
        private System.Windows.Forms.ToolStripMenuItem loadSEQCntxtMenu;
        private System.Windows.Forms.ToolStripMenuItem stopCntxtMenu;
        private System.Windows.Forms.ToolStripMenuItem playCntxtMenu;
        private System.Windows.Forms.ToolStripMenuItem editSEQCntxtMenu;
        private System.Windows.Forms.Timer importTimer;
    }
}

