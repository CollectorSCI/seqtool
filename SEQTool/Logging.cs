﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace SEQTool
{
    /// <summary>
    /// Method for generating standard output and error logs
    /// </summary>

    public class ErrorLogging
    {

        #region Declares, Imports. etc.

        public static FileStream fileStream;
        public static StreamWriter streamWriter;

        public static string ErrLog = Path.GetTempPath() + @"\SEQTool_ERR.LOG";

        MainFrm mainFrm = (MainFrm)Application.OpenForms["MainFrm"];
        //ErrorLogging errorLogging = new ErrorLogging();

        #endregion Declares, Imports. etc.


        #region Error Logging

        /// <summary>
        /// Creates new log file
        /// </summary>
        public static void OpenFile()
        {
            string strPath = ErrLog;
            if (System.IO.File.Exists(strPath))
            {
                fileStream = new FileStream(strPath, FileMode.Append, FileAccess.Write);
            }
            else
            {
                fileStream = new FileStream(strPath, FileMode.Create, FileAccess.Write);
            }
            streamWriter = new StreamWriter(fileStream);
        }


        /// <summary>
        /// Writes exception message to log file
        /// </summary>
        /// <param name="strComments"></param>
        public static void WriteLog(string strComments)
        {
            OpenFile();
            streamWriter.WriteLine(strComments);
            CloseFile();
        }


        /// <summary>
        /// Closes log file
        /// </summary>
        public static void CloseFile()
        {
            streamWriter.Close();
            fileStream.Close();
        }

        #endregion Error Logging
    }


    #region Process RedirectStandard Output & Error Handling

    public class StdOutput
    {

        /// <summary>
        /// Class for creating output and error logs
        /// </summary>


        #region Declares, Imports. etc.

        //StdOutput stdOutput = new StdOutput();
        public delegate void SetTextCallback(string value);

        #endregion Declares, Imports. etc.


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sendingProcess"></param>
        /// <param name="outLine"></param>
        public void GotData(object sendingProcess, System.Diagnostics.DataReceivedEventArgs outLine)
        {
            if (!String.IsNullOrEmpty(outLine.Data))
            {
                SetText(outLine.Data);
            }
        }


        /// <summary>
        /// Needed?
        /// </summary>
        /// <param name="value"></param>
        public void SetText(string value)
        {
            //if (this.textBox1.InvokeRequired) { 
            //    SetTextCallback d = new SetTextCallback(new System.EventHandler(this.SetText));
            //    this.Invoke(d, new object[] {
            //            value});
            //} else {
            //    this.textBox1.Text = (this.textBox1.Text + (value + Environment.NewLine));
            //}
        }
    }

    #endregion Process RedirectStandard Output & Error Handling
}
