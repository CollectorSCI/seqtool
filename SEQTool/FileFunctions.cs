/// 
/// 

using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Security.Cryptography;


namespace SEQTool
{
    #region Declares, Imports. etc.

    [StructLayout(LayoutKind.Sequential)]
    public struct SHFILEINFO
    {
        public IntPtr hIcon;
        public IntPtr iIcon;
        public uint dwAttributes;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
        public string szDisplayName;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 80)]
        public string szTypeName;
    };

    #endregion Declares, Imports. etc.


    /// <summary>
    /// File Hash Methods
    /// </summary>
    class FileMethods
    {
        #region Get Hash

        /// <summary>
        /// Reads MD-5 Hash of selected file
        /// </summary>
        /// <param name="fileName">selected file</param>
        /// <returns>MD-5 Hash</returns>
        public static string GetMD5Hash(string fileName)
        {
            try
            {
                using (var md5 = MD5.Create())
                {
                    using (var stream = File.OpenRead(fileName))
                    {
                        return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", string.Empty);
                    }
                }
            }
            catch (Exception ex)
            {
                string err = ("Error: " + ex.Message);
                MessageBox.Show(err, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + err);
                return "Hash not generated";
            }

            //using (var md5 = MD5.Create())
            //{
            //    using (var stream = File.OpenRead(fileName))
            //    {
            //        return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", string.Empty);
            //    }
            //}
        }


        /// <summary>
        /// Compares two files to see if theyare identical
        /// </summary>
        /// <param name="file1">first file to compare</param>
        /// <param name="file2">second file to compare</param>
        /// <returns>bool</returns>
        public static bool IsSame(string file1, string file2)
        {
            using (var reader1 = new FileStream(file1, FileMode.Open, FileAccess.Read))
            {
                using (var reader2 = new FileStream(file2, FileMode.Open, FileAccess.Read))
                {
                    byte[] hash1;
                    byte[] hash2;

                    using (var md51 = new System.Security.Cryptography.MD5CryptoServiceProvider())
                    {
                        md51.ComputeHash(reader1);
                        hash1 = md51.Hash;
                    }

                    using (var md52 = new System.Security.Cryptography.MD5CryptoServiceProvider())
                    {
                        md52.ComputeHash(reader2);
                        hash2 = md52.Hash;
                    }

                    int j = 0;
                    for (j = 0; j < hash1.Length; j++)
                    {
                        if (hash1[j] != hash2[j])
                        {
                            break;
                        }
                    }

                    // Files are equal
                    if (j == hash1.Length)
                    {
                        return true;
                    }
                    // Files are not equal
                    else
                    {
                        return false;
                    }
                }
            }
        }

        #endregion Get Hash


        /// <summary>
        /// Checks if file is in use
        /// http://stackoverflow.com/questions/2987559/check-if-a-file-is-open
        /// </summary>
        /// <param name="file">File to be checked</param>
        /// <returns>bool in use</returns>
        public static bool FileInUse(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
            return false;
        }
    }


    /// <summary>
    /// A collection of classes used for shell functions
    /// </summary>
    public class ShellClass
    {

        #region Declares, Imports. etc.

        MainFrm mainFrm = (MainFrm)Application.OpenForms["MainFrm"];
        //ShellClass ShellClass = new ShellClass();

        [DllImport("user32.dll", SetLastError = true)]
        public static extern uint SetParent(IntPtr hWndChild, IntPtr hWndNewParent);
        public static void LoadProcessInControl(string _Process, Control _Control)
        {
            System.Diagnostics.Process p = System.Diagnostics.Process.Start(_Process);
            p.WaitForInputIdle();
            SetParent(p.MainWindowHandle, _Control.Handle);
        }


        [DllImport("shell32.dll", CharSet = CharSet.Auto)]
        static extern bool ShellExecuteEx(ref SHELLEXECUTEINFO lpExecInfo);

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public struct SHELLEXECUTEINFO
        {
            public int cbSize;
            public uint fMask;
            public IntPtr hwnd;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpVerb;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpFile;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpParameters;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpDirectory;
            public int nShow;
            public IntPtr hInstApp;
            public IntPtr lpIDList;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpClass;
            public IntPtr hkeyClass;
            public uint dwHotKey;
            public IntPtr hIcon;
            public IntPtr hProcess;
        }

        public const int SW_SHOW = 5;
        public const int SEE_MASK_INVOKEIDLIST = 0xc;

        public static bool ShowFileProperties(string Filename)
        {
            SHELLEXECUTEINFO info = new SHELLEXECUTEINFO();
            info.cbSize = Marshal.SizeOf(info);
            info.lpVerb = "properties";
            info.lpFile = Filename;
            info.nShow = SW_SHOW;
            info.fMask = SEE_MASK_INVOKEIDLIST;
            return ShellExecuteEx(ref info);
        }

        #endregion Declares, Imports. etc.


        #region Shell Functions

        // Call File Edit
        //public static bool CallFileEdit(string Filename) {
        public static void CallFileEdit(string Filename)
        {
            SHELLEXECUTEINFO shInfo = new SHELLEXECUTEINFO();
            shInfo.cbSize = Marshal.SizeOf(shInfo);
            shInfo.lpVerb = "edit";
            shInfo.lpFile = Filename;
            shInfo.nShow = SW_SHOW;
            shInfo.fMask = SEE_MASK_INVOKEIDLIST;
            ShellExecuteEx(ref shInfo);
            //return ShellExecuteEx(ref shInfo);
        }


        /// <summary>
        /// Opens selected file in default application
        /// </summary>
        /// <param name="Filename">selected file</param>
        public static void OpenFile(string Filename)
        {
            SHELLEXECUTEINFO shInfo = new SHELLEXECUTEINFO();
            shInfo.cbSize = Marshal.SizeOf(shInfo);
            shInfo.lpFile = Filename;
            shInfo.nShow = SW_SHOW;
            shInfo.fMask = SEE_MASK_INVOKEIDLIST;
            shInfo.lpVerb = "open";
            ShellExecuteEx(ref shInfo);
        }


        /// <summary>
        /// Opens selected file's properties dialog
        /// </summary>
        /// <param name="Filename">selected file</param>
        public static void DisplayFileProperties(string Filename)
        {
            SHELLEXECUTEINFO shInfo = new SHELLEXECUTEINFO();
            shInfo.cbSize = Marshal.SizeOf(shInfo);
            shInfo.lpFile = Filename;
            shInfo.nShow = SW_SHOW;
            shInfo.fMask = SEE_MASK_INVOKEIDLIST;
            shInfo.lpVerb = "properties";
            ShellExecuteEx(ref shInfo);
        }


        /// <summary>
        /// Copys source file path
        /// </summary>
        /// <param name="Filename">selected file</param>
        public static void CopyFile(string Filename)
        {
            SHELLEXECUTEINFO shInfo = new SHELLEXECUTEINFO();
            shInfo.cbSize = Marshal.SizeOf(shInfo);
            shInfo.lpFile = Filename;
            shInfo.nShow = SW_SHOW;
            shInfo.fMask = SEE_MASK_INVOKEIDLIST;
            shInfo.lpVerb = "copy";
            ShellExecuteEx(ref shInfo);
        }


        /// <summary>
        /// Pastes File
        /// </summary>
        /// <param name="Pathname">target path</param>
        public static void PasteFile(string Pathname)
        {
            SHELLEXECUTEINFO shInfo = new SHELLEXECUTEINFO();
            shInfo.cbSize = Marshal.SizeOf(shInfo);
            shInfo.lpFile = Pathname;
            shInfo.nShow = SW_SHOW;
            shInfo.fMask = SEE_MASK_INVOKEIDLIST;
            shInfo.lpVerb = "paste";
            ShellExecuteEx(ref shInfo);
        }


        /// <summary>
        /// Get .lnk Target
        /// </summary>
        /// <param name="name">selected .lnk</param>
        /// <returns>target file</returns>
        public static string ResolveShellLink(string name)
        {
            // Find target of short-cut
            if (string.Compare(Path.GetExtension(name), ".lnk", true) != 0)
                return name;
            Shell32.Shell shl = new Shell32.Shell();
            Shell32.Folder dir = shl.NameSpace(Path.GetDirectoryName(name));
            Shell32.FolderItem itm = dir.Items().Item(Path.GetFileName(name));
            Shell32.ShellLinkObject lnk = (Shell32.ShellLinkObject)itm.GetLink;
            return lnk.Path;
        }

        #endregion Shell Functions
    }


    /// <summary>
    /// Class to gets Shell Icons for Drag&Drop shortcuts
    /// </summary>
    class Win32
    {
        public const uint SHGFI_ICON = 0x100;
        public const uint SHGFI_SMALLICON = 0x1; // 'Small icon
        public const uint SHGFI_LARGEICON = 0x0; // 'Large icon

        [DllImport("shell32.dll")]
        public static extern IntPtr SHGetFileInfo(string pszPath, uint dwFileAttributes, ref SHFILEINFO psfi, uint cbSizeFileInfo, uint uFlags);
    }


    /// <summary>
    /// Class to send files to the recycle bin instead of permanently deleting them 
    /// </summary>
    public class RecyleBin
    {
        /// <summary>
        /// David Amenta - dave@DaveAmenta.com
        /// 05/05/08
        /// Updated 04/18/2010 for x64
        /// 
        /// http://www.daveamenta.com/2008-05/c-delete-a-file-to-the-recycle-bin/
        /// 
        /// Possible flags for the SHFileOperation method.
        /// </summary>

        #region Declares, Imports. etc.

        [Flags]
        public enum FileOperationFlags : ushort
        {
            /// <summary>
            /// Do not show a dialog during the process
            /// </summary>
            FOF_SILENT = 0x0004,
            /// <summary>
            /// Do not ask the user to confirm selection
            /// </summary>
            FOF_NOCONFIRMATION = 0x0010,
            /// <summary>
            /// Delete the file to the recycle bin.  (Required flag to send a file to the bin
            /// </summary>
            FOF_ALLOWUNDO = 0x0040,
            /// <summary>
            /// Do not show the names of the files or folders that are being recycled.
            /// </summary>
            FOF_SIMPLEPROGRESS = 0x0100,
            /// <summary>
            /// Surpress errors, if any occur during the process.
            /// </summary>
            FOF_NOERRORUI = 0x0400,
            /// <summary>
            /// Warn if files are too big to fit in the recycle bin and will need
            /// to be deleted completely.
            /// </summary>
            FOF_WANTNUKEWARNING = 0x4000,
        }

        /// <summary>
        /// File Operation Function Type for SHFileOperation
        /// </summary>
        public enum FileOperationType : uint
        {
            /// <summary>
            /// Move the objects
            /// </summary>
            FO_MOVE = 0x0001,
            /// <summary>
            /// Copy the objects
            /// </summary>
            FO_COPY = 0x0002,
            /// <summary>
            /// Delete (or recycle) the objects
            /// </summary>
            FO_DELETE = 0x0003,
            /// <summary>
            /// Rename the object(s)
            /// </summary>
            FO_RENAME = 0x0004,
        }

        /// <summary>
        /// SHFILEOPSTRUCT for SHFileOperation from COM
        /// </summary>
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto, Pack = 1)]
        private struct SHFILEOPSTRUCT_x86
        {
            public IntPtr hwnd;
            [MarshalAs(UnmanagedType.U4)]
            public FileOperationType wFunc;
            public string pFrom;
            public string pTo;
            public FileOperationFlags fFlags;
            [MarshalAs(UnmanagedType.Bool)]
            public bool fAnyOperationsAborted;
            public IntPtr hNameMappings;
            public string lpszProgressTitle;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        private struct SHFILEOPSTRUCT_x64
        {
            public IntPtr hwnd;
            [MarshalAs(UnmanagedType.U4)]
            public FileOperationType wFunc;
            public string pFrom;
            public string pTo;
            public FileOperationFlags fFlags;
            [MarshalAs(UnmanagedType.Bool)]
            public bool fAnyOperationsAborted;
            public IntPtr hNameMappings;
            public string lpszProgressTitle;
        }

        [DllImport("shell32.dll", CharSet = CharSet.Auto, EntryPoint = "SHFileOperation")]
        private static extern int SHFileOperation_x86(ref SHFILEOPSTRUCT_x86 FileOp);

        [DllImport("shell32.dll", CharSet = CharSet.Auto, EntryPoint = "SHFileOperation")]
        private static extern int SHFileOperation_x64(ref SHFILEOPSTRUCT_x64 FileOp);

        #endregion Declares, Imports. etc.


        /// <summary>
        /// IsWOW64Process Method
        /// Determines if process is 64-bit or 32-bit
        /// </summary>
        /// <returns>boolean</returns>
        private static bool IsWOW64Process()
        {
            return IntPtr.Size == 8;
        }


        /// <summary>
        /// Send file to recycle bin
        /// </summary>
        /// <param name="path">Location of directory or file to recycle</param>
        /// <param name="flags">FileOperationFlags to add in addition to FOF_ALLOWUNDO</param>
        public static bool Send(string path, FileOperationFlags flags)
        {
            try
            {
                if (IsWOW64Process())
                {
                    SHFILEOPSTRUCT_x64 fs = new SHFILEOPSTRUCT_x64();
                    fs.wFunc = FileOperationType.FO_DELETE;
                    // important to double-terminate the string.
                    fs.pFrom = path + '\0' + '\0';
                    fs.fFlags = FileOperationFlags.FOF_ALLOWUNDO | flags;
                    SHFileOperation_x64(ref fs);
                }
                else
                {
                    SHFILEOPSTRUCT_x86 fs = new SHFILEOPSTRUCT_x86();
                    fs.wFunc = FileOperationType.FO_DELETE;
                    // important to double-terminate the string.
                    fs.pFrom = path + '\0' + '\0';
                    fs.fFlags = FileOperationFlags.FOF_ALLOWUNDO | flags;
                    SHFileOperation_x86(ref fs);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }


        /// <summary>
        /// Send file to recycle bin.  Display dialog, display warning if files are too big to fit (FOF_WANTNUKEWARNING)
        /// </summary>
        /// <param name="path">Location of directory or file to recycle</param>
        public static bool Send(string path)
        {
            return Send(path, FileOperationFlags.FOF_NOCONFIRMATION | FileOperationFlags.FOF_WANTNUKEWARNING);
        }


        /// <summary>
        /// Send file silently to recycle bin.  Surpress dialog, surpress errors, delete if too large.
        /// </summary>
        /// <param name="path">Location of directory or file to recycle</param>
        public static bool SendSilent(string path)
        {
            return Send(path, FileOperationFlags.FOF_NOCONFIRMATION | FileOperationFlags.FOF_NOERRORUI | FileOperationFlags.FOF_SILENT);
        }
    }
}
